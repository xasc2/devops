#!/usr/bin/python
import os
import time
import sys
import commands
import argparse
import socket
from lib import environments
from __init__ import *

def officeips():
	return ['101.95.106.238/32','202.59.232.65/32']




def exeCMD(cmds,flag=0):
	if isinstance(cmds,tuple) or isinstance(cmds,list):
		for cmd in cmds:
			try:
				status,result=commands.getstatusoutput(cmd)
				if flag == 1:
					if debug:
						print cmd
						print result
				if status != 0:
					print result
					sys.exit(1)
			except Exception as e:
				status,result = 1,e
				if debug:
					print result
				return status,result

	else:
		try:
			status,result=commands.getstatusoutput(cmds)
			if flag == 1:
				if debug:
					print cmds
					print result
			if status != 0:
				sys.exit(1)
			return result
		except Exception as e:
			status,result = 1,e
			print result
			return status,result

def addsgrule(sourceip,sgname):
	if debug:
		print "\n<--  adding sourceip:%s sgname:%s ---->\n"%(sourceip,sgname)
	incmd="aws ec2 authorize-security-group-ingress --group-name %s --protocol %s --port %s --cidr %s"%(sgname,'tcp',22,sourceip)
	exeCMD(incmd)

def removerule(sourceip,sgname):
	if debug:
		print "\n<-- deleteing sourceip:%s sgname:%s -->\n"%(sourceip,sgname)
	incmd="aws ec2 revoke-security-group-ingress --group-name %s --protocol %s --port %s --cidr %s"%(sgname,'tcp',22,sourceip)
	exeCMD(incmd)




def launchstack(stackname,template):
	if debug:
		print '\n<--------  launchstack        -------->\n"'
	launchcmd="aws cloudformation create-stack --stack-name %s --template-body file://%s"%(stackname,template)
	exeCMD(launchcmd)
	return getStacksID(stackname)

def getStackStatus(stackname):
	if debug:
		print '\n<--------   getStackStatus    -------->\n'
	cmd="aws cloudformation list-stack-resources --stack-name %s"%(stackname)
	jsondata=json.loads(exeCMD(cmd))
	for stack in jsondata["StackResourceSummaries"]:
		if debug:
			print 'ResourceStatus:%s,PhysicalResourceId:%s'%(stack["ResourceStatus"],stack["PhysicalResourceId"])
		return (stack["ResourceStatus"],stack["PhysicalResourceId"])



def getStacksID(stackname):
	if debug:
		print '\n<--------    getStacksID      -------->\n'
	cmd="aws cloudformation list-stack-resources --stack-name %s"%(stackname)
	jsondata=json.loads(exeCMD(cmd))
	while not jsondata["StackResourceSummaries"]:
		jsondata=json.loads(exeCMD(cmd))
		time.sleep(5)
	while getStackStatus(stackname)[0] == "CREATE_IN_PROGRESS":
		if debug:
			print "Stack is creating , next check at 5 seconds later...\n"
		time.sleep(5)
	return getStackStatus(stackname)[1]


def getinstanceIP(instanceid):
	if debug:
		print "\n<--------   getinstanceIP     -------->\n"
	cmd='aws ec2 describe-instances  --instance-ids %s --query Reservations[*].Instances[*].{ip:PublicIpAddress}'%(instanceid)
	jsondata=json.loads(exeCMD(cmd))
	return jsondata[0][0]['ip']

def getport22status(ip):	
	if debug:
		print "\n<--------   getport22status   -------->\n"
	sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sk.settimeout(1)
	try:
		sk.connect((ip,22))
		sk.close()
		return True
	except Exception:
		sk.close()
		return False



def getAMIversion(project,env):
	if debug:
		print "\n<--------   getAMIversion     -------->\n"
	cmd='aws ec2 describe-images --filters Name=is-public,Values=false Name=tag:project,Values=%s  Name=tag:environment,Values=%s --query Images[*].{name:Name}'%(project,env)
	jsondata=json.loads(exeCMD(cmd,0))
	
	version=0
	if jsondata:
		for name in jsondata:
			ami_name=name['name']
			curr_version=ami_name.split('-')[2]
			if version < int(curr_version):
				version=int(curr_version)
	return version+1

def createAMI(template,project,env,stackname="AMI-Base-Stack"):
	amiid=''
	try:
		sourceiplist=officeips()
		for source in sourceiplist:
			addsgrule(source,'default')
		if debug:
			print "\n<--------   Create AMI        -------->\n"
		instanceid=launchstack(stackname,template)
		if debug:
			print 'end launchstack'
		ip=getinstanceIP(instanceid)
		while not  getport22status(ip):
			if debug:
				print 'waiting for ssh port up:'
		        time.sleep(5)
		cmdscp='scp -i ~/.ssh/awsinit -o StrictHostKeyChecking=no ./file/imag.sh centos@%s:~'%ip
		cmdrunbash='ssh -i ~/.ssh/awsinit -o StrictHostKeyChecking=no centos@%s "sudo bash imag.sh"'%ip
		exeCMD(cmdscp,1)
		exeCMD(cmdrunbash,1)
		version=getAMIversion(project,env)
		createami=" aws ec2 create-image --instance-id %s  --name \"%s-%s-%s-ami\" --description '%s %s %s ami'"%(instanceid,project,env,version,project,env,version)
		jsondata=json.loads(exeCMD(createami))
		amiid=jsondata["ImageId"]
		if debug:		
			print "\n<--------Private Base AMI is ready now, AMI ID:%s -------->\n"%(amiid)
		while getAMIstatus(amiid) != "available":
			time.sleep(5)
		aminame=project+'-'+env+'-'+str(version)+'-ami'
		create_name_tag='aws ec2 create-tags --resources %s --tags Key=Name,Value=%s' %(amiid,aminame)
		create_project_tag="aws ec2 create-tags --resources %s --tags Key=project,Value=%s"%(amiid,project)
		create_env_tag='aws ec2 create-tags --resources %s --tags Key=environment,Value=%s'%(amiid,env)
		create_version_tag='aws ec2 create-tags --resources %s --tags Key=version,Value=%s'%(amiid,version)
		exeCMD([create_name_tag,create_project_tag,create_env_tag,create_version_tag],1)
	except Exception,e:
		print e
	finally:
		deleteBaseStack()
		sourceiplist=officeips()
		for source in sourceiplist:
			removerule(source,'default')	
	return {'ami-id':amiid}


def getAMIstatus(amiid):
	cmd="aws ec2 describe-images --image-ids %s"%(amiid)
	jsondata=json.loads(exeCMD(cmd))
	for image in jsondata['Images']:
		return image['State']


def deleteBaseStack():
	if debug:
		print "\n Begin to delete initial stack AMI-Base-Stack..."
	cmd="aws cloudformation delete-stack --stack-name AMI-Base-Stack"
	exeCMD(cmd)




















if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--region', help='AWS Region Name, e.g. us-east-1')
	parser.add_argument('--access_key_id', help='AWS Access Key ID')
	parser.add_argument('--secret_access_key', help='AWS Secert Access Key')
	parser.add_argument('--project', help='project Name')
	parser.add_argument('--environment',help='environment name eg,dev,lt ....')
	parser.add_argument('--img',help='image id')
	parser.add_argument('--template',help='template file')
	parser.add_argument('--debug',action='store_true' ,help='debug info')


	args = parser.parse_args()
	if  args.region and args.access_key_id and args.secret_access_key and args.project and args.environment and args.img :
		environments.set_env(args.region,args.access_key_id,args.secret_access_key)
		env_dist = os.environ
		env=args.environment.lower()
		project=args.project.lower()
		img=args.img
		debug=args.debug
		if not img.startswith('ami-'):
			print 'image id error please check !!'
			exit(1)
		if args.template:
			template=args.template
		else:
			template='template/img.json.template'
		localfile='template/img_%s.json' %project
		replacecmd="sed 's/{{IMAGEID}}/%s/g' %s >%s"%(img,template,localfile)
		exeCMD(replacecmd)
		print createAMI(localfile,project,env)
	else:
		print 'Usage: python createIamGroup.py --region [REGION] --access_key_id [AWS_ACCESS_KEY_ID] --secret_access_key [AWS_SECRET_KEY] --project [PROJECT]'
