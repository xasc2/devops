#!/usr/bin/python
import re
import json
import string 

#localfile = 'file/initial.json'
def getFileJsonData(localfile):
        with open(localfile,'r') as jsonfile:
                jsondata=''.join(jsonfile.readlines())
        return jsondata


def getInitialInfo(jsonstring,Resource):
        jsondata=json.loads(jsonstring)
        return jsondata[Resource]
