#!/usr/bin/python
import os
import sys
import commands
import argparse
import random
import re
from lib import environments
from __init__ import *

def exeCMD(cmds,flag=0):
	if isinstance(cmds,tuple) or isinstance(cmds,list):
		for cmd in cmds:
			try:
				status,result=commands.getstatusoutput(cmd)
				if flag == 1:
					if debug:
						print cmd
						print result
				if status != 0:
					print cmd
					print result
					sys.exit(1)
			except Exception as e:
				status,result = 1,e
				print result
				return status,result

	else:
		try:
			status,result=commands.getstatusoutput(cmds)
			if flag == 1:
				if debug:
					print result
			if status != 0:
				sys.exit(1)
			return result
		except Exception as e:
			status,result = 1,e
			print oresult
			return status,result
	


def createUsers(jsonstring,initialpassword):
	if debug:
		print "\n<--------Create Users-------->\n"
	if not initialpassword:
		print "No password specified for SD account,no users will be create, please use --passwd to indicate and try again!"
		sys.exit(1)
	infos=getInitialInfo(jsonstring,'Users')
	users=[]
	existusers=[]
	getusers="aws iam list-users"
	flag=exeCMD(getusers,0)
	jsondata=json.loads(flag)
        for i in jsondata['Users']:
                existusers.append(i['UserName'])
	for info in infos:
		username,usergroup=info,infos[info]['Group']
		if username in existusers:
			print "username %s already existed,will not create"%(username)
			exit(1)
		users.append(username)
		createuser="aws iam create-user --user-name %s"%(username.lower())
		attachgroup="aws iam add-user-to-group --user-name %s --group-name %s"%(username.lower(),usergroup.lower())
		if re.match('.*_sd',username):
			createprofile='aws iam create-login-profile  --user-name %s --password %s'%(username.lower(),initialpassword)
			exeCMD((createuser,attachgroup,createprofile))
		else:
			exeCMD((createuser,attachgroup),1)
	return {'users':users}
	

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--region', help='AWS Region Name, e.g. us-east-1')
	parser.add_argument('--access_key_id', help='AWS Access Key ID')
	parser.add_argument('--secret_access_key', help='AWS Secert Access Key')
	parser.add_argument('--project', help='project Name')
	parser.add_argument('--passwd',help='sd defualt password')
	parser.add_argument('--template',help='template file')
	parser.add_argument('--debug',action='store_true' ,help='debug info')
	args = parser.parse_args()
	if  args.region and args.access_key_id and args.secret_access_key and args.project:
		environments.set_env(args.region,args.access_key_id,args.secret_access_key)
		project=args.project.lower()
		debug=args.debug
		if args.template:
			template=args.template
		else:
			template='template/inital.json.template'
		localfile='template/%s.json' %project
		if args.region == 'cn-north-1':
			replacecmd="sed 's/{{PROJECT}}/%s/g' %s>%s && sed -i 's/{{project}}/%s/g;s/{{aws}}/aws-cn/g' %s"%(project,template,localfile,project,localfile)
		else:
			replacecmd="sed 's/{{PROJECT}}/%s/g' %s>%s && sed -i 's/{{project}}/%s/g;s/{{aws}}/aws/g' %s"%(project,template,localfile,project,localfile)
		exeCMD(replacecmd)
		jsonstring=getFileJsonData(localfile)
		if args.passwd:
			passwd=args.passwd
		else:
			passwd=None
		print createUsers(jsonstring,passwd)
	else:
		print 'Usage: python createIamGroup.py --region [REGION] --access_key_id [AWS_ACCESS_KEY_ID] --secret_access_key [AWS_SECRET_KEY] --project [PROJECT]'
