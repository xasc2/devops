#!/usr/bin/python


#author zhang li ming 
#create   2017-12-04
#version 0.1

import boto3
import sys,os
import argparse
import logging
from lib import environments
from logging import info as loginfo


config=['ec2','vpc','subnet','routertable','internetgateway','bucket','iam_group','iam_user','iam_role','image','securitygroup','sshkey']

client_json={
		'EC2':'describe_instances',
		'VPC':'describe_vpcs',
		'SUBNET':'describe_subnets',
		'ROUTERTABLE':'describe_route_tables',
		'INTERNETGATEWAY':'describe_internet_gateways',
		'BUCKET':'list_buckets',
		'IAM_GROUP':'list_groups',
		'IAM_USER':'list_users',
		'IAM_ROLE':'list_roles',
		'IMAGE':'describe_images',
		'SECURITYGROUP':'describe_security_groups',
		'SSHKEY':'describe_key_pairs'
		

}
service_json={
		'EC2':'ec2',
		'VPC':'ec2',
		'SUBNET':'ec2',
		'ROUTERTABLE':'ec2',
		'INTERNETGATEWAY':'ec2',
		'BUCKET':'s3',
		'IAM_GROUP':'iam',
		'IAM_USER':'iam',
		'IAM_ROLE':'iam',
		'IMAGE':'ec2',
		'SECURITYGROUP':'ec2',
		'SSHKEY':'ec2'
		

}
resouce_json={
		'EC2':'Instances',
		'VPC':'Vpcs',
		'SUBNET':'Subnets',
		'ROUTERTABLE':'RouteTables',
		'INTERNETGATEWAY':'InternetGateways',
		'BUCKET':'Buckets',
		'IAM_GROUP':'Groups',
		'IAM_USER':'Users',
		'IAM_ROLE':'Roles',
		'IMAGE':'Images',
		'SECURITYGROUP':'SecurityGroups',
		'SSHKEY':'KeyPairs'
		
}

describe_json={
	'VPC':['VpcId','CidrBlock','State','IsDefault','Tags'],
	'EC2':['InstanceId','InstanceType','ImageId','VpcId','SubnetId','PublicIpAddress','PrivateIpAddress','State','Groups','Tags'],
	'SUBNET':['SubnetId','CidrBlock','VpcId','State','Tags'],
	'ROUTERTABLE':['RouteTableId','VpcId','Tags'],
	'INTERNETGATEWAY':['InternetGatewayId','VpcId','State','Tags'],
	'BUCKET':['Name','CreationDate'],
	'IAM_GROUP':['GroupId','GroupName','Path','Arn'],
	'IAM_USER':['UserId','UserName','Path','Arn','CreateDate'],
	'IAM_ROLE':['RoleId','RoleName','Path','Arn','CreateDate','Description'],
	'IMAGE':['ImageId','Name','State','KernelId','Description','ProductCodes','VirtualizationType','Tags'],
	'SECURITYGROUP':['GroupId','GroupName','VpcId','Tags'],
	'SSHKEY':['KeyName','KeyFingerprint']
	}






def list_resource(resouce):
	try:
		if debug:
			print '%s:%s service is :%s'%(sys._getframe().f_code.co_name,resouce,service_json.get(resouce.upper()))
			print '%s:%s func:is client.%s'%(sys._getframe().f_code.co_name,resouce,client_json.get(resouce.upper()))
		loginfo('%s:service is :%s'%(sys._getframe().f_code.co_name,service_json.get(resouce.upper())))
		client = boto3.client(service_json.get(resouce.upper()))
		loginfo('%s:func:is client.%s'%(sys._getframe().f_code.co_name,client_json.get(resouce.upper())))
		if project:
			if resouce == 'image':
				response = eval('client.'+ client_json.get(resouce.upper()))(Filters=
					[
						{'Name':'is-public','Values':['false']},
						{'Name':'tag:project','Values':[project]}
					])
			elif resouce in ['iam_group','iam_user','iam_role','bucket','sshkey']:
				response = eval('client.'+ client_json.get(resouce.upper()))()
			else:
				response = eval('client.'+ client_json.get(resouce.upper()))(Filters=
					[
					{'Name':'tag:project','Values':[project]}
					])
		else:		
			if resouce == 'image':
				response = eval('client.'+ client_json.get(resouce.upper()))(Filters=[{'Name':'is-public','Values':['false']}])
			else:
				response = eval('client.'+ client_json.get(resouce.upper()))()
	except Exception as e:
		print e
		sys.exit(1)
	loginfo('%s :%s  result:'%(sys._getframe().f_code.co_name,resouce.upper()))
	loginfo(response)
	return response

				
def format_resource_return(service,data,return_list):
	loginfo('%s:%s debug ...'%(sys._getframe().f_code.co_name , service))
	if type(data) is list:
		for item in data:
			format_resource_return(service,item,return_list)
	if type(data) is dict and data.get(resouce_json.get(service.upper())) is not None:
		if debug:
			print '\n'
			print '----------------%s :%s debug ...----------------'%(sys._getframe().f_code.co_name,service)
	
		for instance in data.get(resouce_json.get(service.upper())):
			instanceinfo={}
			for getinfo in describe_json[service.upper()]:
				if service.upper() == 'EC2' and getinfo == 'Groups':
                                        networkinterfaces=instance.get('NetworkInterfaces')
                                        for networkinterface in networkinterfaces:
                                                instanceinfo[getinfo]=networkinterface.get(getinfo)
                                        continue
				elif service.upper() == 'INTERNETGATEWAY' and (getinfo == 'VpcId' or getinfo == 'State'):
					attachments=instance.get('Attachments')
					for attachment in attachments:
						instanceinfo[getinfo]=attachment.get(getinfo)
					continue	
				instanceinfo[getinfo]=instance.get(getinfo)
			if debug:
				print '%s %s :%s '%(sys._getframe().f_code.co_name,service,instanceinfo)
			loginfo('%s %s :%s'%(sys._getframe().f_code.co_name,service,instanceinfo))
			return_list.append(instanceinfo)
		if debug:
			print '>>>>>>>>>>>>>>>>>>>>%s :%s debug end<<<<<<<<<<<<<<<<'%(sys._getframe().f_code.co_name,service)
			print '\n'
	elif type(data) is dict :
		for k in data:
			format_resource_return(service,data[k],return_list)
	loginfo('%s %s return:%s'%(sys._getframe().f_code.co_name,service,return_list))
	return return_list









if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--region', help='AWS Region Name, e.g. us-east-1')
	parser.add_argument('--access_key_id', help='AWS Access Key ID')
	parser.add_argument('--secret_access_key', help='AWS Secert Access Key')
	parser.add_argument('--project', help='project Name')
	parser.add_argument('--debug',action='store_true' ,help='debug info')	
	
	args = parser.parse_args()
	script_name=os.path.basename(__file__)
	script_first=script_name.split('.')[0]
	log_file=script_first+'.log'
	logging.basicConfig(level=logging.INFO,
                format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                datefmt='%a, %d %b %Y %H:%M:%S',
                filename=log_file, filemode='w')

	if  args.region and args.access_key_id and args.secret_access_key  :
		environments.set_env(args.region,args.access_key_id,args.secret_access_key)
		debug=args.debug
		project=args.project
		
		result_json={}
		for desc in config:
			resource_data=list_resource(desc)
			return_data=format_resource_return(desc,resource_data,[])
			loginfo( '%s:return data is :%s'%(desc,return_data))
			result_json[desc.upper()]=return_data
		
#		if debug:
#			print 'result json is :%s'%(result_json)
