import time
import datetime
import re
import sys
import subprocess
import inspect
import json
from lib.CreateTmpAdmin import CreateTmpAdmin
from lib.vpc import Vpc
from lib.bucket import Bucket
from lib.logger import ScriptLogger

now = datetime.datetime.now()
module_name = __file__.split('.')[0]
log_file = str(module_name + '-' + str(now.strftime("%Y%m%d%H%M")))
ologger = ScriptLogger(module_name, log_file)
ologger.logging_info('logging starts')

def function_name():
    return inspect.stack()[1][3]

if __name__ == '__main__':
    
    region_list = ['us-east-1','us-east-2','us-west-1','us-west-2','ap-south-1','ap-northeast-1','ap-northeast-2','ap-southeast-1','ap-southeast-2','ca-central-1','eu-central-1','eu-west-1','eu-west-2','sa-east-1']
    region_set = set(region_list)

    with open('file/parameters.json') as json_file:  
        data = json.load(json_file)
        
        print data
    
    pattern_text = r'[\w\+=,.@-]+'
    pattern_cidr = r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$'
    pattern_error = 'error'
    
    deployment_detail = {}
    
    print 'create temp admin account'
    if data['credential_info']['root_account_region'] in region_set:
        myadmin = CreateTmpAdmin(data['credential_info']['root_access_key_id'], data['credential_info']['root_secret_access_key'], data['credential_info']['root_account_region'])
        cred_dict = myadmin.createtmpadmin()
        ologger.logging_info(cred_dict)
         
        deployment_detail['tmp_credential']  = cred_dict
             
        time.sleep(60)
    else:
        ologger.logging_info('The given root region is not an acceptable region')
        print 'The given root region is not an acceptable region'

    print ''
    print 'create bucket'
    if re.compile(pattern_text).search(data['project_info']['project_name']):
        
        mybucket = Bucket(data['project_info']['project_name'], cred_dict['infra_region'], cred_dict['access_key_pair_id'], cred_dict['access_key_pair_secret'])
        status = mybucket.s3exists()
        if status['backup_bucket'] == 'False':
            mybucket.createbackupbucket()
        else:
            ologger.logging_error('backup bucket is already exist')
            sys.exit('backup bucket is already exist')

        if status['log_bucket'] == 'False':
            mybucket.createlogbucket()
        else:
            ologger.logging_error('log bucket is already exist')
            sys.exit('log bucket is already exist')

        if status['repo_bucket'] == 'False':
            mybucket.createyumrepobucket()
        else:
            ologger.logging_error('repo bucket is already exist')
            sys.exit('repo bucket is already exist')

        mybucket_info = mybucket.getbucketinfo()
        ologger.logging_info(mybucket_info)
    
        print ''
        print 'createIamGroup.py'
        try:
            piamgrp = subprocess.Popen(['python','createIamGroup.py',
                                        '--region', cred_dict['infra_region'],
                                        '--access_key_id', cred_dict['access_key_pair_id'],
                                        '--secret_access_key', cred_dict['access_key_pair_secret'],
                                        '--project', data['project_info']['project_name']
                                       ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            piamgrp_tuple = piamgrp.communicate()
        except OSError as e:
            ologger.logging_error('failed to execute python createIamGroup.py: {0}'.format(str(e)))
            sys.exit('failed to execute python createIamGroup.py: {0}'.format(str(e)))
        
        if piamgrp.returncode == 0:
            if re.search(pattern_error, piamgrp_tuple[0].rstrip()):
                ologger.logging_error('AWS api call encounters error message: {0}'.format(piamgrp_tuple[0].rstrip()))
                sys.exit(piamgrp_tuple[0].rstrip())
            else:
                ologger.logging_info(piamgrp_tuple[0].rstrip())
        else:
            ologger.logging_error(str(piamgrp_tuple))
            sys.exit(str(piamgrp_tuple))
        
        print ''
        print 'createIamUser.py'
        try:
            piamusers = subprocess.Popen(['python','createIamUser.py',
                                          '--region', cred_dict['infra_region'],
                                          '--access_key_id', cred_dict['access_key_pair_id'],
                                          '--secret_access_key', cred_dict['access_key_pair_secret'],
                                          '--project', data['project_info']['project_name'],
                                          '--passwd', data['ima_info']['password']
                                         ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            piamusers_tuple = piamusers.communicate()
        except OSError as e:
            ologger.logging_error('failed to execute python createIamUser.py: {0}'.format(str(e)))
            sys.exit('failed to execute python createIamUser.py: {0}'.format(str(e)))
 
        if piamusers.returncode == 0:
            if re.search(pattern_error, piamusers_tuple[0].rstrip()):
                ologger.logging_error('AWS api call encounters error message: {0}'.format(piamusers_tuple[0].rstrip()))
                sys.exit(piamusers_tuple[0].rstrip())
            else:
                ologger.logging_info(piamusers_tuple[0].rstrip())
        else:
            ologger.logging_error(str(piamusers_tuple))
            sys.exit(str(piamusers_tuple))
 
        print ''
        print 'createIamRole.py'
        try:
            piamrole = subprocess.Popen(['python','createIamRole.py',
                                        '--region', cred_dict['infra_region'],
                                        '--access_key_id', cred_dict['access_key_pair_id'],
                                        '--secret_access_key', cred_dict['access_key_pair_secret'],
                                        '--project', data['project_info']['project_name']
                                        ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            piamrole_tuple = piamrole.communicate()
        except OSError as e:
            ologger.logging_error('failed to execute python createIamRole.py: {0}'.format(str(e)))
            sys.exit('failed to execute python createIamRole.py: {0}'.format(str(e)))
 
        if piamrole.returncode == 0:
            if re.search(pattern_error, piamrole_tuple[0].rstrip()):
                ologger.logging_info('AWS api call encounters error message: {0}'.format(piamrole_tuple[0].rstrip()))
                sys.exit(piamrole_tuple[0].rstrip())
            else:
                ologger.logging_info(piamgrp_tuple[0].rstrip())
        else:
            ologger.logging_error(str(piamrole_tuple))
            sys.exit(str(piamrole_tuple))
     
        for i in range(len(data['project_info']['env_info'])):
     
            print ''
            print 'create image'
            if (
                re.compile(pattern_text).search(data['project_info']['env_info'][i]['environment']) and
                data['project_info']['env_info'][i]['infra_info']['infra_region'] in region_set and 
                re.compile(pattern_text).search(data['image_info']['image_id'])
               ):
                try:    
                    pimage = subprocess.Popen(['python','createImage.py',
                                               '--region', data['project_info']['env_info'][i]['infra_info']['infra_region'],
                                               '--access_key_id', cred_dict['access_key_pair_id'],
                                               '--secret_access_key', cred_dict['access_key_pair_secret'],
                                               '--project', data['project_info']['project_name'],
                                               '--environment', data['project_info']['env_info'][i]['environment'],
                                               '--img', data['project_info']['env_info'][i]['infra_info']['image_id']
                                               ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    pimage_tuple = pimage.communicate()
                except OSError as e:
                    ologger.logging_error('failed to execute python createImage.py: {0}'.format(str(e)))
                    sys.exit('failed to execute python createImage.py: {0}'.format(str(e)))
         
                if pimage.returncode == 0:
                    if re.search(pattern_error, pimage_tuple[0].rstrip()):
                        ologger.logging_error('AWS api call encounters error message: {0}'.format(pimage_tuple[0].rstrip()))
                        sys.exit(pimage_tuple[0].rstrip())
                    else:
                        ologger.logging_info(pimage_tuple[0].rstrip())
                else:
                    ologger.logging_error(str(pimage_tuple))
                    sys.exit(str(pimage_tuple))
         
                for j in range(len(data['project_info']['env_info'][i]['infra_info']['network_info'])):
         
                    print ''
                    print 'create vpc'
                    if (
                        re.compile(pattern_cidr).search(data['project_info']['env_info'][i]['infra_info']['network_info'][j]['infra_vpc_cidr_block']) and
                        re.compile(pattern_cidr).search(data['project_info']['env_info'][i]['infra_info']['network_info'][j]['infra_subnet_cidr_block'])
                       ):
                        myvpc = Vpc(data['project_info']['project_name'],
                                    data['project_info']['env_info'][i]['environment'],
                                    data['project_info']['env_info'][i]['infra_info'][j]['infra_vpc_cidr_block'],
                                    data['project_info']['env_info'][i]['infra_info'][j]['infra_subnet_cidr_block'],
                                    data['project_info']['env_info'][i]['infra_info']['infra_region'],
                                    cred_dict['access_key_pair_id'],
                                    cred_dict['access_key_pair_secret'])
                        if not myvpc.vpcexists():
                            myvpc.initializevpc()
                            myvpc.createsubnet()
                            myvpc.createroutetable()
                            myvpc.createroute()
                            myvpc.createinternetgateway()
                            myvpc.attachgatewaytovpc()
                            myvpc_info = myvpc.getvpcinfo()
                            ologger.logging_info(myvpc_info)
                        else:
                            ologger.logging_error('vpc already exists, exiting this program...')
                            sys.exit('vpc already exists, exiting this program...')
                         
                        print ''
                        print 'createSecurityGroup.py'
                        try:
                            psegrp = subprocess.Popen(['python','createSecurityGroup.py',
                                                        '--region', data['project_info']['env_info'][i]['infra_info']['infra_region'],
                                                        '--access_key_id', cred_dict['access_key_pair_id'],
                                                        '--secret_access_key', cred_dict['access_key_pair_secret'],
                                                        '--project', data['project_info']['project_name'],
                                                        '--environment', data['project_info']['env_info'][i]['environment'],
                                                        '--vpc', myvpc_info['vpc_id']
                                                        ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                            psegrp_tuple = psegrp.communicate()
                        except OSError as e:
                            ologger.logging_error('failed to execute python createImage.py: {0}'.format(str(e)))
                            sys.exit('failed to execute python createImage.py: {0}'.format(str(e)))
                 
                        if psegrp.returncode == 0:
                            if re.search(pattern_error, psegrp_tuple[0].rstrip()):
                                ologger.logging_error('AWS api call encounters error message: {0}'.format(psegrp_tuple[0].rstrip()))
                                sys.exit(psegrp_tuple[0].rstrip())
                            else:
                                ologger.logging_info(psegrp_tuple[0].rstrip())
                        else:
                            ologger.logging_error(str(psegrp_tuple))
                            sys.exit(str(psegrp_tuple))
                    else:
                        ologger.logging_error('The given cidr block format is wrong')
                        sys.exit('The given cidr block format is wrong')
            else:
                ologger.logging_error('The given environment name or image id does not match alphanumeric characters, or any of the following: _+=,.@- or the given infra_region does not exist, please recheck your configuration, the accepted region list is us-east-1 us-east-2 us-west-1 us-west-2 ap-south-1 ap-northeast-1 ap-northeast-2 ap-southeast-1 ap-southeast-2 ca-central-1 eu-central-1 eu-west-1 eu-west-2 sa-east-1')
                sys.exit('The given environment name or image id does not match alphanumeric characters, or any of the following: _+=,.@- or the given infra_region does not exist, please recheck your configuration, the accepted region list is us-east-1 us-east-2 us-west-1 us-west-2 ap-south-1 ap-northeast-1 ap-northeast-2 ap-southeast-1 ap-southeast-2 ca-central-1 eu-central-1 eu-west-1 eu-west-2 sa-east-1')
    else:
        ologger.logging_error('The given project name does not match alphanumeric characters, or any of the following: _+=,.@-')
        sys.exit('The given project name does not match alphanumeric characters, or any of the following: _+=,.@-')