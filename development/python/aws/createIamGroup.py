#!/usr/bin/python
import os
import sys
import commands
import argparse
import random
from lib import environments
from __init__ import *

def exeCMD(cmds,flag=0):
	if isinstance(cmds,tuple) or isinstance(cmds,list):
		for cmd in cmds:
			try:
				status,result=commands.getstatusoutput(cmd)
				if flag == 1:
					if debug:
						print cmd
						print result
				if status != 0:
					print cmd
					print result
					sys.exit(1)
			except Exception as e:
				status,result = 1,e
				print result
				return result

	else:
		try:
			status,result=commands.getstatusoutput(cmds)
			if flag == 1:
				if debug:
					print result
			if status != 0:
				print result
				sys.exit(1)
			return result
		except Exception as e:
			status,result = 1,e
			print result
			return result
	

def getPolicyARN(policyname):
	try:
		getPolicyArn="aws iam list-policies |grep 'policy/%s'|awk -F\\\" '{print $4}'"%(policyname)
		status,output = commands.getstatusoutput(getPolicyArn)
		return status,output
	except Exception as e:
                status,output = 1,e
		print output
                return status,output




def createPolicies(jsonstring):
	if debug:
		print "\n<--------Create Policies-------->\n"
	existpolicies=[]
	infos=getInitialInfo(jsonstring,'Policies')
	getpolices="aws iam list-policies"
	flag=exeCMD(getpolices,0)
	jsondata=json.loads(flag)
	for i in jsondata['Policies']:
		existpolicies.append(i['PolicyName'])
	for info in infos:
		policyname,PolicyDocument=info,json.dumps(infos[info]['PolicyDocument'])
		if policyname in existpolicies:
			if debug:
				print "policy %s already existed,will not create"%(policyname)
		else:
			docname="file/doc-%s.json"%(''.join(random.sample(string.ascii_letters + string.digits, 8)))
			with open(docname,'w') as doc:
                	        doc.write(PolicyDocument)
			createpolicy="aws iam create-policy --policy-name %s --policy-document file://%s"%(policyname,docname)
			exeCMD(createpolicy,1)
			exeCMD("rm -f file/doc-*json")



def createGroups(jsonstring):
	if debug:
		print "\n<--------Create Groups-------->\n"
	existgroups=[]
	infos=getInitialInfo(jsonstring,'Groups')
	getgroups="aws iam list-groups"
	flag=exeCMD(getgroups,0)
	jsondata=json.loads(flag)
	for i in jsondata['Groups']:
		existgroups.append(i['GroupName'])
	groupnames=[]
	for info in infos:
		groupname,Policy=info,infos[info]['Policy']
		groupnames.append(groupname)
		if groupname in existgroups:
			print "group %s already existed,will not create"%(groupname)
			exit(1)
		else:
			policyARN=getPolicyARN(Policy)[1]
			creategroup="aws iam create-group --group-name %s"%(groupname.lower())
			attachpolicy="aws iam attach-group-policy --policy-arn %s --group-name %s"%(policyARN,groupname)
			exeCMD((creategroup,attachpolicy),1)
	return {'groupnames':groupnames}


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--region', help='AWS Region Name, e.g. us-east-1')
	parser.add_argument('--access_key_id', help='AWS Access Key ID')
	parser.add_argument('--secret_access_key', help='AWS Secert Access Key')
	parser.add_argument('--project', help='project Name')
	parser.add_argument('--template',help='template file')
	parser.add_argument('--debug',action='store_true' ,help='debug info')
	args = parser.parse_args()
	if  args.region and args.access_key_id and args.secret_access_key and args.project:
		environments.set_env(args.region,args.access_key_id,args.secret_access_key)
		project=args.project.lower()
		debug=args.debug
		if args.template:
			template=args.template
		else:
			template='template/inital.json.template'
		localfile='template/%s.json' %project
		if args.region == 'cn-north-1':
			replacecmd="sed 's/{{PROJECT}}/%s/g' %s>%s && sed -i 's/{{project}}/%s/g;s/{{aws}}/aws-cn/g' %s"%(project,template,localfile,project,localfile)
		else:
			replacecmd="sed 's/{{PROJECT}}/%s/g' %s>%s && sed -i 's/{{project}}/%s/g;s/{{aws}}/aws/g' %s"%(project,template,localfile,project,localfile)
		exeCMD(replacecmd)
		jsonstring=getFileJsonData(localfile)
		createPolicies(jsonstring)
		print createGroups(jsonstring)
	else:
		print 'Usage: python createIamGroup.py --region [REGION] --access_key_id [AWS_ACCESS_KEY_ID] --secret_access_key [AWS_SECRET_KEY] --project [PROJECT]'
