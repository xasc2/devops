#!/usr/bin/python
import os
import sys
import commands
import argparse
from lib import environments
from __init__ import *

def exeCMD(cmds,flag=0):
	if isinstance(cmds,tuple) or isinstance(cmds,list):
		for cmd in cmds:
			try:
				status,result=commands.getstatusoutput(cmd)
				if flag == 1:
					if debug:
						print result
				if status != 0:
					print result
					sys.exit(1)
			except Exception as e:
				status,result = 1,e
				print result
				return status,result

	else:
		try:
			status,result=commands.getstatusoutput(cmds)
			if flag == 1:
				if debug:
					print result
			if status != 0:
				print result
				sys.exit(1)
			return result
		except Exception as e:
			status,result = 1,e
			print result
			return status,result

def getVPCID(vpcname):
	descmd="aws ec2 describe-vpcs --filters Name=tag:Name,Values=%s --query 'Vpcs[*].{ID:VpcId}'"%(vpcname)
	desc=exeCMD(descmd)
	s=json.loads(desc)
	if s :
		return dict(s[0])["ID"]
	else:
		print 'not find vpc:%s'%(vpcname)
		exit(1)

def getSGID(sgname,vpcid):
        descmd="aws ec2 describe-security-groups --filters Name=group-name,Values=%s  Name=vpc-id,Values=%s  --query 'SecurityGroups[*].{ID:GroupId}'"%(sgname,vpcid)
        desc=exeCMD(descmd)
        s=json.loads(desc)
        return dict(s[0])["ID"]



def createSecurityGroups(jsonstring,vpcid,project,environment):
	VPCID=vpcid
	if debug:
		print "\n<--------Create SecurityGroups-------->\n"
	infos=getInitialInfo(jsonstring,'SecurityGroups')
	#create security group first
	sgiddict={}
	return_sg_list=[]
	exitsgs=[]
	getsgs='aws ec2 describe-security-groups --filters Name=vpc-id,Values=%s'%(vpcid)
	flag=exeCMD(getsgs,0)
        jsondata=json.loads(flag)
	for i in jsondata['SecurityGroups']:
		exitsgs.append(i['GroupName'])
	for info in infos:
		sgname,sgcontent=info,infos[info]
		if sgname in exitsgs:
			print "security group: %s already existed,will not create"%(sgname)
			exit(1)
		description=sgcontent['Description']
		createsg="aws ec2 create-security-group --group-name %s --vpc-id %s --description %s"%(sgname,VPCID,description)
		exeCMD(createsg,1)
		SGID=getSGID(sgname,VPCID)
		return_sg_list.append(SGID)
		sgiddict[sgname]=SGID
		create_name_tag="aws ec2 create-tags --resources %s --tags Key=Name,Value=%s"%(SGID,sgname)
		create_project_tag="aws ec2 create-tags --resources %s --tags Key=project,Value=%s"%(SGID,project)
		create_env_tag="aws ec2 create-tags --resources %s --tags Key=environment,Value=%s"%(SGID,environment)
		exeCMD([create_name_tag,create_project_tag,create_env_tag])
	#create rules in security group
	for info in infos:
		sg,sgcontent=info,infos[info]
		sgname=sgiddict[sg]
		rules=sgcontent['rules']
		for (rule,rulecontent) in rules.items():
			if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d{1,2}",rulecontent['Source']):
				incmd="aws ec2 authorize-security-group-ingress --group-id %s --protocol %s --port %s --cidr %s"%(sgname,rulecontent['Protocol'],rulecontent['Port'],rulecontent['Source'])
			else:
				source_sg_id=getSGID(rulecontent['Source'],VPCID)
				incmd="aws ec2 authorize-security-group-ingress --group-id %s --protocol %s --port %s --source-group %s"%(sgname,rulecontent['Protocol'],rulecontent['Port'],source_sg_id) 
			exeCMD(incmd)
	return {'securitygroups':return_sg_list}



if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--region', help='AWS Region Name, e.g. us-east-1')
	parser.add_argument('--access_key_id', help='AWS Access Key ID')
	parser.add_argument('--secret_access_key', help='AWS Secert Access Key')
	parser.add_argument('--project', help='project Name')
	parser.add_argument('--environment',help='environment name eg,dev,lt ....')
	parser.add_argument('--vpc',help='vpc name or id')
	parser.add_argument('--template',help='template file')
	parser.add_argument('--debug',action='store_true' ,help='debug info')

	args = parser.parse_args()
	if  args.region and args.access_key_id and args.secret_access_key and args.project and args.environment:
		environments.set_env(args.region,args.access_key_id,args.secret_access_key)
		env=args.environment.lower()
		environment=args.environment.lower()
		project=args.project.lower()
		debug=args.debug
		if args.template:
			template=args.template
		else:
			template='template/inital.json.template'
		localfile='template/%s.json' %project
		if args.region == 'cn-north-1':
			replacecmd="sed 's/{{PROJECT}}/%s/g' %s >%s && sed -i 's/{{project}}/%s/g;s/{{aws}}/aws-cn/g;s/{{ENVIRONMENT}}/%s/g' %s"%(project,template,localfile,project,env,localfile)
		else:
			replacecmd="sed 's/{{PROJECT}}/%s/g' %s>%s && sed -i 's/{{project}}/%s/g;s/{{aws}}/aws/g;s/{{ENVIRONMENT}}/%s/g' %s"%(project,template,localfile,project,env,localfile)
		exeCMD(replacecmd)
		jsonstring=getFileJsonData(localfile)
		if args.vpc:
			vpcname=args.vpc
			if vpcname.startswith('vpc-'):
				VPCID=vpcname
			else:
				VPCID=getVPCID(vpcname.lower())
		else:
			defaultvpccmd="aws ec2 describe-vpcs --filters Name=isDefault,Values=true  --query 'Vpcs[*].{ID:VpcId} '"
			defaultvpc=exeCMD(defaultvpccmd)
			s=json.loads(defaultvpc)
			VPCID=dict(s[0])["ID"]

		print createSecurityGroups(jsonstring,VPCID,project,environment)
	else:
		print 'Usage: python createIamGroup.py --region [REGION] --access_key_id [AWS_ACCESS_KEY_ID] --secret_access_key [AWS_SECRET_KEY] --project [PROJECT]'
