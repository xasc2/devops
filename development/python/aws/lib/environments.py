import os

def set_env(region,acccess_key_id,secret_access_key):
    os.environ["AWS_DEFAULT_REGION"] = region
    os.environ["AWS_ACCESS_KEY_ID"] = acccess_key_id
    os.environ["AWS_SECRET_ACCESS_KEY"] = secret_access_key
    return 0
