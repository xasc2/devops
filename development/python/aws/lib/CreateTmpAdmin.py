import boto3
import random
import sys
from botocore.exceptions import ClientError

class CreateTmpAdmin():
    def __init__(self, rootaccessid, rootaccesskey, infraregion, tmpadminname='tmpAdmin'):
        self.aws_access_key_id = rootaccessid
        self.aws_access_secret_key = rootaccesskey
        self.infra_region = infraregion
        self.tmpadminname = tmpadminname
        self.bot_username = '{0}{1}'.format(self.tmpadminname,random.randrange(10000,99999))

        self.session = boto3.session.Session(self.aws_access_key_id,
                                             self.aws_access_secret_key,
                                            )

        self.iam = self.session.resource('iam')
        self.tmp_credential_detail = {}
        
    def createtmpadmin(self):
        try:
            user = self.iam.create_user(UserName=self.bot_username)
        except ClientError as e:
            print e.response
            sys.exit()

        try:
            user.attach_policy(PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess')
        except ClientError as e:
            print e.response
            sys.exit()
            
        try:
            access_key_pair = user.create_access_key_pair()
        except ClientError as e:
            print e.response
            sys.exit()
        
        self.tmp_credential_detail['admin'] = self.bot_username
        self.tmp_credential_detail['infra_region'] = self.infra_region
        self.tmp_credential_detail['access_key_pair_id'] = access_key_pair.id
        self.tmp_credential_detail['access_key_pair_secret'] = access_key_pair.secret 
        
        return self.tmp_credential_detail

if __name__ == '__main__':
    myadmin = CreateTmpAdmin('', '', 'us-east-2')
    cred_dict = myadmin.createtmpadmin()
    print cred_dict
