import boto3
from botocore.exceptions import ClientError

class Vpc():

    def __init__(self, project, env, vpccidrblock, subnetcidrblock, defaultregion, accesskeyid, accesssecretkey):
        self.project = project
        self.env = env
        self.vpc_cidr_block = vpccidrblock
        self.subnet_cidr_block = subnetcidrblock
        self.aws_default_region = defaultregion
        self.aws_access_key_id = accesskeyid
        self.aws_access_secret_key = accesssecretkey
        self.vpc_detail = {}
        
        self.session = boto3.session.Session(self.aws_access_key_id,
                                             self.aws_access_secret_key,
                                             region_name = self.aws_default_region
                                             )
        
        self.ec2 = self.session.resource('ec2')
        self.vpc = self.session.client('ec2')

    def vpcexists(self):
        tag_value = '{0}-{1}-vpc'.format(self.project, self.env)
        vpc_response = self.vpc.describe_vpcs(
            Filters=[
                    {
                        'Name'   : 'tag:Name',
                        'Values' : [tag_value]
                    }    
                ]
        )
        
        vpc_resp = vpc_response['Vpcs']
        if vpc_resp:
            return True
        else:
            return False

    def initializevpc(self):
        try:
            self.vpc = self.ec2.create_vpc(CidrBlock=self.vpc_cidr_block)
        except ClientError as e:
            print e.response
        self.vpc_detail['vpc_id'] = str(self.vpc.id)
        self._createtags(self.vpc, 'vpc')

    def createsubnet(self):
        try:
            self.subnet = self.vpc.create_subnet(CidrBlock=self.subnet_cidr_block)
        except ClientError as e:
            print e.response
        self.vpc_detail['subnet_id'] = str(self.subnet.id)
        self._createtags(self.subnet, 'subnet')

    def createroutetable(self):
        try:
            self.route_table = self.vpc.create_route_table(self.vpc.id)
        except ClientError as e:
            print e.response
        self.vpc_detail['routetable_id'] = str(self.route_table.id)
        self._createtags(self.route_table, 'routetable')

    def createroute(self):
        try:
            self.route = self.ec2.Route(self.route_table.id, '0.0.0.0/0')
        except ClientError as e:
            print e.response

    def createinternetgateway(self):
        try:
            self.gateway = self.ec2.create_internet_gateway()
        except ClientError as e:
            print e.response
        self.vpc_detail['gateway_id'] = str(self.gateway.id)
        self._createtags(self.gateway, 'internet-gateway')

    def attachgatewaytovpc(self):
        try:
            self.gateway.attach_to_vpc(VpcId=self.vpc.id)
        except ClientError as e:
            print e.response

    def getvpcinfo(self):
        return self.vpc_detail

    def _createtags(self, vpcresource, rcstring):
        tag_value = '{0}-{1}-{2}'.format(self.project, self.env, rcstring) if rcstring != 'vpc' else '{0}-{1}-{2}-{3}'.format(self.project, self.env, rcstring, self.vpc_cidr_block)
        try:   
            tag = vpcresource.create_tags(Tags = [
                                                  {'Key'   :'Name',
                                                   'Value' : tag_value
                                                  },
                                                  {'Key'   : 'Project',
                                                   'Value' : self.project
                                                  },
                                                  {'Key'   : 'environment',
                                                   'Value' : self.env
                                                  }
                                                 ]
                                          )
        except ClientError as e:
            print e.response

if __name__ == '__main__':
    myvpc = Vpc('testprj', 'dev', '10.0.0.0/16', '10.0.1.0/24', 'us-east-2', '', '')
    myvpc.initializevpc()
    myvpc.createsubnet()
    myvpc.createroutetable()
    myvpc.createroute()
    myvpc.createinternetgateway()
    myvpc.attachgatewaytovpc()
    print myvpc.getvpcinfo()