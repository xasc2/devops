import logging
from singleton import Singleton

class ScriptLogger():
    
    __metaclass__ = Singleton
    
    def __init__(self, moduleName, logFileName, logFormat='%(asctime)s - %(name)s - %(levelname)s - %(message)s'):
        self.moduleName = moduleName
        self.logFileName = logFileName
        self.logFormat = logFormat
        
        self.logger = logging.getLogger(self.moduleName)
        self.handler = logging.FileHandler(self.logFileName)
        self.formatter = logging.Formatter(self.logFormat)
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)
    
    def logging_debug(self, msg, *args, **kwargs):
        self.logger.setLevel(logging.DEBUG)
        self.logger.debug(msg, *args, **kwargs)
    
    def logging_info(self, msg, *args, **kwargs):
        self.logger.setLevel(logging.INFO)
        self.logger.info(msg, *args, **kwargs)

    def logging_warning(self, msg, *args, **kwargs):
        self.logger.setLevel(logging.WARNING)
        self.logger.warining(msg, *args, **kwargs)

    def logging_error(self, msg, *args, **kwargs):
        self.logger.setLevel(logging.ERROR)
        self.logger.error(msg, *args, **kwargs)

    def logging_critical(self, msg, *args, **kwargs):
        self.logger.setLevel(logging.CRITICAL)
        self.logger.critical(msg, *args, **kwargs)
    
    def __str__(self):
        return 'this is str'

if __name__ == '__main__':
    mylogger = ScriptLogger('my_module', 'hello.log')
#     mylogger.logging_info('i am from class')
    print mylogger