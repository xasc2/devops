import boto3
from botocore.exceptions import ClientError

class Bucket():
    
    def __init__(self, project, defaultregion, accesskeyid, accesssecretkey, bucketacl='private'):
        self.project = project
        self.aws_default_region = defaultregion
        self.backup_bucketname = '{0}-backup'.format(self.project)
        self.log_bucketname = '{0}-log'.format(self.project)
        self.repo_bucketname = '{0}-s3-yum-repo'.format(self.project)
        self.bucketacl = bucketacl
        self.aws_access_key_id = accesskeyid
        self.aws_access_secret_key = accesssecretkey
        
        self.bucket_detail = {}
        self.bucket_detail['bucket_name'] = []

        self.session = boto3.session.Session(self.aws_access_key_id,
                                             self.aws_access_secret_key,
                                             region_name = self.aws_default_region
                                            )
        
        self.s3 = self.session.resource('s3')
    
    def s3exists(self):
        bucket_status = {}
        bucket_status['backup_bucket'] = str(self.s3.Bucket(self.backup_bucketname) in self.s3.buckets.all())
        bucket_status['log_bucket'] = str(self.s3.Bucket(self.log_bucketname) in self.s3.buckets.all())
        bucket_status['repo_bucket'] = str(self.s3.Bucket(self.repo_bucketname) in self.s3.buckets.all())
        
        return bucket_status
 
    def createbackupbucket(self):
        try:
            if self.aws_default_region == 'us-east-1':
                self.s3_response = self.s3.create_bucket(
                                                    ACL=self.bucketacl,
                                                    Bucket=self.backup_bucketname
                                                   )
            else:
                self.s3_response = self.s3.create_bucket(
                                                    ACL=self.bucketacl,
                                                    Bucket=self.backup_bucketname,
                                                    CreateBucketConfiguration={'LocationConstraint':self.aws_default_region}
                                                   )
        except ClientError as e:
            print e.response
        
        self.bucket_detail['bucket_name'].append(self.s3_response.name)

    def createlogbucket(self):
        try:
            if self.aws_default_region == 'us-east-1':
                self.s3_response = self.s3.create_bucket(
                                                    ACL=self.bucketacl,
                                                    Bucket=self.log_bucketname
                                                   )
            else:
                self.s3_response = self.s3.create_bucket(
                                                    ACL=self.bucketacl,
                                                    Bucket=self.log_bucketname,
                                                    CreateBucketConfiguration={'LocationConstraint':self.aws_default_region}
                                                   )
        except ClientError as e:
            print e.response
        
        self.bucket_detail['bucket_name'].append(self.s3_response.name)

    def createyumrepobucket(self):
        try:
            if self.aws_default_region == 'us-east-1':
                self.s3_response = self.s3.create_bucket(
                                                    ACL=self.bucketacl,
                                                    Bucket=self.repo_bucketname
                                                   )
            else:
                self.s3_response = self.s3.create_bucket(
                                                    ACL=self.bucketacl,
                                                    Bucket=self.repo_bucketname,
                                                    CreateBucketConfiguration={'LocationConstraint':self.aws_default_region}
                                                   )
        except ClientError as e:
            print e.response
        
        self.bucket_detail['bucket_name'].append(self.s3_response.name)
        
    def getbucketinfo(self):
        return self.bucket_detail

if __name__ == '__main__':
    mybucket = Bucket('spacewalk01', 'us-east-1', '', '')
    mybucket.createbackupbucket()
    mybucket.createlogbucket()
    mybucket.createyumrepobucket()
    print mybucket.getbucketinfo()