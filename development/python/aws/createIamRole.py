#!/usr/bin/python
import os
import sys
import commands
import argparse
import random
from lib import environments
from __init__ import *

def exeCMD(cmds,flag=0):
	if isinstance(cmds,tuple) or isinstance(cmds,list):
		for cmd in cmds:
			try:
				status,result=commands.getstatusoutput(cmd)
				if flag == 1:
					if debug:
						print cmd
						print result
				if status != 0:
					if debug:
						print cmd
						print result
					sys.exit(1)
			except Exception as e:
				status,result = 1,e
				print result
				return result

	else:
		try:
			status,result=commands.getstatusoutput(cmds)
			if flag == 1:
				print result
			if status != 0:
				print cmd
				print result
				sys.exit(1)
			return result
		except Exception as e:
			status,result = 1,e
			print result
			return result
	

def getPolicyARN(policyname):
	try:
		getPolicyArn="aws iam list-policies |grep 'policy/%s'|awk -F\\\" '{print $4}'"%(policyname)
		status,output = commands.getstatusoutput(getPolicyArn)
		return status,output
	except Exception as e:
                status,output = 1,e
		print output
                return status,output




def createPolicies(jsonstring):
	if debug:
		print "\n<--------Create Policies-------->\n"
	existpolicies=[]
	infos=getInitialInfo(jsonstring,'Policies')
	getpolices="aws iam list-policies"
	flag=exeCMD(getpolices)
	jsondata=json.loads(flag)
	for i in jsondata['Policies']:
		existpolicies.append(i['PolicyName'])
	for info in infos:
		policyname,PolicyDocument=info,json.dumps(infos[info]['PolicyDocument'])
		if policyname in existpolicies:
			if debug:
				print "polic %s already existed,will not create"%(policyname)
		else:
			docname="file/doc-%s.json"%(''.join(random.sample(string.ascii_letters + string.digits, 8)))
			with open(docname,'w') as doc:
                	        doc.write(PolicyDocument)
			createpolicy="aws iam create-policy --policy-name %s --policy-document file://%s"%(policyname,docname)
			exeCMD(createpolicy)
			exeCMD("rm -f file/doc-*json")
		



def createRoles(jsonstring):
	if debug:
		print "\n<--------Create Roles-------->\n"
	infos=getInitialInfo(jsonstring,'Roles')
	exitroles=[]
	getroles='aws iam list-roles'
	flag=exeCMD(getroles,0)
	jsondata=json.loads(flag)
	for i in jsondata['Roles']:
                exitroles.append(i['RoleName'])
	rolenames=[]
	for info in infos:
		rolename,permissonpolicy,trustpolicy=info,infos[info]['PermissionPolicy'],json.dumps(infos[info]['TrustPolicy'])
		if rolename in exitroles:
			print "role %s already existed,will not create"%(rolename)	
			exit(1)
		rolenames.append(rolename)
		docname="doc-%s.json"%(''.join(random.sample(string.ascii_letters + string.digits, 8)))
		with open(docname,'w') as doc:
                        doc.write(trustpolicy)
		policyARN=getPolicyARN(permissonpolicy)[1]
		createrole="aws iam create-role --role-name %s --assume-role-policy-document file://%s"%(rolename,docname)
		attachpolicy="aws iam attach-role-policy --policy-arn %s --role-name %s"%(policyARN,rolename)
		createProfile="aws iam create-instance-profile --instance-profile-name %s"%(rolename)
		attachProfile="aws iam add-role-to-instance-profile --instance-profile-name %s --role-name %s"%(rolename,rolename)
		exeCMD((createrole,attachpolicy,createProfile,attachProfile))
		exeCMD("rm -f ./doc-*json")
	
	return {'rolenames':rolenames}





if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--region', help='AWS Region Name, e.g. us-east-1')
	parser.add_argument('--access_key_id', help='AWS Access Key ID')
	parser.add_argument('--secret_access_key', help='AWS Secert Access Key')
	parser.add_argument('--project', help='project Name')
	parser.add_argument('--template',help='template file')
	parser.add_argument('--debug',action='store_true' ,help='debug info')
	args = parser.parse_args()
	if  args.region and args.access_key_id and args.secret_access_key and args.project:
		environments.set_env(args.region,args.access_key_id,args.secret_access_key)
		project=args.project.lower()
		debug=args.debug
		if args.template:
			template=args.template
		else:
			template='template/inital.json.template'
	
		localfile='template/%s.json' %project
		if args.region == 'cn-north-1':
			replacecmd="sed 's/{{PROJECT}}/%s/g' %s>%s && sed -i 's/{{project}}/%s/g;s/{{aws}}/aws-cn/g;s/{{cn}}/.cn/g' %s"%(project,template,localfile,project,localfile)
		else:
			replacecmd="sed 's/{{PROJECT}}/%s/g' %s>%s && sed -i 's/{{project}}/%s/g;s/{{aws}}/aws/g;s/{{cn}}//g' %s"%(project,template,localfile,project,localfile)
		exeCMD(replacecmd)
		jsonstring=getFileJsonData(localfile)
		createPolicies(jsonstring)
		print createRoles(jsonstring)
	else:
		print 'Usage: python createIamGroup.py --region [REGION] --access_key_id [AWS_ACCESS_KEY_ID] --secret_access_key [AWS_SECRET_KEY] --project [PROJECT]'
