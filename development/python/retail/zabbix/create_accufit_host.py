#!/usr/bin/env python2.6.6

"""list of modifiable variables 
        
        :param zabbix_domain_name:                        domain name of zabbix server in web interface 
        :param zabbix_auth_account_name:                  zabbix user account, the account should have api privilege
        :param zabbix_auth_account_password:              zabbix user password
        :param vm_list_file:                              list file of imported machines
        """

zabbix_auth_account_name='xxxxx'
zabbix_auth_account_password='xxxxxxxxx'
zabbix_domain_name='http://icinga.luxottica.com.au/zabbix'
vm_list_file='zabbix_import_list'

import zabbix_api
import sys

# Connect to Zabbix server
z=zabbix_api.ZabbixAPI(server=zabbix_domain_name)
z.login(user=zabbix_auth_account_name, password=zabbix_auth_account_password)

for line in open(vm_list_file):
    columns = line.split()
#    print columns[0][3:]
    ip = "10.1"+columns[0][3:5]+"."+columns[0][5:]+".12"
#    print ip
    hostname = columns[0][3:]+" - Accufit"
#    print hostname

    # create hosts in the hostgroup
    z.host.create(
       {
                "host": hostname,
#                "name": visible_name,
                "proxy_hostid":"10105",
                "interfaces": [
                    {
                        "type": 1,
                        "main": 1,
                        "useip": 1,
                        "ip": ip,
                        "dns": "",
                        "port": "10050"
                    }
                ],
                "groups": [
                    {
                        "groupid": "38"
                    }
                ],
                "templates": [
                    {
                        "templateid": "11138"
                    },
                    {
                        "templateid": "11127"
                    },
                    {
                        "templateid": "10001"
                    }
                ]
    #            "inventory": {
    #                "macaddress_a": "01234",
    #                "macaddress_b": "56768"
    #            }
        })