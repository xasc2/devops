#!/usr/bin/env python

import dns.resolver

#### python 2.7.9 code style ###
# with open('DomainList.txt', 'r') as fp, open('dns_invalid_file', 'w') as invalid_handler, open('dns_valid_file', 'w') as valid_handler:
#     for line in fp:
#         try:
#             answers = dns.resolver.query(line.rstrip(), 'MX')
#         except:
# #            print 'invalid '+line.rstrip()           
#             invalid_handler.write(line.rstrip()+"\n")
#             invalid_handler.flush()
#         else:
# #            print line.rstrip()
#             valid_handler.write(line.rstrip()+"\n")
#             valid_handler.flush() 
#     invalid_handler.close()
#     valid_handler.close()
#     fp.close()
    
### python 2.6.6 code style ###
with open('DomainList.txt', 'r') as fp:
    with open('dns_invalid_file', 'w') as invalid_handler:
        with open('dns_valid_file', 'w') as valid_handler:
            for line in fp:
                try:
                    answers = dns.resolver.query(line.rstrip(), 'MX')
                except:
#                    print 'invalid '+line.rstrip()           
                    invalid_handler.write(line.rstrip()+"\n")
                    invalid_handler.flush()
                else:
#                    print line.rstrip()
                    valid_handler.write(line.rstrip()+"\n")
                    valid_handler.flush() 
            invalid_handler.close()
            valid_handler.close()
            fp.close()