#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
import commands
import os
import threading
import subprocess
import wx.animate
from collections import defaultdict
from wx.lib.pubsub import setupkwargs
from wx.lib.pubsub import pub
# import wx.lib.inspection

# get script owner
whoami = commands.getoutput("whoami")
# get terminal line in devices.conf
tc_output = commands.getoutput("grep tc /usr/local/share/lrapsoe/etc/devices.conf")
tc_count = 0
for tc_line in tc_output.split(os.linesep):
    try:
        assert tc_line
        if(tc_line.split()[0] == "tc"):
            # total terminal amount
            tc_count = tc_count + 1
    except AssertionError:
        pass

# get camera line in devices.conf
cam_output = commands.getoutput("grep cam /usr/local/share/lrapsoe/etc/devices.conf")
cam_count = 0
cam_list = list()
term_list = list()
for cam_line in cam_output.split(os.linesep):
    try:
        assert cam_line
        if(cam_line.split()[0] == "cam"):
            # total camera amount
            cam_count = cam_count + 1
            cam_list.append(cam_line.split()[1].replace('cam', 'ws0'))
            term_list.append(cam_line.split()[3])
    except AssertionError:
        pass

# get printer line in devices.conf
pr_output = commands.getoutput("grep pr /usr/local/share/lrapsoe/etc/devices.conf")
pr_count = 0
pr_list = list()
for pr_line in pr_output.split(os.linesep):
    try:
        assert pr_line
        if(pr_line.split()[0] == "pr"):
            pr_list.append(pr_line.split()[1])
            # total printer amount
            pr_count = pr_count + 1
    except AssertionError:
        pass

tc_list = list()
cur_list = list()

# organize terminal name then store then into tc_list
for s0x_count in range(tc_count):
    tc_list.append("s0"+str(s0x_count+1))

tc_error = False

# get cashdrawer name and store them in to cashdrawer_list
cashdrawer_list = list()
cashdrawer_dict = {}
with open("/usr/local/share/lrapsoe/etc/cashdrawer.conf") as fp:
    for line in fp:
        line_list = line.split()
        cashdrawer_dict[line_list[0]] = line_list[1]
        cashdrawer_list.append(line_list[1])
    fp.close

filenames = [309, 310, 311]

# home window
class PortalWindow(wx.Frame):
    def __init__(self, parent, pw_title, pw_style):
        wx.Frame.__init__(self, parent, title=pw_title, style=pw_style, size=(1024,700))
                
        # --- panel 1 ---#
        self.panel_1 = wx.Panel(self, 0, pos=(0,0), size=(1024,100))
        self.panel_1.SetBackgroundColour(wx.WHITE)
        self.title_text = wx.StaticText(self.panel_1, -1, title, (155, 20))
        self.title_text.SetFont(wx.Font(38, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.title_text.SetForegroundColour(wx.BLACK)

        # --- panel 2 ---#
        self.panel_2 = wx.Panel(self, -1, pos=(0,100),size=(1024,70))
        self.panel_2.SetBackgroundColour(wx.WHITE)
        self.question = "Please select an area that you experience issues with"
        self.q_text = wx.StaticText(self.panel_2, -1, self.question, (57, 10))
        self.q_text.SetFont(wx.Font(29, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.q_text.SetForegroundColour(wx.BLACK)

        # --- panel 3 ---#
        self.panel_3 = wx.Panel(self, -1, pos=(0,170),size=(1024,380))
        self.panel_3.SetBackgroundColour(wx.WHITE)
        self.fgs = wx.FlexGridSizer(1, 3, hgap=10)
        for file_id in filenames:
            self.img = wx.Image(str(file_id), wx.BITMAP_TYPE_ANY)
            self.sb = wx.StaticBitmap(self.panel_3, int(file_id), wx.BitmapFromImage(self.img))
            self.sb.Bind(wx.EVT_LEFT_UP, self.HidePortalFrame, id=file_id)
            self.fgs.Add(self.sb)

        self.panel_3.SetSizerAndFit(self.fgs)
        
        # --- panel 4 ---#
        self.panel_4 = wx.Panel(self, 0, pos=(0,550), size=(1024,100))
        self.panel_4.SetBackgroundColour(wx.WHITE)
        self.textarea = wx.TextCtrl(self.panel_4, -1, style=wx.TE_MULTILINE|wx.TE_READONLY|wx.BORDER_NONE, size=(1024,100))
        self.textarea.SetFont(wx.Font(13, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.textarea.SetForegroundColour(wx.BLUE)
        self.textarea.LoadFile("hint",fileType=wx.TEXT_TYPE_ANY)

        # --- panel 5 ---#
        self.panel_5 = wx.Panel(self, 0, pos=(0,650), size=(1024,50))
        self.panel_5.SetBackgroundColour(wx.WHITE)
        self.logo = wx.Image("OPSM_logo", wx.BITMAP_TYPE_ANY)
        self.logo_sb = wx.StaticBitmap(self.panel_5, -1, wx.BitmapFromImage(self.logo), (15,5))
        self.exit = wx.Image("exit", wx.BITMAP_TYPE_ANY)
        self.exit_sb = wx.StaticBitmap(self.panel_5, -1, wx.BitmapFromImage(self.exit), (900,5))
        self.exit_sb.Bind(wx.EVT_LEFT_UP, self.ProgramExit)

        self.Center()
        self.Show()

    def HidePortalFrame(self, event): 
        self.Hide()
        self.Center()

        if(event.GetId() == 309):
            termwindow.Show()
            termwindow.Center()
        if(event.GetId() == 310):
            printerwindow.Show()
            printerwindow.Center()
        if(event.GetId() == 311):
            commwindow.Show()
            commwindow.Center()

    def ProgramExit(self, event):
        wx.GetApp().Exit()

# termwindow from clicked terminal icon on home window
class TermWindow(wx.Frame):
    def __init__(self, parent, tw_title, tw_style):
        wx.Frame.__init__(self, parent, title=tw_title, style=tw_style, size=(1024,700))
          
        # --- panel 1 ---#
        self.t_panel_1 = wx.Panel(self, 0, pos=(0,0), size=(1024,100))
        self.t_panel_1.SetBackgroundColour(wx.WHITE)
        self.t_title_text = wx.StaticText(self.t_panel_1, -1, "Which terminal is having issues?", (200, 20))
        self.t_title_text. SetFont(wx.Font(30, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.t_title_text.SetForegroundColour(wx.BLACK)
          
        # --- panel 2 ---#
        self.t_panel_2 = wx.Panel(self, -1, pos=(0,100),size=(1024,380))
        self.t_panel_2.SetBackgroundColour(wx.WHITE)
        self.tc_img = wx.Image("t_client", wx.BITMAP_TYPE_ANY)
        
        try:
            assert tc_count <= 5
            self.t_panel_2ns = [wx.Panel(self.t_panel_2) for self.i in range(tc_count)]
            self.tc_boxsizer = wx.BoxSizer(wx.HORIZONTAL)
            for self.t_panel_2n in self.t_panel_2ns:
                self.panel_index = self.t_panel_2ns.index(self.t_panel_2n)
                self.tc_string = 's0' + str(self.panel_index + 1)
                self.t_panel_2n_uppanel = wx.Panel(self.t_panel_2n)
                self.t_panel_2n_uppanel.SetBackgroundColour(wx.WHITE)
                self.tc_sb = wx.StaticBitmap(self.t_panel_2n_uppanel, self.panel_index + 1, wx.BitmapFromImage(self.tc_img))
                self.tc_sb.Bind(wx.EVT_LEFT_UP, self.HideTermFrame, id=self.panel_index + 1)
                self.t_panel_2n_downpanel = wx.Panel(self.t_panel_2n)
                self.t_panel_2n_downpanel.SetBackgroundColour(wx.WHITE)
                self.t_panel_2n_downpanel_text = wx.StaticText(self.t_panel_2n_downpanel, -1, self.tc_string, (65, 20))
                self.t_panel_2n_downpanel_text.SetFont(wx.Font(30, wx.SWISS, wx.NORMAL, wx.BOLD))
                self.t_panel_2n_downpanel_text.SetForegroundColour(wx.BLUE)
           
                self.t_panel_2n_boxsizer = wx.BoxSizer(wx.VERTICAL)
                self.t_panel_2n_boxsizer.Add(self.t_panel_2n_uppanel, 4, wx.EXPAND)
                self.t_panel_2n_boxsizer.Add(self.t_panel_2n_downpanel, 1, wx.EXPAND)
                self.t_panel_2n.SetAutoLayout(True)
                self.t_panel_2n.SetSizer(self.t_panel_2n_boxsizer)
                self.t_panel_2n.Layout()

                self.tc_boxsizer.Add(self.t_panel_2n, 1, wx.EXPAND)
                self.t_panel_2.SetAutoLayout(True)
                self.t_panel_2.SetSizer(self.tc_boxsizer)
                self.t_panel_2.Layout()

        except AssertionError:
            self.tc_combo = wx.ComboBox(self.t_panel_2, -1, "Existing Terminals", (435, 180), (200, 40), tc_list[:], wx.CB_DROPDOWN)
            self.tc_combo.Bind(wx.EVT_COMBOBOX, self.HideTermFrame)

        # --- panel 3 ---#
        self.t_panel_3 = wx.Panel(self, 0, pos=(0,480), size=(1024,40))
        self.t_panel_3.SetBackgroundColour(wx.WHITE)
          
        # --- panel 4 ---#
        self.t_panel_4 = wx.Panel(self, 0, pos=(0,520), size=(1024,60))
        self.t_panel_4.SetBackgroundColour(wx.WHITE)
        self.t_panel_4_back_panel = wx.Panel(self.t_panel_4, 0, pos=(482,0), size=(60,60))
        self.t_panel_4_back_panel.SetBackgroundColour(wx.WHITE)
        self.back = wx.Image("back", wx.BITMAP_TYPE_ANY)
        self.back_sb = wx.StaticBitmap(self.t_panel_4_back_panel, -1, wx.BitmapFromImage(self.back), (0,0))
        self.back_sb.Bind(wx.EVT_LEFT_UP, self.GoToBack)

        # --- panel 5 ---#
        self.t_panel_5 = wx.Panel(self, 0, pos=(0,580), size=(1024,120))
        self.t_panel_5.SetBackgroundColour(wx.WHITE)

        self.Center()
        self.Hide()
#         wx.lib.inspection.InspectionTool().Show()

    def GoToBack(self, evt):
        self.Hide()
        self.Center()
        portalwindow.Show()
        portalwindow.Center()
           
    def HideTermFrame(self, event):
        try:
            assert event.GetId() <= 5 and event.GetId() > 0
            pub.sendMessage("tcName", tc_id=str(event.GetId()))
        except AssertionError:
            pub.sendMessage("tcName", tc_id=tc_list.index(self.tc_combo.GetValue()) + 1)
        self.Hide()
        self.Center()
        whatswrongwindow.Show()
        whatswrongwindow.Center()
#         someelsewindow.Show()
#         someelsewindow.Center()

# printer window from home window
class PrinterWindow(wx.Frame):
    def __init__(self, parent, pw_title, pw_style):
        wx.Frame.__init__(self, parent, title=pw_title, style=pw_style, size=(1024,700))

        # --- panel 1 ---#
        self.p_panel_1 = wx.Panel(self, 0, pos=(0,0), size=(1024,100))
        self.p_panel_1.SetBackgroundColour(wx.WHITE)
        self.p_title_text = wx.StaticText(self.p_panel_1, -1, "Printer Selection", (340, 20))
        self.p_title_text. SetFont(wx.Font(30, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.p_title_text.SetForegroundColour(wx.BLACK)

        # --- panel 2 ---#
        self.p_panel_2 = wx.Panel(self, -1, pos=(0,100),size=(1024,380))
        self.p_panel_2.SetBackgroundColour(wx.WHITE)
        self.pr_img = wx.Image("printer", wx.BITMAP_TYPE_ANY)
        
        try:
            assert pr_count <= 5
            self.p_panel_2ns = [wx.Panel(self.p_panel_2) for self.i in range(pr_count)]
            self.pr_boxsizer = wx.BoxSizer(wx.HORIZONTAL)
            for self.p_panel_2n in self.p_panel_2ns:
                self.panel_index = self.p_panel_2ns.index(self.p_panel_2n)
                self.pr_string = str(pr_list[self.panel_index])
                self.p_panel_2n_uppanel = wx.Panel(self.p_panel_2n)
                self.p_panel_2n_uppanel.SetBackgroundColour(wx.WHITE)
                self.pr_sb = wx.StaticBitmap(self.p_panel_2n_uppanel, self.panel_index + 1, wx.BitmapFromImage(self.pr_img))
                self.pr_sb.Bind(wx.EVT_LEFT_UP, self.HideTermFrame, id=self.panel_index + 1)
                self.p_panel_2n_downpanel = wx.Panel(self.p_panel_2n)
                self.p_panel_2n_downpanel.SetBackgroundColour(wx.WHITE)
                self.p_panel_2n_downpanel_text = wx.StaticText(self.p_panel_2n_downpanel, -1, self.pr_string, (65, 20))
                self.p_panel_2n_downpanel_text.SetFont(wx.Font(32, wx.SWISS, wx.NORMAL, wx.BOLD))
                self.p_panel_2n_downpanel_text.SetForegroundColour(wx.BLUE)
           
                self.p_panel_2n_boxsizer = wx.BoxSizer(wx.VERTICAL)
                self.p_panel_2n_boxsizer.Add(self.p_panel_2n_uppanel, 4, wx.EXPAND)
                self.p_panel_2n_boxsizer.Add(self.p_panel_2n_downpanel, 1, wx.EXPAND)
                self.p_panel_2n.SetAutoLayout(True)
                self.p_panel_2n.SetSizer(self.p_panel_2n_boxsizer)
                self.p_panel_2n.Layout()

                self.pr_boxsizer.Add(self.p_panel_2n, 1, wx.EXPAND)
                self.p_panel_2.SetAutoLayout(True)
                self.p_panel_2.SetSizer(self.pr_boxsizer)
                self.p_panel_2.Layout()

        except AssertionError:
            self.pr_combo = wx.ComboBox(self.p_panel_2, -1, "Existing Printers", (435, 180), (200, 40), pr_list[:], wx.CB_DROPDOWN)
            self.pr_combo.Bind(wx.EVT_COMBOBOX, self.HideTermFrame)

        # --- panel 3 ---#
        self.p_panel_3 = wx.Panel(self, 0, pos=(0,480), size=(1024,100))
        self.p_panel_3.SetBackgroundColour(wx.WHITE)
        self.p_panel_3_back_panel = wx.Panel(self.p_panel_3, 0, pos=(482,40), size=(60,60))
        self.p_panel_3_back_panel.SetBackgroundColour(wx.WHITE)
        self.back = wx.Image("back", wx.BITMAP_TYPE_ANY)
        self.back_sb = wx.StaticBitmap(self.p_panel_3_back_panel, -1, wx.BitmapFromImage(self.back), (0,0))
        self.back_sb.Bind(wx.EVT_LEFT_UP, self.GoToBack)

        # --- panel 4 ---#
        self.p_panel_4 = wx.Panel(self, 0, pos=(0,580), size=(1024,120))
        self.p_panel_4.SetBackgroundColour(wx.WHITE)

        self.Center()
        self.Hide()
#         wx.lib.inspection.InspectionTool().Show()

    def GoToBack(self, evt):
        self.Hide()
        self.Center()
        portalwindow.Show()
        portalwindow.Center()
           
    def HideTermFrame(self, evt): 
        self.Hide()
        self.Center()
        printercheckwindow.Show()
        printercheckwindow.Center()
#         someelsewindow.Show()
#         someelsewindow.Center()
        
        self.Center()
        self.Hide()

# accessories window from home network button
class CommWindow(wx.Frame):
    def __init__(self, parent, cw_title, cw_style):
        wx.Frame.__init__(self, parent, title=cw_title, style=cw_style, size=(1024,700))

        # --- panel 1 ---#
        self.c_panel_1 = wx.Panel(self, 0, pos=(0,0), size=(1024,100))
        self.c_panel_1.SetBackgroundColour(wx.WHITE)
        self.c_title_text = wx.StaticText(self.c_panel_1, -1, "Under Construction", (340, 20))
        self.c_title_text. SetFont(wx.Font(30, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.c_title_text.SetForegroundColour(wx.BLACK)

        # --- panel 2 ---#
        self.c_panel_2 = wx.Panel(self, -1, pos=(0,100),size=(1024,380))

        # --- panel 3 ---#
        self.c_panel_3 = wx.Panel(self, 0, pos=(0,480), size=(1024,100))
        self.c_panel_3.SetBackgroundColour(wx.WHITE)
        self.c_panel_3_back_panel = wx.Panel(self.c_panel_3, 0, pos=(482,40), size=(60,60))
        self.c_panel_3_back_panel.SetBackgroundColour(wx.WHITE)
        self.back = wx.Image("back", wx.BITMAP_TYPE_ANY)
        self.back_sb = wx.StaticBitmap(self.c_panel_3_back_panel, -1, wx.BitmapFromImage(self.back), (0,0))
        self.back_sb.Bind(wx.EVT_LEFT_UP, self.GoToBack)

        # --- panel 4 ---#
        self.c_panel_4 = wx.Panel(self, 0, pos=(0,580), size=(1024,120))
        self.c_panel_4.SetBackgroundColour(wx.WHITE)

        self.Center()
        self.Hide()
#         wx.lib.inspection.InspectionTool().Show()

    def GoToBack(self, evt):
        self.Hide()
        self.Center()
        portalwindow.Show()
        portalwindow.Center()
           
    def HideTermFrame(self, evt): 
        self.Hide()
        self.Center()
        whatswrongwindow.Show()
        whatswrongwindow.Center()
#         someelsewindow.Show()
#         someelsewindow.Center()

        self.Center()
        self.Hide()
# titled what's wrong window
class WhatsWrongWindow(wx.Frame):
    def __init__(self, parent, tw_title, tw_style):
        wx.Frame.__init__(self, parent, title=tw_title, style=tw_style, size=(1024,700))
        self.popup_win = DescriptionPopup(self.GetTopLevelParent(), wx.SIMPLE_BORDER)
        pub.subscribe(self.tcListener, "tcName")
        
        # --- panel 1 ---#
        self.w_panel_1 = wx.Panel(self, 0, pos=(0,0), size=(1024,100))
        self.w_panel_1.SetBackgroundColour(wx.WHITE)
        self.w_title_text = wx.StaticText(self.w_panel_1, -1, "What's Wrong?", (390, 20))
        self.w_title_text. SetFont(wx.Font(30, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.w_title_text.SetForegroundColour(wx.BLACK)

        # --- panel 2 ---#
        self.w_panel_2 = wx.Panel(self, -1, pos=(0,100),size=(1024,420))
        self.w_panel_2.SetBackgroundColour(wx.WHITE)
        
        self.accufit = wx.Image("accufit", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.accufit_button = wx.BitmapButton(self.w_panel_2, 312, self.accufit, (31,10), style=wx.NO_BORDER|wx.BU_EXACTFIT)
        self.accufit_button.Bind(wx.EVT_LEFT_UP, self.RunAccufitCheck)
        self.accufit_button.Bind(wx.EVT_ENTER_WINDOW, self.onShowPopup)
        self.accufit_button.Bind(wx.EVT_LEAVE_WINDOW, self.onHidePopup)
        
        self.terminal = wx.Image("terminal", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.terminal_button = wx.BitmapButton(self.w_panel_2, 313, self.terminal, (362,10), style=wx.NO_BORDER|wx.BU_EXACTFIT)
        self.terminal_button.Bind(wx.EVT_LEFT_UP, self.RunTerminalCheck)
        self.terminal_button.Bind(wx.EVT_ENTER_WINDOW, self.onShowPopup)
        self.terminal_button.Bind(wx.EVT_LEAVE_WINDOW, self.onHidePopup)
        
        self.cashier = wx.Image("cashier", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.cashier_button = wx.BitmapButton(self.w_panel_2, 314, self.cashier, (693,10), style=wx.NO_BORDER|wx.BU_EXACTFIT)
        self.cashier_button.Bind(wx.EVT_LEFT_UP, self.RunCashierCheck)
        self.cashier_button.Bind(wx.EVT_ENTER_WINDOW, self.onShowPopup)
        self.cashier_button.Bind(wx.EVT_LEAVE_WINDOW, self.onHidePopup)
        
        # --- panel 3 ---#
        self.w_panel_3 = wx.Panel(self, 0, pos=(0,520), size=(1024,60))
        self.w_panel_3.SetBackgroundColour(wx.WHITE)
        self.w_panel_3_back_panel = wx.Panel(self.w_panel_3, 0, pos=(482,0), size=(60,60))
        self.w_panel_3_back_panel.SetBackgroundColour(wx.WHITE)
        self.back = wx.Image("back", wx.BITMAP_TYPE_ANY)
        self.back_sb = wx.StaticBitmap(self.w_panel_3_back_panel, -1, wx.BitmapFromImage(self.back), (0,0))
        self.back_sb.Bind(wx.EVT_LEFT_UP, self.GoToBack)

        # --- panel 4 ---#
        self.w_panel_4 = wx.Panel(self, 0, pos=(0,580), size=(1024,120))
        self.w_panel_4.SetBackgroundColour(wx.WHITE)
        AddMainPanel(self.w_panel_4)

        self.Center()
        self.Hide()

    def tcListener(self, tc_id):
        global cur_list
        self.tc_title = "s0" + str(tc_id)
        AddTermIdPanel(self.w_panel_1, self.tc_title)
        try:
            assert len(cur_list) is not 0
            cur_list.pop(0)
            cur_list.insert(0, self.tc_title)
        except AssertionError:
            cur_list.append(self.tc_title)

    def GoToBack(self, event):
        self.Hide()
        self.Center()
        termwindow.Show()
        termwindow.Center()        

    # accufit check function
    def RunAccufitCheck(self, event):
        self.popup_win.Hide()
        try:
            assert cur_list[0] in term_list
            camera_name = cur_list[0].replace('s0', 'ws00')
            self.accheck_diagnosedialog = DiagnoseDialog(self, './checkcams '+str(camera_name))
            self.accheck_diagnosedialog.ShowModal()
            self.accheck_diagnosedialog.Center()
        except AssertionError:
            self.no_accufit_message = """
            No camera is attached to this terminal
            """
 
            self.no_accufit_dlg = wx.MessageDialog(None, self.no_accufit_message, 'No Camera Attached', wx.OK | wx.ICON_EXCLAMATION)
            self.result = self.no_accufit_dlg.ShowModal()
    
    #tctermreset function
    def RunTerminalCheck(self, event):
        self.popup_win.Hide()
        try:
            assert whoami != cur_list[0] 
            self.tcreset_question = """
            You are resetting your desktop at terminal """+cur_list[0]+""". 
    
            All open windows and programs on """+cur_list[0]+""" will be lost.
            Are you sure you want to continue?
            """
    
            self.tc_dlg = wx.MessageDialog(None, self.tcreset_question, 'Terminal '+cur_list[0]+' Reset', wx.YES_NO | wx.NO_DEFAULT | wx.ICON_EXCLAMATION)
            self.result = self.tc_dlg.ShowModal()
            if self.result == wx.ID_YES:
                self.tcreset_diagnosedialog = DiagnoseDialog(self, 'sudo /usr/local/bin/tctermreset '+ cur_list[0])
                self.tcreset_diagnosedialog.ShowModal()
                self.tcreset_diagnosedialog.Center()
            else:
                pass
        except AssertionError:
            self.tcreset_question = """
            Reset not allowed from terminal
            """+cur_list[0]+"""
            as it is the terminal you are currently logged in.
            To reset """+cur_list[0]+""" do this from another terminal.
            """
            
            self.tc_dlg = wx.MessageDialog(None, self.tcreset_question, 'Terminal '+cur_list[0]+' Reset', wx.OK | wx.ICON_EXCLAMATION)
            self.result = self.tc_dlg.ShowModal()

    #cash drawer check function
    def RunCashierCheck(self, event):
        self.popup_win.Hide()
        self.rcc_cashdrawerselectiondialog = CashDrawerSelectionDialog(self, cur_list[0], ["Receipt Printer " + i for i in list(set(cashdrawer_list))])
        self.rcc_cashdrawerselectiondialog.ShowModal()
        self.rcc_cashdrawerselectiondialog.Center()

    def onShowPopup(self, event):
        btn = event.GetEventObject()
        pos = btn.ClientToScreen((75,0))
        sz =  btn.GetSize()
        self.popup_win.Position(pos, (0, sz[1]))
        
        if(event.GetId() == 312):
            self.accufit_description = "If you have an issue\n" + "with Accufit camera\n" + "click here."
            self.popup_win.SetDescription(self.accufit_description)
            self.popup_win.SetLayerSize()
        if(event.GetId() == 313):
            self.accufit_description = "If you have an issue\n" + "with terminal click\n" + "here to reset it."
            self.popup_win.SetDescription(self.accufit_description)
            self.popup_win.SetLayerSize()
        if(event.GetId() == 314):
            self.accufit_description = "If you want to configure\n" + "this terminal with a\n" + "cashdrawer click here"
            self.popup_win.SetDescription(self.accufit_description)
            self.popup_win.SetLayerSize()
            
        self.popup_win.Show(True)

    def onHidePopup(self, event):
        self.popup_win.Hide()

# printer check window
class PrinterCheckWindow(wx.Frame):
    def __init__(self, parent, pcw_title, pcw_style):
        wx.Frame.__init__(self, parent, title=pcw_title, style=pcw_style, size=(1024,700))
        self.popup_win = DescriptionPopup(self.GetTopLevelParent(), wx.SIMPLE_BORDER)
#         pub.subscribe(self.tcListener, "prName")

        # --- panel 1 ---#
        self.pcw_panel_1 = wx.Panel(self, 0, pos=(0,0), size=(1024,100))
        self.pcw_panel_1.SetBackgroundColour(wx.WHITE)
        self.pcw_title_text = wx.StaticText(self.pcw_panel_1, -1, "Printer Setting", (390, 20))
        self.pcw_title_text. SetFont(wx.Font(30, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.pcw_title_text.SetForegroundColour(wx.BLACK)

        # --- panel 2 ---#
        self.pcw_panel_2 = wx.Panel(self, -1, pos=(0,100),size=(1024,420))
        self.pcw_panel_2.SetBackgroundColour(wx.WHITE)
        
#         self.accufit = wx.Image("accufit", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
#         self.accufit_button = wx.BitmapButton(self.w_panel_2, 312, self.accufit, (31,10), style=wx.NO_BORDER|wx.BU_EXACTFIT)
#         self.accufit_button.Bind(wx.EVT_LEFT_UP, self.RunAccufitCheck)
#         self.accufit_button.Bind(wx.EVT_ENTER_WINDOW, self.onShowPopup)
#         self.accufit_button.Bind(wx.EVT_LEAVE_WINDOW, self.onHidePopup)
#         
#         self.terminal = wx.Image("terminal", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
#         self.terminal_button = wx.BitmapButton(self.w_panel_2, 313, self.terminal, (362,10), style=wx.NO_BORDER|wx.BU_EXACTFIT)
#         self.terminal_button.Bind(wx.EVT_LEFT_UP, self.RunTerminalCheck)
#         self.terminal_button.Bind(wx.EVT_ENTER_WINDOW, self.onShowPopup)
#         self.terminal_button.Bind(wx.EVT_LEAVE_WINDOW, self.onHidePopup)
#         
#         self.cashier = wx.Image("cashier", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
#         self.cashier_button = wx.BitmapButton(self.w_panel_2, 314, self.cashier, (693,10), style=wx.NO_BORDER|wx.BU_EXACTFIT)
#         self.cashier_button.Bind(wx.EVT_LEFT_UP, self.RunCashierCheck)
#         self.cashier_button.Bind(wx.EVT_ENTER_WINDOW, self.onShowPopup)
#         self.cashier_button.Bind(wx.EVT_LEAVE_WINDOW, self.onHidePopup)
        
        # --- panel 3 ---#
        self.pcw_panel_3 = wx.Panel(self, 0, pos=(0,520), size=(1024,60))
        self.pcw_panel_3.SetBackgroundColour(wx.WHITE)
        self.pcw_panel_3_back_panel = wx.Panel(self.pcw_panel_3, 0, pos=(482,0), size=(60,60))
        self.pcw_panel_3_back_panel.SetBackgroundColour(wx.WHITE)
        self.back = wx.Image("back", wx.BITMAP_TYPE_ANY)
        self.back_sb = wx.StaticBitmap(self.pcw_panel_3_back_panel, -1, wx.BitmapFromImage(self.back), (0,0))
        self.back_sb.Bind(wx.EVT_LEFT_UP, self.GoToBack)

        # --- panel 4 ---#
        self.pcw_panel_4 = wx.Panel(self, 0, pos=(0,580), size=(1024,120))
        self.pcw_panel_4.SetBackgroundColour(wx.WHITE)
        AddMainPanel(self.pcw_panel_4)

        self.Center()
        self.Hide()
        
    def GoToBack(self, event):
        self.Hide()
        self.Center()
        printerwindow.Show()
        printerwindow.Center()

# 
class DiagnoseDialog(wx.Dialog):
    """
        cmd: real linux command/script with arguments or pseudo command
    """
    def __init__(self, parent, cmd):
        wx.Dialog.__init__(self, None, -1, 'Diagnosing...', size=(300, 300), style=wx.MINIMIZE_BOX | wx.MAXIMIZE_BOX)
        self.cmd = cmd
        self.ag_fname = wx.animate.Animation("loadingcirclests16.gif")
        self.ag = wx.animate.AnimationCtrl(self, -1, self.ag_fname, pos=(79, 10), size=(40, 40))
        self.ag.SetUseWindowBackgroundColour(False)
        self.ag.Play()
        
        self.info_text = wx.StaticText(self, -1, "Diagnosing...", pos=(20, 160))
        self.info_text.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.info_text.SetForegroundColour(wx.BLACK)

        self.SetBackgroundColour(wx.WHITE)

        thread = threading.Thread(target=self.run)
        thread.setDaemon(True)
        thread.start()
        
    def auto_close(self):
        wx.CallLater(4000, self.Destroy)
        wx.CallLater(4000, whatswrongwindow.Hide)
        wx.CallLater(4000, askfixedwindow.Show)
        wx.CallLater(4000, askfixedwindow.Center)
    
    def show_tcresult(self, exit_code):
        self.ag.Stop()
        try:
            assert exit_code == 0
            self.info_text.SetLabel('Terminal restarting completed successfully')
        except AssertionError:
            self.info_text.SetLabel('There are issues in terminal restarting')
            
    def show_acresult(self, text):
        self.ag.Stop()
        self.info_text.SetLabel(text)
    
    # camera check logical
    def run(self):
        self.linux_cmd = self.cmd.split()
        
        try:
            assert self.linux_cmd[0] == './checkcams'
            self.cam_info = defaultdict(lambda: defaultdict(str))

            camera = self.linux_cmd[1]
            self.issue_cam_text = ''
            if commands.getoutput('nmap -sP ' + camera).find('Host is up') == -1:
                self.cam_info[camera]['Thin Client'] = ': Dead, Reboot The Terminal'
            else:
                if commands.getoutput('sudo ssh -q -p 2222 ' + camera + ' \'lsusb |grep 04a9\'').find('04a9') == -1:
                    self.cam_info[camera]['Camera USB'] = ': NOT Connected, unplug and plug usb in again, then recheck'
                else:
                    self.cam_info[camera]['Camera Status'] = ': OK'
             
            for self.cam_name in self.cam_info:
                self.issue_cam_text = self.cam_name + '\n'
                for self.cam_key in self.cam_info[self.cam_name]:
                    self.issue_cam_text = self.issue_cam_text + str(self.cam_key) + self.cam_info[self.cam_name][self.cam_key] + '\n'
     
            wx.CallAfter(self.show_acresult, self.issue_cam_text)

        except AssertionError:
            self.proc = subprocess.Popen(self.linux_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            self.proc.stdout,self.proc.stderr = self.proc.communicate()
            wx.CallAfter(self.show_tcresult, self.proc.wait())
        
        wx.CallAfter(self.auto_close)

# popuped cash drawer dialog
class CashDrawerSelectionDialog(wx.Dialog):
    """
        terminal: pass clicked terminal name into this dialog instance
        printer_list: pass whole printer list into this dialog instance
    """
    def __init__(self, parent, terminal, printer_list):
        wx.Dialog.__init__(self, None, -1, 'CashDrawer Selection', size=(300, 350), style=wx.MINIMIZE_BOX | wx.MAXIMIZE_BOX)
        self.terminal = terminal
        self.printer_list = printer_list
        self.a_fname = wx.animate.Animation("loadingcirclests16.gif")
        self.a = wx.animate.AnimationCtrl(self, -1, self.a_fname, pos=(79, 10), size=(40, 40))
        self.a.SetUseWindowBackgroundColour(False)
#         self.ag.Play()
        try:
            assert self.terminal in cashdrawer_dict
            self.dlg_message = "Current terminal "+self.terminal+" is connected to\nreceipt printer"+cashdrawer_dict[self.terminal]+" cashdrawer.\n\nTo reconfigure with different cashdrawer\nplease select one of the below options."
        except AssertionError:
            self.dlg_message = "Current terminal "+self.terminal+" is not configured\nwith any cashdrawer.\n\nIf you want to configure it please select\none of the below options."

        self.info_text = wx.StaticText(self, -1, self.dlg_message, pos=(20, 160))
        self.info_text.SetFont(wx.Font(11, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.info_text.SetForegroundColour(wx.BLACK)

        self.receipt_combo = wx.ComboBox(self, -1, "Existing Receipt", (75, 250), (160, 40), list(set(self.printer_list[:])), wx.CB_DROPDOWN)
#         self.receipt_combo.Bind(wx.EVT_COMBOBOX, self.HideTermFrame)

        self.okButton = wx.Button(self, wx.ID_OK, "Apply", pos=(50, 300))
#         self.okButton.Disable()
        self.cancelButton = wx.Button(self, wx.ID_CANCEL, "Cancel", pos=(170, 300))
        self.cancelButton.SetDefault()

        self.SetBackgroundColour(wx.WHITE)

#         self.thread = threading.Thread(target=self.run)
#         self.thread.setDaemon(True)
#         self.thread.start()

# on mouse hover pops up layer to show descriptions
class DescriptionPopup(wx.PopupWindow):
    def __init__(self, parent, style):
        wx.PopupWindow.__init__(self, parent, style)

        self.panel = wx.Panel(self)
        self.panel.SetBackgroundColour(wx.WHITE)

        self.st = wx.StaticText(self.panel, -1, "", pos=(10,10))
 
        wx.CallAfter(self.Refresh)
    
    def SetDescription(self, description):
        self.st.SetLabel(description)
    
    def SetLayerSize(self):
        self.sz = self.st.GetBestSize()
        self.SetSize((self.sz.width+20, self.sz.height+20))
        self.panel.SetSize((self.sz.width+20, self.sz.height+20))

# window for asking if issues is fixed
class AskFixedWindow(wx.Frame):
    def __init__(self, parent, afw_title, afw_style):
        wx.Frame.__init__(self, parent, title=afw_title, style=afw_style, size=(1024,700))
        # --- panel 1 ---#
        self.afw_panel_1 = wx.Panel(self, 0, pos=(0,0), size=(1024,100))
        self.afw_panel_1.SetBackgroundColour(wx.WHITE)
        self.afw_title_text = wx.StaticText(self.afw_panel_1, -1, afw_title, (255, 20))
        self.afw_title_text.SetFont(wx.Font(38, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.afw_title_text.SetForegroundColour(wx.BLACK)

        # --- panel 2 ---#
        self.afw_panel_2 = wx.Panel(self, -1, pos=(0,100),size=(1024,70))
        self.afw_panel_2.SetBackgroundColour(wx.WHITE)

        # --- panel 3 ---#
        self.afw_panel_3 = wx.Panel(self, -1, pos=(0,170),size=(1024,380))
        self.afw_panel_3.SetBackgroundColour(wx.WHITE)
        self.afw_panel_3_yes_panel = wx.Panel(self.afw_panel_3, 0, pos=(234,150), size=(200,60))
        self.afw_panel_3_yes_panel.SetBackgroundColour(wx.WHITE)
        self.afw_panel_3_yes_panel_text = wx.StaticText(self.afw_panel_3_yes_panel, -1, "YES", (43, 15))
        self.afw_panel_3_yes_panel_text.SetFont(wx.Font(35, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.afw_panel_3_yes_panel_text.SetForegroundColour(wx.BLUE)
        self.afw_panel_3_yes_panel_text.Bind(wx.EVT_LEFT_UP, self.GoToMain)
        self.afw_panel_3_no_panel = wx.Panel(self.afw_panel_3, 0, pos=(746,150), size=(200,60))
        self.afw_panel_3_no_panel.SetBackgroundColour(wx.WHITE)
        self.afw_panel_3_no_panel_text = wx.StaticText(self.afw_panel_3_no_panel, -1, "NO", (43, 15))
        self.afw_panel_3_no_panel_text.SetFont(wx.Font(35, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.afw_panel_3_no_panel_text.SetForegroundColour(wx.BLUE)
        self.afw_panel_3_no_panel.Bind(wx.EVT_LEFT_UP, self.GoServiceCallWindown)
        self.afw_panel_3_no_panel_text.Bind(wx.EVT_LEFT_UP, self.GoServiceCallWindown)

        # --- panel 4 ---#
        self.panel_4 = wx.Panel(self, 0, pos=(0,550), size=(1024,150))
        self.panel_4.SetBackgroundColour(wx.WHITE)

        self.Center()
        self.Hide()
        
    def GoToMain(self, event):
        event.Skip()
        self.Hide()
        portalwindow.Show()
        portalwindow.Center()
    
    def GoServiceCallWindown(self, event):
        event.Skip()
        self.Hide()
        servicecallwindow.Show()
        servicecallwindow.Center()

# window for service call window
class ServiceCallWindow(wx.Frame):
    def __init__(self, parent, scw_title, scw_style):
        wx.Frame.__init__(self, parent, title=scw_title, style=scw_style, size=(1024,700))
        # --- panel 1 ---#
        self.scw_panel_1 = wx.Panel(self, 0, pos=(0,0), size=(1024,100))
        self.scw_panel_1.SetBackgroundColour(wx.WHITE)

        # --- panel 2 ---#
        self.scw_panel_2 = wx.Panel(self, -1, pos=(0,100),size=(1024,70))
        self.scw_panel_2.SetBackgroundColour(wx.WHITE)

        # --- panel 3 ---#
        self.scw_panel_3 = wx.Panel(self, -1, pos=(0,170),size=(1024,380))
        self.scw_panel_3.SetBackgroundColour(wx.WHITE)
        self.scw_info_text = wx.StaticText(self.scw_panel_3, -1, "Please contact our IT Helpdesk on +61 2 9815 2888", (100, 50))
        self.scw_info_text.SetFont(wx.Font(25, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.scw_info_text.SetForegroundColour(wx.BLACK)
        
        # --- panel 4 ---#
        self.scw_panel_4 = wx.Panel(self, 0, pos=(0,550), size=(1024,150))
        self.scw_panel_4.SetBackgroundColour(wx.WHITE)
        self.scw_panel_4 = wx.Panel(self.scw_panel_4, 0, pos=(492,0), size=(100,40))
        self.home_img = wx.Image("home", wx.BITMAP_TYPE_ANY)
        self.home_img_sb = wx.StaticBitmap(self.scw_panel_4, -1, wx.BitmapFromImage(self.home_img))
        self.home_img_sb.Bind(wx.EVT_LEFT_UP, self.GoToMain)

        self.Center()
        self.Hide()
        
    def GoToMain(self, event):
        self.Hide()
        portalwindow.Show()
        portalwindow.Center()

# right up corner terminal name panel
def AddTermIdPanel(parent,term_id):
    term_id_panel = wx.Panel(parent, 0, pos=(900,30), size=(100,50))
    term_id_panel.SetBackgroundColour(wx.WHITE)
    term_id = wx.StaticText(term_id_panel, -1, term_id, (12, 5))
    term_id.SetFont(wx.Font(20, wx.SWISS, wx.NORMAL, wx.BOLD))
    term_id.SetForegroundColour(wx.RED)

# go to home panel
def AddMainPanel(parent):
    """ Add a functional panel for going back to home screen  """ 
    main_panel = wx.Panel(parent, 0, pos=(900,10), size=(100,40))
    main_panel.SetBackgroundColour(wx.WHITE)
    home_img = wx.Image("home", wx.BITMAP_TYPE_ANY)
    home_img_sb = wx.StaticBitmap(main_panel, -1, wx.BitmapFromImage(home_img))
    home_img_sb.Bind(wx.EVT_LEFT_UP, GoToMain)

def GoToMain(evt):
    """ Go to home function """
    portalwindow.Show()
    portalwindow.Center()
    termwindow.Hide()
    termwindow.Center()
    whatswrongwindow.Hide()
    whatswrongwindow.Center()

# print wx.version()    
app = wx.App(False)
title = "OPSM Retail Self Support Portal"
window_style = wx.MINIMIZE_BOX | wx.MAXIMIZE_BOX
portalwindow = PortalWindow(None, title, window_style)
termwindow = TermWindow(None, "Terminal Choice", window_style)
printerwindow = PrinterWindow(None, "Printers", window_style)
commwindow = CommWindow(None, "Accessories", window_style)
whatswrongwindow = WhatsWrongWindow(None, "What's Wrong", window_style)
printercheckwindow = PrinterCheckWindow(None, "Printer Diagnose", window_style)
askfixedwindow = AskFixedWindow(None, "Has the issue been fixed?", window_style)
servicecallwindow = ServiceCallWindow(None, "Service Information", window_style)
app.MainLoop()