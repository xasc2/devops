#!/usr/bin/python

import json
import datetime

from aliyunsdkcore import client

#ram account afk_mgmt
clt = client.AcsClient('xxxxxxxxxxxxxxxx', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'cn-shenzhen')

now = datetime.datetime.now()
minute_ago_3 = now - datetime.timedelta(minutes=3)
minute_ago_2 = now - datetime.timedelta(minutes=2)
format_time_3 = minute_ago_3.strftime("%Y-%m-%d %I:%M:00")
format_time_2 = minute_ago_2.strftime("%Y-%m-%d %I:%M:00")
#print format_time_2
#print format_time_3

from aliyunsdkcms.request.v20170301 import QueryMetricLastRequest
qmd_request = QueryMetricLastRequest.QueryMetricLastRequest()
qmd_request.set_Project('acs_express_connect')
qmd_request.set_Dimensions('{"instanceId":"ri-wz9iutt0dsd2zvdhla7vb"}')
qmd_request.set_Metric('ReceiveBandwidth')
qmd_request.set_StartTime(format_time_3)
# qmd_request.set_EndTime('2017-04-18T01:00:00Z')
qmd_request.set_EndTime(format_time_2)
qmd_request.set_accept_format('json')
qmd_result = clt.do_action_with_exception(qmd_request)
json_result = json.loads(qmd_result)
print json_result['Datapoints'][0]['value']
#print json_result