# !/usr/bin/python
# -*- coding: utf-8 -*-

# import sys, json
from fabric.api import run, env, settings
from fabric.context_managers import hide

env.user = 'root'
# instance_ip = sys.argv[1]
instance_ip = '101.37.77.54'
env.password = 'xxxxxxxxxxxxx'
# instance_ip = '10.192.9.122'
# env.password = 'xxxxxxxxxxxxxxxx'
env.hosts = []
env.hosts.append(str(instance_ip))
        
def detect_disk():
    with settings(host_string=instance_ip, user=env.user, password=env.password, abort_on_prompts=True, warn_only=True):
        disk_result = run('lsblk | grep sdb')
        if disk_result.return_code:
            print '/dev/sdb is not attached and mounted, script is exiting'
            exit()

def install_lvm2():
    with settings(hide('warnings', 'running', 'stdout'), host_string=instance_ip, user=env.user, password=env.password, abort_on_prompts=True, warn_only=True):
        am_code = run('yum update -y')
        if am_code.return_code:
            print 'yum update -y failed'
            exit()

        lvm2_code = run('yum install -y lvm2')
        if lvm2_code.return_code:
            print 'yum install -y lvm2 failed'
            exit()
            
        xfs_code = run('yum install -y xfsprogs')
        if xfs_code.return_code:
            print 'yum install -y xfsprogs failed'
            exit()
          
def create_lvm(): 
    with settings(hide('running'), host_string=instance_ip, user=env.user, password=env.password, abort_on_prompts=True, warn_only=True):
        pv_code = run('pvcreate /dev/sdb')
        if pv_code.return_code == 5:
            print 'pvcreate /dev/sdb is already done'
            pass
        
        vg_code = run('vgcreate optvg /dev/sdb')
        if vg_code.return_code == 5:
            print 'vgcreate optvg /dev/sdb is already done'
            pass

        lv_code = run('lvcreate -l +100%FREE -n optlv /dev/optvg')
        if lv_code.return_code == 5:
            print 'lvcreate -1 +100%FREE -n optlv /dev/optvg is already done'
            pass

        mk_code = run('mkfs.xfs -f /dev/optvg/optlv')
        if mk_code.return_code:
            print 'mkfs.xfs /dev/optvg/optlv failed'
            exit()

def mount_lvm():
    with settings(hide('warnings', 'running', 'stdout'), host_string=instance_ip, user=env.user, password=env.password, abort_on_prompts=True, warn_only=True):
        fstab_file = open('/etc/fstab', 'r')
        line_pattern = '/dev/optvg/optlv    /opt    xfs    defaults    0 0'
        flines = fstab_file.readlines()
        res = filter(lambda x: line_pattern in x, flines)
        fstab_file.close()
        if len(res) == 0:
            echo_code = run('echo "/dev/optvg/optlv    /opt    xfs    defaults    0 0" | tee -a /etc/fstab\n')
            if echo_code.return_code:
                print 'echo "/dev/optvg/optlv    /opt    xfs    default    0 0" | tee -a /etc/fstab failed'
                exit()            
        else:
            print 'mount option already exits in /etc/fstab'
            
        mount_code = run('mount /opt')
        if mount_code.return_code:
            print 'mount /opt failed'
            exit()

if __name__ == '__main__':
    detect_disk()
    install_lvm2()
    create_lvm()
    mount_lvm()