#!/usr/bin/env python
from fabric.api import *

env.user = 'root'
env.hosts = ['10.192.9.124']
# env.passwords = {'root@10.192.9.124' : 'admin123sh'}
def echo():
    with settings(host_string=env.hosts, user=env.user):
        run("echo -n 'Hello, you are tuned to Tecmint ' ")
def deploy_lamp():
    with settings(host_string="10.192.9.124", user = "root"):
        run ("yum install -y httpd mariadb-server php php-mysql")

if __name__ == '__main__':
    deploy_lamp()