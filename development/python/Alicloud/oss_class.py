import json
import oss2
import re
import os, errno
import sys
import platform
from itertools import islice
from tabulate import tabulate
from oss2.exceptions import ServerError, RequestError

class ossManager():
    
    ENDPOINT_LIST = ['http://oss-cn-hangzhou.aliyuncs.com',
                     'http://oss-cn-shanghai.aliyuncs.com',
                     'http://oss-cn-qingdao.aliyuncs.com',
                     'http://oss-cn-beijing.aliyuncs.com',
                     'http://oss-cn-zhangjiakou.aliyuncs.com',
                     'http://oss-cn-shenzhen.aliyuncs.com',
                     'http://oss-cn-hongkong.aliyuncs.com',
                     'http://oss-us-west-1.aliyuncs.com',
                     'http://oss-us-east-1.aliyuncs.com',
                     'http://oss-ap-southeast-1.aliyuncs.com',
                     'http://oss-ap-southeast-2.aliyuncs.com',
                     'http://oss-ap-northeast-1.aliyuncs.com',
                     'http://oss-eu-central-1.aliyuncs.com',
                     'http://oss-me-east-1.aliyuncs.com'
                    ]

    PERMISSION_LIST = ['oss2.BUCKET_ACL_PRIVATE',
                       'oss2.BUCKET_ACL_PUBLIC_READ',
                       'oss2.BUCKET_ACL_PUBLIC_READ_WRITE'
                      ]

    def __init__(self, myendpoint, authfile='ali_auth.json', cfgfile='oss_template.json'):
        self.myendpoint = myendpoint
        self.authfile = authfile
        self.ali_auth_json = json.load(open(self.authfile))
        self.access_key_id = self.ali_auth_json['AccessKeyId']
        self.access_key_secret = self.ali_auth_json['AccessKeySecret']
        
        self.auth = oss2.Auth(self.access_key_id, self.access_key_secret)
        self.service = oss2.Service(self.auth, self.myendpoint)

        self.cfgfile = cfgfile
        self.cfgfile_json = json.load(open(self.cfgfile))

    def _getAllBucket(self):
        return [b.name for b in oss2.BucketIterator(self.service)]
 
    def _get_filepaths(self, directory):
        files_list = []
     
        for path, subdirs, files in os.walk(directory):
            for name in files:
                files_list.append(os.path.join(path, name))
 
        return files_list

    def _listFilesinBucket(self, bucketname):
        files_list = []
        bucket = oss2.Bucket(self.auth, self.myendpoint, bucketname)
        for b in islice(oss2.ObjectIterator(bucket), None):
            files_list.append(b.key)
        return files_list
  
    @staticmethod
    def progress(count, total, status=''):
        bar_len = 60
        filled_len = int(round(bar_len * count / float(total)))
     
        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)
     
        sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
        sys.stdout.flush()

    def showEndPoint(self):
        for ossendpoint in ossManager.ENDPOINT_LIST:
            print ossendpoint

    def showACL(self):
        for acl in ossManager.PERMISSION_LIST:
            print acl
    
    def showAllBucket(self):
        bucketlist = self._getAllBucket()
        unit_dict = {}
        whole_list = []
        for i in range(len(bucketlist)):
            unit_dict['Bucket Name'] = bucketlist[i]
            whole_list.append(unit_dict)
            unit_dict = {}
 
        print tabulate(whole_list, headers='keys', tablefmt='psql').encode('utf8')
     
    def createBucket(self):
        pattern = re.compile('^[a-z0-9][a-z0-9\-]{1,61}[a-z0-9]$')
        if self.cfgfile_json['createBucket']['m_bucketName']:
            bucket_tuple = map(None, self.cfgfile_json['createBucket']['m_bucketName'],
                                     self.cfgfile_json['createBucket']['permission']
                              )            
            for unit_tuple in bucket_tuple:
                if pattern.match(unit_tuple[0]):
                    try:
                        bucket = oss2.Bucket(self.auth, self.myendpoint, unit_tuple[0])
                        bucket.create_bucket(unit_tuple[1])
                    except ServerError as e:
                        return e.status
#                         print e
                    except RequestError as e:
#                         print e
                        return e.status
                else:
                    return 0
        else:
            return 0
 
    def deleteBucket(self):
        if self.cfgfile_json['deleteBucket']['m_bucketName']:
            for b in self.cfgfile_json['deleteBucket']['m_bucketName']:
                bucket = oss2.Bucket(self.auth, self.myendpoint, b)
                try:
                    bucket.delete_bucket()
                except oss2.exceptions.BucketNotEmpty:
                    print('bucket is not empty.')
                    return 0
                except oss2.exceptions.NoSuchBucket:
                    print('bucket does not exist')
                    return 0
        else:
            print 'template bucket list is empty'
            return 0
 
    def getBucketACL(self):
        if self.cfgfile_json['getBucketACL']['m_bucketName']:
            for b in self.cfgfile_json['getBucketACL']['m_bucketName']:
                bucket = oss2.Bucket(self.auth, self.myendpoint, b)
                print bucket.get_bucket_acl().acl
        else:
            print 'template bucket list is empty'
            return 0
 
    def setBucketACL(self):
        if self.cfgfile_json['setBucketACL']['m_bucketName'] and self.cfgfile_json['setBucketACL']['permission']:
            bucketacl_tuple = map(None, self.cfgfile_json['setBucketACL']['m_bucketName'],
                                        self.cfgfile_json['setBucketACL']['permission'])
            for unit_tuple in bucketacl_tuple:                
                bucket = oss2.Bucket(self.auth, self.myendpoint, unit_tuple[0])
                if unit_tuple[1] is None:
                    print 'please set bucket acl for specific bucket'
                    return 0
                else:
                    if unit_tuple[1] == 'oss2.BUCKET_ACL_PRIVATE':
                        bucket.put_bucket_acl(oss2.BUCKET_ACL_PRIVATE)
                    elif unit_tuple[1] == 'oss2.BUCKET_ACL_PUBLIC_READ':
                        bucket.put_bucket_acl(oss2.BUCKET_ACL_PUBLIC_READ)
                    else:
                        bucket.put_bucket_acl(oss2.BUCKET_ACL_PUBLIC_READ_WRITE)
        else:
            print 'Both values of bucket name and bucket acl in template should not be empty'
            return 0
 
    def listFilesinBucket(self):
        if self.cfgfile_json['listFilesinBucket']['m_bucketName']:
            bucket = oss2.Bucket(self.auth, self.myendpoint, self.cfgfile_json['listFilesinBucket']['m_bucketName'])
            for b in islice(oss2.ObjectIterator(bucket), None):
                print(b.key)
        else:
            print 'bucket name in template is not set'
         
    def uploadFile(self):
        if self.cfgfile_json['uploadFile']['m_bucketName'] and self.cfgfile_json['uploadFile']['files']:
            for f in self.cfgfile_json['uploadFile']['files']:
                bucket = oss2.Bucket(self.auth, self.myendpoint, self.cfgfile_json['uploadFile']['m_bucketName'])
                bucket.put_object_from_file(f, f)
        else:
            print 'Both values of bucket name and file in template should not be empty'
   
    def createDir(self):
        if self.cfgfile_json['createDir']['m_bucketName'] and self.cfgfile_json['createDir']['dirs']:
            for f in self.cfgfile_json['createDir']['dirs']:
#                 print f+'/1.jpg'
                bucket = oss2.Bucket(self.auth, self.myendpoint, self.cfgfile_json['createDir']['m_bucketName'])
                bucket.put_object_from_file(f+'/session.xts', f+'/session.xts')
        else:
            print 'Both values of bucket name and file in template should not be empty'

    def deleteOSSObject(self):
        if self.cfgfile_json['deleteOSSObject']['m_bucketName']:
            objects_list = self._listFilesinBucket(self.cfgfile_json['deleteOSSObject']['m_bucketName'])
            bucket = oss2.Bucket(self.auth, self.myendpoint, self.cfgfile_json['deleteOSSObject']['m_bucketName'])
            if self.cfgfile_json['deleteOSSObject']['OSSDirectory']:
                for oss_dir in self.cfgfile_json['deleteOSSObject']['OSSDirectory']:
                    match_list = [s for s in objects_list if oss_dir in s]
                    if match_list:
                        for item in match_list:
                            print 'deleting %s' % item
                            bucket.delete_object(item)
                    else:
                        print 'The specified oss directory does not exist in this bucket %s' % self.cfgfile_json['deleteOSSObject']['m_bucketName']
                        return 0
            elif self.cfgfile_json['deleteOSSObject']['OSSObject']:
                for oss_object in self.cfgfile_json['deleteOSSObject']['OSSObject']:
                    match_list = [s for s in objects_list if oss_object in s]                       
                    if match_list:
                        for item in match_list:
                            print 'deleting %s' % item
                            bucket.delete_object(item)
                    else:
                        print 'The specified oss objects do not exist in this bucket %s' % self.cfgfile_json['deleteOSSObject']['m_bucketName']
                        return 0
            else:
                print 'Either OSS directory or oss objects need to be specified'
                return 0
        else:
            print 'To delete an oss object, bucket name is mandatory'
            return 0
          
    def localDirtoBucket(self):
        pattern = re.compile('^[a-z0-9][a-z0-9\-]{1,61}[a-z0-9]$')
        if self.cfgfile_json['localDirtoBucket']['bucketName']:
            if pattern.match(os.path.basename(self.cfgfile_json['localDirtoBucket']['bucketName'])):
                bucket = oss2.Bucket(self.auth, self.myendpoint, self.cfgfile_json['localDirtoBucket']['bucketName'])
                if self.cfgfile_json['localDirtoBucket']['m_localDir']:
                    leading_pattern = '^.*'+os.path.basename(self.cfgfile_json['localDirtoBucket']['m_localDir'])
                    allfiles_list = self._get_filepaths(self.cfgfile_json['localDirtoBucket']['m_localDir'])
                    for uploadfilename in allfiles_list:
                        filenamewithoutleading = re.sub(re.compile(leading_pattern), os.path.basename(self.cfgfile_json['localDirtoBucket']['m_localDir']), uploadfilename)
                        upload_file = filenamewithoutleading.replace('\\', '/') if platform.system() == 'Windows' else filenamewithoutleading
                        bucket.put_object_from_file(upload_file, uploadfilename, headers=self.cfgfile_json['localDirtoBucket']['httpHeader'], progress_callback=ossManager.progress)
                else:
                    print 'Please specific a local directory to sync to bucket ' + self.cfgfile_json['localDirtoBucket']['bucketName']
                    return 0
            else:
                print 'bucket name does not match naming rules'
                return 0
        else:
            if pattern.match(os.path.basename(self.cfgfile_json['localDirtoBucket']['m_localDir'])):
                try:
                    bucket = oss2.Bucket(self.auth, self.myendpoint, os.path.basename(self.cfgfile_json['localDirtoBucket']['m_localDir']))
                    bucket.create_bucket(os.path.basename(self.cfgfile_json['localDirtoBucket']['permission']))
                except ServerError as e:
                    print e
                    return 0
                except RequestError as e:
                    print e
                    return 0
 
                leading_pattern = '^.*'+os.path.basename(self.cfgfile_json['localDirtoBucket']['m_localDir'])
                allfiles_list = self._get_filepaths(self.cfgfile_json['localDirtoBucket']['m_localDir'])
                for uploadfilename in allfiles_list:
                    filenamewithoutleading = re.sub(re.compile(leading_pattern), '', uploadfilename).strip('\\')
                    upload_file = filenamewithoutleading.replace('\\', '/') if platform.system() == 'Windows' else filenamewithoutleading
                    bucket.put_object_from_file(upload_file, uploadfilename, headers=self.cfgfile_json['localDirtoBucket']['httpHeader'], progress_callback=ossManager.progress)
            else:
                print 'There is no specified bucket for syncing folder, the bucket will be created as the name of folder, but folder name does not match bucket naming rules, rename the folder name to solve this issue'
                return 0
   
    def buckettoLocalDir(self):
        if self.cfgfile_json['buckettoLocalDir']['bucketName']:
            bucket = oss2.Bucket(self.auth, self.myendpoint, self.cfgfile_json['buckettoLocalDir']['bucketName'])
            if self.cfgfile_json['buckettoLocalDir']['OSSDirectory']:
                if self.cfgfile_json['buckettoLocalDir']['m_localDir']:
                    match_list = [s for s in self._listFilesinBucket(self.cfgfile_json['buckettoLocalDir']['bucketName']) if self.cfgfile_json['buckettoLocalDir']['OSSDirectory'] in s]
                    for item in match_list:
                        dir_position = item.split('/').index(self.cfgfile_json['buckettoLocalDir']['OSSDirectory'])
                        fixed_path = '/'.join(item.split('/')[dir_position:])
                        file_path = self.cfgfile_json['buckettoLocalDir']['m_localDir']+os.sep+fixed_path if platform.system() != 'Windows' else self.cfgfile_json['buckettoLocalDir']['m_localDir']+os.sep+fixed_path.replace('/', os.sep)
                        if item.split('/')[-1] == '':
                            try:
                                os.makedirs(file_path)
                            except OSError as e:
                                pass
                        else:
                            sub_fixed_path = os.sep.join(fixed_path.split('/')[0:len(fixed_path.split('/'))-1])
                            sub_file_path = os.sep.join(fixed_path.split('/'))
                            dir_path = self.cfgfile_json['buckettoLocalDir']['m_localDir']+os.sep+sub_fixed_path
                            try:
                                os.makedirs(dir_path)
                            except OSError as e:
                                pass
                            final_path = self.cfgfile_json['buckettoLocalDir']['m_localDir']+os.sep+sub_file_path
                            bucket.get_object_to_file(item, final_path)
                else:
                    current_dir = os.path.dirname(os.path.realpath(__file__))
                    match_list = [s for s in self._listFilesinBucket(self.cfgfile_json['buckettoLocalDir']['bucketName']) if self.cfgfile_json['buckettoLocalDir']['OSSDirectory'] in s]
                    for item in match_list:
                        dir_position = item.split('/').index(self.cfgfile_json['buckettoLocalDir']['OSSDirectory'])
                        fixed_path = '/'.join(item.split('/')[dir_position:])
                        file_path = current_dir+os.sep+fixed_path if platform.system() != 'Windows' else current_dir+os.sep+fixed_path.replace('/', os.sep)
                        if item.split('/')[-1] == '':
                            try:
                                os.makedirs(file_path)
                            except OSError as e:
                                pass
                        else:
                            sub_fixed_path = os.sep.join(fixed_path.split('/')[0:len(fixed_path.split('/'))-1])
                            sub_file_path = os.sep.join(fixed_path.split('/'))
                            dir_path = current_dir+os.sep+sub_fixed_path
                            try:
                                os.makedirs(dir_path)
                            except OSError as e:
                                pass
                            final_path = current_dir+os.sep+sub_file_path
                            bucket.get_object_to_file(item, final_path)
        else:
            pass

if __name__ =='__main__':
    myossmanager = ossManager('http://oss-cn-hangzhou.aliyuncs.com')
#     print myossmanager._getAllBucket()
#     myossmanager.showAllBucket()
#     myossmanager.createBucket()
#     myossmanager.deleteBucket()
#     myossmanager.getBucketACL()
#     myossmanager.setBucketACL()
#     myossmanager.uploadFile()
#     myossmanager.createDir()
#     myossmanager.localDirtoBucket()
#     myossmanager.listFilesinBucket()
#     myossmanager.buckettoLocalDir()
    myossmanager.deleteOSSObject()