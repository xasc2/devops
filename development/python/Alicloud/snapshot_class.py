# -*- coding: utf-8 -*-

import json, re
from tabulate import tabulate
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ServerException

class Snapshot():

    HTTPERRORCODELIST = ['0', '400', '403', '404', '500']

    def __init__(self, regionid, pagesize=10, authfile='ali_auth.json', cfgfile='snapshot_template.json'):
        self.regionid = regionid
        self.pagesize = pagesize
        
        self.authfile = authfile
        self.ali_auth_json = json.load(open(self.authfile))
        self.access_key_id = self.ali_auth_json['AccessKeyId']
        self.access_key_secret = self.ali_auth_json['AccessKeySecret']
        self.client = AcsClient(
            str(self.access_key_id), 
            str(self.access_key_secret),
            str(self.regionid)
        )
        
        self.cfgfile = cfgfile
        self.cfgfile_json = json.load(open(self.cfgfile))

    def _fetchDiskInfo(self):
        from aliyunsdkecs.request.v20140526 import DescribeDisksRequest
        dd_request = DescribeDisksRequest.DescribeDisksRequest()
        try:
            dd_response = self.client.do_action_with_exception(dd_request)
        except ServerException as e:
            return e.get_http_status()
        json_result = json.loads(dd_response)
#         print json_result

        if json_result['TotalCount'] == 0:
            print 'Total count of Disk is 0'
            return 0
        elif json_result['TotalCount'] > self.pagesize:
            self.pagesize = json_result['TotalCount']
            dd_request = DescribeDisksRequest.DescribeDisksRequest()
            dd_request.set_PageSize(self.pagesize)
            try:
                dd_response = self.client.do_action_with_exception(dd_request)
            except ServerException as e:
                return e.get_http_status()
            json_result = json.loads(dd_response)
            return json_result['Disks']['Disk']
        else:
            return json_result['Disks']['Disk']
    
    def _fetchSnapshotInfo(self):
        from aliyunsdkecs.request.v20140526 import DescribeSnapshotsRequest
        des_request = DescribeSnapshotsRequest.DescribeSnapshotsRequest()
        try:
            des_response = self.client.do_action_with_exception(des_request)
        except ServerException as e:
            return e.get_http_status()
        json_result = json.loads(des_response)

        if json_result['TotalCount'] == 0:
            print 'Total count of snapshot is 0'
            return 0
        elif json_result['TotalCount'] > self.pagesize:
            self.pagesize = json_result['TotalCount']
            des_request = DescribeSnapshotsRequest.DescribeSnapshotsRequest()
            des_request.set_PageSize(self.pagesize)
            try:
                des_response = self.client.do_action_with_exception(des_request)
            except ServerException as e:
                return e.get_http_status()
            json_result = json.loads(des_response)
            return json_result['Snapshots']['Snapshot']
        else:
            return json_result['Snapshots']['Snapshot']

    def _fetchSnapshotPolicyExInfo(self):
        from aliyunsdkecs.request.v20140526 import DescribeAutoSnapshotPolicyExRequest
        daspex_request = DescribeAutoSnapshotPolicyExRequest.DescribeAutoSnapshotPolicyExRequest()
        try:
            daspex_response = self.client.do_action_with_exception(daspex_request)
        except ServerException as e:
            return e.get_http_status()
        json_result = json.loads(daspex_response)
        
        if json_result['TotalCount'] == 0:
            return 0
        elif json_result['TotalCount'] > self.pagesize:
            self.pagesize = json_result['TotalCount']
            daspex_request = DescribeAutoSnapshotPolicyExRequest.DescribeAutoSnapshotPolicyExRequest()
            daspex_request.set_PageSize(self.pagesize)
            try:
                daspex_response = self.client.do_action_with_exception(daspex_request)
            except ServerException as e:
                return e.get_http_status()
            json_result = json.loads(daspex_response)
            return json_result['AutoSnapshotPolicies']['AutoSnapshotPolicy']
        else:
            return json_result['AutoSnapshotPolicies']['AutoSnapshotPolicy']
    
    def listDisk(self):
        disk_info = self._fetchDiskInfo()
        if disk_info in Snapshot.HTTPERRORCODELIST:
            print 'Error occurs while fetching disk info, error code is {0}'.format(disk_info)
        else:
            unit_list = []
            whole_list = []
            for i in range(len(disk_info)):
                unit_list.append(disk_info[i]['DiskId'])
                unit_list.append(disk_info[i]['DiskName'])
                whole_list.append(unit_list)
                unit_list = []
    
            print tabulate(whole_list, headers=['Disk id', 'Disk name'], tablefmt='psql').encode('utf8')
    
    def querySnapshotId(self):
        snapshotid_list = []
        snapshotid_dict = {}
        snapshot_list = self._fetchSnapshotInfo()
        for i in range(len(snapshot_list)):
            snapshotid_dict = dict(SnapshotId=snapshot_list[i]['SnapshotId'])
            snapshotid_list.append(snapshotid_dict)
        print tabulate(snapshotid_list, headers='keys', tablefmt='psql').encode('utf8')
    
    def createSnapshot(self):
        if self.cfgfile_json['createSnapshot']['m_DiskId'] and self.cfgfile_json['createSnapshot']['m_SnapshotName'] and (len(self.cfgfile_json['createSnapshot']['m_SnapshotName']) != len(set(self.cfgfile_json['createSnapshot']['m_SnapshotName']))):
            snapshot_tuple = map(None, self.cfgfile_json['createSnapshot']['m_DiskId'],
                                       self.cfgfile_json['createSnapshot']['m_SnapshotName'], 
                                       self.cfgfile_json['createSnapshot']['Description'],
                                       self.cfgfile_json['createSnapshot']['ClientToken']
                                )
            from aliyunsdkecs.request.v20140526 import CreateSnapshotRequest
            for i in range(len(snapshot_tuple)):
                csnap_request = CreateSnapshotRequest.CreateSnapshotRequest()
                csnap_request.set_DiskId(snapshot_tuple[i][0])
                if snapshot_tuple[i][1]:
                    csnap_request.set_SnapshotName(snapshot_tuple[i][1])
                else:
                    print 'snapshot name is mandatory'
                csnap_request.set_Description(snapshot_tuple[i][2])
                try:
                    csnap_response = self.client.do_action_with_exception(csnap_request)
                except ServerException as e:
                    if e.get_error_code() == 'IncorrectDiskStatus.CreatingSnapshot':
                        print 'The snapshot of spicified disk id is still being creating, please try it later.'
                        return 1
                    else:
                        print e
                        return 1
                json_result = json.loads(csnap_response)
                print json_result
        else:
            print 'Make sure every Disk ID and related Snapshot name is mandatory and not duplicated'
    
    def deleteSnapshot(self, snapshotnamelist=None):
        from aliyunsdkecs.request.v20140526 import DeleteSnapshotRequest
        if snapshotnamelist is None:
            if self.cfgfile_json['deleteSnapshot']['SnapshotId']:
                for i in range(len(self.cfgfile_json['deleteSnapshot']['SnapshotId'])):
                    dsnap_request = DeleteSnapshotRequest.DeleteSnapshotRequest()
                    dsnap_request.set_SnapshotId(self.cfgfile_json['deleteSnapshot']['SnapshotId'][i])
                    try:
                        dsnap_response = self.client.do_action_with_exception(dsnap_request)
                    except ServerException as e:
                        print e
            else:
                snapshot_list = self._fetchSnapshotInfo()
                for i in range(len(self.cfgfile_json['deleteSnapshot']['m_SnapshotName'])):
                    count = 0
                    for j in range(len(snapshot_list)):
                        if self.cfgfile_json['deleteSnapshot']['m_SnapshotName'][i] in snapshot_list[j].values():
                            count += 1
                            dsnap_request = DeleteSnapshotRequest.DeleteSnapshotRequest()
                            dsnap_request.set_SnapshotId(snapshot_list[j]['SnapshotId'])
                            try:
                                dsnap_response = self.client.do_action_with_exception(dsnap_request)
                                json_result = json.loads(dsnap_response)
                                print json_result
                                return 0
                            except ServerException as e:
                                print e
                                return 0
                        else:
                            pass
                    if count == 0:
                        print 'SnapshotName in template does not exist'
                        return 0
#                 print tabulate(snapshot_list, headers='keys', tablefmt='psql').encode('utf8')                
        else:
            snapshotname_list = snapshotnamelist
            snapshot_list = self._fetchSnapshotInfo()
            for i in range(len(snapshotname_list)):
                count = 0
                for j in range(len(snapshot_list)):
                    if snapshotname_list[i] in snapshot_list[j].values():
                        count += 1
                        print snapshot_list[j]
                        dsnap_request = DeleteSnapshotRequest.DeleteSnapshotRequest()
                        dsnap_request.set_SnapshotId(snapshot_list[j]['SnapshotId'])
                        try:
                            dsnap_response = self.client.do_action_with_exception(dsnap_request)
                            return json.loads(dsnap_response)
                        except ServerException as e:
                            print e
                            return 0
                    else:
                        pass
                    if count == 0:
                        print 'SnapshotName in template does not exist'
                        return 0

    def listSnapshot(self):
        snapshotid_list = []
        snapshotid_dict = {}
        snapshot_list = self._fetchSnapshotInfo()
        for i in range(len(snapshot_list)):
            snapshotid_dict = dict(SourceDiskId=snapshot_list[i]['SourceDiskId'], SnapshotId=snapshot_list[i]['SnapshotId'], SnapshotName=snapshot_list[i]['SnapshotName'])
#             print snapshotid_dict
            snapshotid_list.append(snapshotid_dict)
        print tabulate(snapshotid_list, headers='keys', tablefmt='psql').encode('utf8')
    
    def autoCreateSnapshotPolicy(self):
        pattern = re.compile('^[\w][\w\d_-]{0,124}[\w\d]$')
        if (self.cfgfile_json['CreateAutoSnapshotPolicy']['m_AutuSnapshotPolicyName'] and
            self.cfgfile_json['CreateAutoSnapshotPolicy']['m_TimePoints'] and
            self.cfgfile_json['CreateAutoSnapshotPolicy']['m_RepeatWeekdays'] and
            self.cfgfile_json['CreateAutoSnapshotPolicy']['m_RetentionDays']):
            policy_tuple = map(None, self.cfgfile_json['CreateAutoSnapshotPolicy']['m_AutuSnapshotPolicyName'],
                                     self.cfgfile_json['CreateAutoSnapshotPolicy']['m_TimePoints'],
                                     self.cfgfile_json['CreateAutoSnapshotPolicy']['m_RepeatWeekdays'],
                                     self.cfgfile_json['CreateAutoSnapshotPolicy']['m_RetentionDays']
                              )
            from aliyunsdkecs.request.v20140526 import CreateAutoSnapshotPolicyRequest
            for i in range(len(policy_tuple)):
                if pattern.match(policy_tuple[i][0]):
                    casp_request = CreateAutoSnapshotPolicyRequest.CreateAutoSnapshotPolicyRequest()
                else:
                    print 'policy name is not match the requirement.'
                casp_request.set_autoSnapshotPolicyName(policy_tuple[i][0])
                casp_request.set_timePoints([x.encode('utf-8') for x in policy_tuple[i][1]])
                casp_request.set_repeatWeekdays([x.encode('utf-8') for x in policy_tuple[i][2]])
                casp_request.set_retentionDays(policy_tuple[i][3])
                 
                try:
                    casp_response = self.client.do_action_with_exception(casp_request)
                    json_result = json.loads(casp_response)
                    return json_result
                except ServerException as e:
                    print e
                    return 0               
        else:
            print 'values of AutuSnapshotPolicyName, TimePoints, RepeatWeekdays, RetentionDays are all mandatory'
    
    def deleteSnapshotPolicy(self, policynamelist=None):
        from aliyunsdkecs.request.v20140526 import DeleteAutoSnapshotPolicyRequest
        if policynamelist is None:
            if self.cfgfile_json['DeleteAutoSnapshotPolicy']['AutoSnapshotPolicyId']:
                for i in range(len(self.cfgfile_json['DeleteAutoSnapshotPolicy']['AutoSnapshotPolicyId'])):
                    dasp_request = DeleteAutoSnapshotPolicyRequest.DeleteAutoSnapshotPolicyRequest()
                    dasp_request.set_autoSnapshotPolicyId(self.cfgfile_json['DeleteAutoSnapshotPolicy']['AutoSnapshotPolicyId'][i])
                    try:
                        dasp_response = self.client.do_action_with_exception(dasp_request)
                        return json.loads(dasp_response)
                    except ServerException as e:
                        print e
                        return 0
            else:
                snapshotpolicy_list = self._fetchSnapshotPolicyExInfo()
                for i in range(len(self.cfgfile_json['DeleteAutoSnapshotPolicy']['m_AutuSnapshotPolicyName'])):
                    count = 0
                    for j in range(len(snapshotpolicy_list)):
                        if self.cfgfile_json['DeleteAutoSnapshotPolicy']['m_AutuSnapshotPolicyName'][i] in snapshotpolicy_list[j].values():
                            count += 1
                            dasp_request = DeleteAutoSnapshotPolicyRequest.DeleteAutoSnapshotPolicyRequest()
                            dasp_request.set_autoSnapshotPolicyId(snapshotpolicy_list[j]['AutoSnapshotPolicyId'])
                            try:
                                dasp_response = self.client.do_action_with_exception(dasp_request)
                                return json.loads(dasp_response)
                            except ServerException as e:
                                print e
                                return 0
                        else:
                            pass
                    if count == 0:
                        print 'AutuSnapshotPolicyName in template does not exist'
                        return 0
#                     print tabulate(snapshot_list, headers='keys', tablefmt='psql').encode('utf8') 
        else:
            snapshotpolicyname_list = policynamelist
            snapshotpolicy_list = self._fetchSnapshotPolicyInfo()
            for i in range(len(snapshotpolicyname_list)):
                count = 0
                for j in range(len(snapshotpolicy_list)):
                    if snapshotpolicyname_list[i] in snapshotpolicy_list[j].values():
                        count += 1
                        print snapshotpolicy_list[j]
                        dasp_request = DeleteAutoSnapshotPolicyRequest.DeleteAutoSnapshotPolicyRequest()
                        dasp_request.set_autoSnapshotPolicyId(snapshotpolicy_list[j]['AutoSnapshotPolicyId'])
                        try:
                            dasp_response = self.client.do_action_with_exception(dasp_request)
                            return json.loads(dasp_response)
                        except ServerException as e:
                            print e
                            return 0
                    else:
                        pass
                    if count == 0:
                        print 'AutuSnapshotPolicyName in template does not exist'
                        return 0

    def updateSnapshotPolicy(self):
        autosnappolicy_tuple = map(None, self.cfgfile_json['updateSnapshotPolicy']['AutoSnapshotPolicyId'],
                                         self.cfgfile_json['updateSnapshotPolicy']['m_AutoSnapshotPolicyName'],
                                         self.cfgfile_json['updateSnapshotPolicy']['TimePoints'],
                                         self.cfgfile_json['updateSnapshotPolicy']['RepeatWeekdays'],
                                         self.cfgfile_json['updateSnapshotPolicy']['RetentionDays']
                               )
        snapshotpolicy_list = self._fetchSnapshotPolicyExInfo()
        from aliyunsdkecs.request.v20140526 import ModifyAutoSnapshotPolicyExRequest
        if self.cfgfile_json['updateSnapshotPolicy']['AutoSnapshotPolicyId']:
            for i in range(len(autosnappolicy_tuple)):
                for j in range(len(snapshotpolicy_list)):
                    if autosnappolicy_tuple[i][0] in snapshotpolicy_list[j].values():
                        usp_request = ModifyAutoSnapshotPolicyExRequest.ModifyAutoSnapshotPolicyExRequest()
                        usp_request.set_autoSnapshotPolicyId(autosnappolicy_tuple[i][0])
                        usp_request.set_autoSnapshotPolicyName(autosnappolicy_tuple[i][1])
                        usp_request.set_timePoints([x.encode('utf-8') for x in autosnappolicy_tuple[i][2]])
                        usp_request.set_repeatWeekdays([x.encode('utf-8') for x in autosnappolicy_tuple[i][3]])
                        usp_request.set_retentionDays(autosnappolicy_tuple[i][4])
                        try:
                            usp_response = self.client.do_action_with_exception(usp_request)
                            return json.loads(usp_response)
                        except ServerException as e:
                            print e
                            return 0                    
                    else:
                        pass
#                         print 'policy id ' + str(autosnappolicy_tuple[i][0]) + ' does not exist'

        else:
            if self.cfgfile_json['updateSnapshotPolicy']['m_AutoSnapshotPolicyName']:
                for i in range(len(autosnappolicy_tuple)):
                    if autosnappolicy_tuple[i][1] is None:
                        print 'Policy can not be update with neither policy name nor policy id 1'
                        return 0
                    else:
                        count = 0
                        for j in range(len(snapshotpolicy_list)):
                            if autosnappolicy_tuple[i][1] in snapshotpolicy_list[j].values():
                                count += 1
                                usp_request = ModifyAutoSnapshotPolicyExRequest.ModifyAutoSnapshotPolicyExRequest()
                                usp_request.set_autoSnapshotPolicyId(snapshotpolicy_list[j]['AutoSnapshotPolicyId'])
                                usp_request.set_timePoints([x.encode('utf-8') for x in autosnappolicy_tuple[i][2]])
                                usp_request.set_repeatWeekdays([x.encode('utf-8') for x in autosnappolicy_tuple[i][3]])
                                usp_request.set_retentionDays(autosnappolicy_tuple[i][4])
                                try:
                                    usp_response = self.client.do_action_with_exception(usp_request)
                                    return json.loads(usp_response)
                                except ServerException as e:
                                    print e
                                    return 0
                            else:
                                pass
                        if count == 0:
                            print 'AutoSnapshotPolicyName in template does not exist 1'
                            return 0
            else:
                print 'Policy can not be update with neither policy name nor policy id 2'
                return 0

    def listSnapshotPolicyEx(self):
        print tabulate(self._fetchSnapshotPolicyExInfo(), headers='keys', tablefmt='psql').encode('utf8')

    
    def executeSnapshotPolicy(self):
        from aliyunsdkecs.request.v20140526 import ApplyAutoSnapshotPolicyRequest
        executeSnapshotPolicy_tuple = map(None, self.cfgfile_json['executeSnapshotPolicy']['m_AutoSnapshotPolicyId'],
                                                self.cfgfile_json['executeSnapshotPolicy']['AutoSnapshotPolicyName'],
                                                self.cfgfile_json['executeSnapshotPolicy']['m_DiskIds'])
        snapshotpolicy_list = self._fetchSnapshotPolicyExInfo()
        if self.cfgfile_json['executeSnapshotPolicy']['m_AutoSnapshotPolicyId']:
            for i in range(len(executeSnapshotPolicy_tuple)):
                count = 0
                for j in range(len(snapshotpolicy_list)):
                    if executeSnapshotPolicy_tuple[i][0] is None:
                        print 'policy id is mandatory to apply on disk id.'
                    else:
                        if executeSnapshotPolicy_tuple[i][0] in snapshotpolicy_list[j].values():
                            count += 1
                            esp_request = ApplyAutoSnapshotPolicyRequest.ApplyAutoSnapshotPolicyRequest()
                            esp_request.set_autoSnapshotPolicyId(executeSnapshotPolicy_tuple[i][0])
                            esp_request.set_diskIds([x.encode('utf-8') for x in executeSnapshotPolicy_tuple[i][2]])
                            try:
                                esp_response = self.client.do_action_with_exception(esp_request)
                                return json.loads(esp_response)
                            except ServerException as e:
                                print e
                                return 0
                        else:
                            pass
                if count == 0:
                    print 'The specified policy id does not exist.'
                    return 0
        else:
            print 'policy id is mandatory to apply on disk id.'
            return 0
                        
                      
    def cancelSnapshotpolicy(self):
        from aliyunsdkecs.request.v20140526 import CancelAutoSnapshotPolicyRequest
        if self.cfgfile_json['cancelSnapshotpolicy']['m_DiskIds']:
            for i in range(len(self.cfgfile_json['cancelSnapshotpolicy']['m_DiskIds'])):
                csp_request = CancelAutoSnapshotPolicyRequest.CancelAutoSnapshotPolicyRequest()
                disk_id = []
                disk_id.append(self.cfgfile_json['cancelSnapshotpolicy']['m_DiskIds'][i])
                csp_request.set_diskIds([x.encode('utf-8') for x in disk_id])
                try:
                    csp_response = self.client.do_action_with_exception(csp_request)
                    return json.loads(csp_response)
                except ServerException as e:
                    print e
                    return 0
        else:
            print 'disk id is mandatory'
            return 0
    
    def __str__(self):
        return ''
    
if __name__ =='__main__':
    mysnapshot = Snapshot('cn-shanghai')
    mysnapshot.listDisk()
#     mysnapshot.createSnapshot()
#     mysnapshot._fetchSnapshotInfo()
#     mysnapshot.querySnapshotId()
#     mysnapshot.deleteSnapshot(['test5'])
#     mysnapshot.listSnapshot()
#     mysnapshot.autoCreateSnapshotPolicy()
#     mysnapshot._fetchSnapshotPolicyInfo()
#     mysnapshot.listSnapshotPolicyEx()
#     print mysnapshot.deleteSnapshotPolicy()
#     mysnapshot.updateSnapshotPolicy()
#     mysnapshot.executeSnapshotPolicy()
#     mysnapshot.cancelSnapshotpolicy()