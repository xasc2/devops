# -*- coding: utf-8 -*-

import json, re
from tabulate import tabulate
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ServerException

class cloudMonitor():
    
    HTTPERRORCODELIST = ['0', '400', '403', '404', '500']
    
    def __init__(self, regionid, pagesize=10, authfile='ali_auth.json', cfgfile='cloudmonitor_template.json'):
        self.regionid = regionid
        self.pagesize = pagesize
        
        self.authfile = authfile
        self.ali_auth_json = json.load(open(self.authfile))
        self.access_key_id = self.ali_auth_json['AccessKeyId']
        self.access_key_secret = self.ali_auth_json['AccessKeySecret']
        self.client = AcsClient(
            str(self.access_key_id), 
            str(self.access_key_secret),
            str(self.regionid)
        )
        
        self.cfgfile = cfgfile
        self.cfgfile_json = json.load(open(self.cfgfile))
        
    
    def _fetchECSInfo(self):
        from aliyunsdkecs.request.v20140526 import DescribeInstancesRequest
        dir_request = DescribeInstancesRequest.DescribeInstancesRequest()
        try:
            dir_response = self.client.do_action_with_exception(dir_request)
        except ServerException as e:
            return e.get_http_status()
        json_result = json.loads(dir_response)
        
        if json_result['TotalCount'] == 0:
            print 'Total count of ESC instance is 0'
            return 0
        elif json_result['TotalCount'] > self.pagesize:
            self.pagesize = json_result['TotalCount']
            dir_request = DescribeInstancesRequest.DescribeInstancesRequest()
            dir_request.set_PageSize(self.pagesize)
            try:
                dir_response = self.client.do_action_with_exception(dir_request)
            except ServerException as e:
                return e.get_http_status()
            json_result = json.loads(dir_response)
            return json_result['Instances']['Instance']
        else:
            return json_result['Instances']['Instance']
    
    def _fetchEIPInfo(self):
        from aliyunsdkecs.request.v20140526 import DescribeEipAddressesRequest
        dea_request = DescribeEipAddressesRequest.DescribeEipAddressesRequest()
        try:
            dea_response = self.client.do_action_with_exception(dea_request)
        except ServerException as e:
            return e.get_http_status()
        json_result = json.loads(dea_response)
#         print json_result

        if json_result['TotalCount'] == 0:
            print 'Total count of allocated IP is 0'
            return 0
        elif json_result['TotalCount'] > self.pagesize:
            self.pagesize = json_result['TotalCount']
            dea_request = DescribeEipAddressesRequest.DescribeEipAddressesRequest()
            dea_request.set_PageSize(self.pagesize)
            try:
                dea_response = self.client.do_action_with_exception(dea_request)
            except ServerException as e:
                return e.get_http_status()
            json_result = json.loads(dea_response)
            return json_result['EipAddresses']['EipAddress']
        else:
            return json_result['EipAddresses']['EipAddress']

    def _fetchDiskInfo(self):
        from aliyunsdkecs.request.v20140526 import DescribeDisksRequest
        dd_request = DescribeDisksRequest.DescribeDisksRequest()
        try:
            dd_response = self.client.do_action_with_exception(dd_request)
        except ServerException as e:
            return e.get_http_status()
        json_result = json.loads(dd_response)
#         print json_result

        if json_result['TotalCount'] == 0:
            print 'Total count of Disk is 0'
            return 0
        elif json_result['TotalCount'] > self.pagesize:
            self.pagesize = json_result['TotalCount']
            dd_request = DescribeDisksRequest.DescribeDisksRequest()
            dd_request.set_PageSize(self.pagesize)
            try:
                dd_response = self.client.do_action_with_exception(dd_request)
            except ServerException as e:
                return e.get_http_status()
            json_result = json.loads(dd_response)
            return json_result['Disks']['Disk']
        else:
            return json_result['Disks']['Disk']

    def listECSInstance(self):
        instance_info = self._fetchECSInfo()
        if instance_info in cloudMonitor.HTTPERRORCODELIST:
            print 'Error occurs while fetching ecs instance info, error code is {0}'.format(instance_info)
        else:        
            unit_list = []
            whole_list = []
            for i in range(len(instance_info)):
                unit_list.append(instance_info[i]['InstanceId'])
                unit_list.append(instance_info[i]['InstanceName'])
                whole_list.append(unit_list)
                unit_list = []
                
            print tabulate(whole_list, headers=['instance id', 'Instance name'], tablefmt='psql').encode('utf8')

    def listEIPInstance(self):
        eip_info = self._fetchEIPInfo()
        if eip_info in cloudMonitor.HTTPERRORCODELIST:
            print 'Error occurs while fetching eip instance info, error code is {0}'.format(eip_info)
        else:        
            unit_list = []
            whole_list = []
            for i in range(len(eip_info)):
                unit_list.append(eip_info[i]['AllocationId'])
                unit_list.append(eip_info[i]['IpAddress'])
                whole_list.append(unit_list)
                unit_list = []
    
            print tabulate(whole_list, headers=['Allocation id', 'Ip address'], tablefmt='psql').encode('utf8')

    def listDisk(self):
        disk_info = self._fetchDiskInfo()
        if disk_info in cloudMonitor.HTTPERRORCODELIST:
            print 'Error occurs while fetching disk info, error code is {0}'.format(disk_info)
        else:
            unit_list = []
            whole_list = []
            for i in range(len(disk_info)):
                unit_list.append(disk_info[i]['DiskId'])
                unit_list.append(disk_info[i]['DiskName'])
                whole_list.append(unit_list)
                unit_list = []
    
            print tabulate(whole_list, headers=['Disk id', 'Disk name'], tablefmt='psql').encode('utf8')

    def ecsMonitor(self):
        from aliyunsdkecs.request.v20140526 import DescribeInstanceMonitorDataRequest
#         ecsinfo = self._fetchECSInfo()
        if (self.cfgfile_json['ecsMonitor']['m_InstanceId'] and
            self.cfgfile_json['ecsMonitor']['m_StartTime'] and
            self.cfgfile_json['ecsMonitor']['m_EndTime']):
            ecmonitor_tuple = map(None, self.cfgfile_json['ecsMonitor']['m_InstanceId'],
                                        self.cfgfile_json['ecsMonitor']['m_StartTime'],
                                        self.cfgfile_json['ecsMonitor']['m_EndTime'],
                                        self.cfgfile_json['ecsMonitor']['Period']
                                 )
#             print ecmonitor_tuple
            for i in range(len(ecmonitor_tuple)):
                if ecmonitor_tuple[i][0] is not None:
                    pass
                else:
                    print 'Instance id is mandatory'
                    return 0
                if ecmonitor_tuple[i][1] is not None:
                    pass
                else:
                    print 'start date is mandatory'
                    return 0
                if ecmonitor_tuple[i][2] is not None:
                    pass
                else:
                    print 'end date is mandatory'
                    return 0
                
                dimd_request = DescribeInstanceMonitorDataRequest.DescribeInstanceMonitorDataRequest()
                dimd_request.set_InstanceId(ecmonitor_tuple[i][0])
                dimd_request.set_StartTime(ecmonitor_tuple[i][1])
                dimd_request.set_EndTime(ecmonitor_tuple[i][2])
                if ecmonitor_tuple[i][3] is not None:
                    dimd_request.set_Period(ecmonitor_tuple[i][3])
                else:
                    pass
                
                try:
                    dimd_response = self.client.do_action_with_exception(dimd_request)
                    json_result = json.loads(dimd_response)
                    print json_result
                except ServerException as e:
                    print e
                    return 0
    
    def eipMonitor(self):
        from aliyunsdkecs.request.v20140526 import DescribeEipMonitorDataRequest
#         ecsinfo = self._fetchECSInfo()
        if (self.cfgfile_json['eipMonitor']['m_AllocationId'] and
            self.cfgfile_json['eipMonitor']['m_StartTime'] and
            self.cfgfile_json['eipMonitor']['m_EndTime']):
            eipmonitor_tuple = map(None, self.cfgfile_json['eipMonitor']['m_AllocationId'],
                                         self.cfgfile_json['eipMonitor']['m_StartTime'],
                                         self.cfgfile_json['eipMonitor']['m_EndTime'],
                                         self.cfgfile_json['eipMonitor']['Period']
                                 )
#             print ecmonitor_tuple
            for i in range(len(eipmonitor_tuple)):
                if eipmonitor_tuple[i][0] is not None:
                    pass
                else:
                    print 'Allocated id is mandatory'
                    return 0
                if eipmonitor_tuple[i][1] is not None:
                    pass
                else:
                    print 'start date is mandatory'
                    return 0
                if eipmonitor_tuple[i][2] is not None:
                    pass
                else:
                    print 'end date is mandatory'
                    return 0
                
                demd_request = DescribeEipMonitorDataRequest.DescribeEipMonitorDataRequest()
                demd_request.set_AllocationId(eipmonitor_tuple[i][0])
                demd_request.set_StartTime(eipmonitor_tuple[i][1])
                demd_request.set_EndTime(eipmonitor_tuple[i][2])
                if eipmonitor_tuple[i][3] is not None:
                    demd_request.set_Period(eipmonitor_tuple[i][3])
                else:
                    pass
                
                try:
                    demd_response = self.client.do_action_with_exception(demd_request)
                    json_result = json.loads(demd_response)
                    print json_result
                except ServerException as e:
                    print e
                    return 0

    def diskMonitor(self):
        from aliyunsdkecs.request.v20140526 import DescribeDiskMonitorDataRequest
#         ecsinfo = self._fetchECSInfo()
        if (self.cfgfile_json['diskMonitor']['m_DiskId'] and
            self.cfgfile_json['diskMonitor']['m_StartTime'] and
            self.cfgfile_json['diskMonitor']['m_EndTime']):
            diskmonitor_tuple = map(None, self.cfgfile_json['diskMonitor']['m_DiskId'],
                                          self.cfgfile_json['diskMonitor']['m_StartTime'],
                                          self.cfgfile_json['diskMonitor']['m_EndTime'],
                                          self.cfgfile_json['diskMonitor']['Period']
                                   )
#             print ecmonitor_tuple
            for i in range(len(diskmonitor_tuple)):
                if diskmonitor_tuple[i][0] is not None:
                    pass
                else:
                    print 'Disk id is mandatory'
                    return 0
                if diskmonitor_tuple[i][1] is not None:
                    pass
                else:
                    print 'start date is mandatory'
                    return 0
                if diskmonitor_tuple[i][2] is not None:
                    pass
                else:
                    print 'end date is mandatory'
                    return 0
                
                ddmd_request = DescribeDiskMonitorDataRequest.DescribeDiskMonitorDataRequest()
                ddmd_request.set_DiskId(diskmonitor_tuple[i][0])
                ddmd_request.set_StartTime(diskmonitor_tuple[i][1])
                ddmd_request.set_EndTime(diskmonitor_tuple[i][2])
                if diskmonitor_tuple[i][3] is not None:
                    ddmd_request.set_Period(diskmonitor_tuple[i][3])
                else:
                    pass
                
                try:
                    ddmd_response = self.client.do_action_with_exception(ddmd_request)
                    json_result = json.loads(ddmd_response)
                    print json_result
                except ServerException as e:
                    print e
                    return 0

if __name__ =='__main__':
    mycloudmonitor = cloudMonitor('cn-shanghai')
#     mycloudmonitor._fetchECSInfo()
#     mycloudmonitor.listECSInstance()
#     mycloudmonitor.ecsMonitor()
#     mycloudmonitor.eipMonitor()
#     mycloudmonitor._fetchEIPInfo()
#     mycloudmonitor.listEIPInstance()
#     mycloudmonitor._fetchDiskInfo()
    mycloudmonitor.listDisk()
#     mycloudmonitor.diskMonitor()