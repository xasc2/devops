# !/usr/bin/python
# -*- coding: utf-8 -*-

import json
from optparse import OptionParser, OptionGroup
from aliyunsdkecs.request.v20140526 import DescribeRegionsRequest, DescribeInstancesRequest, CreateImageRequest
from aliyunsdkcore import client

def main():
    parser_usage = 'usage: python %prog -r  --> show available regions \n' \
                   'usage: python %prog -z <regionId>  -i --> show instances in specified region \n' \
                   'usage: python %prog -d <InstanceId> -c -n <image name>  -z <regionId>--> create image from specified instance'
    parser = OptionParser(usage=parser_usage)
    
    group = OptionGroup(parser, 'common options') 
    group.add_option('-z', '--regionid', dest='regionid', action='store',  help='-z <regionId> or --regionid <regionId> to specific regionId',)

    group1 = OptionGroup(parser, 'query information options')
    group1.add_option('-r', '--show_region', dest='show_region', action='store_true',  help='show available region',)
    group1.add_option('-i', '--show_instance', dest='show_instance', action='store_true',  help='show available instances',)

    group2 = OptionGroup(parser, 'create image options')     
    group2.add_option('-d', '--instanceid', dest='instanceid', action='store', help='instance id')
    group2.add_option('-c', '--create_image', dest='create_image', action='store_true', help='create image with specified instance id')
    group2.add_option('-n', '--img_name', dest='img_name', action='store', help='image name for new created image')
 
    parser.add_option_group(group)
    parser.add_option_group(group1)
    parser.add_option_group(group2)
 
    (options, args) = parser.parse_args()

    ali_cfg_json = json.load(open('alicfg.json'))   
    access_key_id = ali_cfg_json['AccessKeyId']
    access_key_secret = ali_cfg_json['AccessKeySecret']

    if options.show_region \
       and options.regionid is None \
       and not options.show_instance \
       and options.instanceid is None \
       and not options.create_image \
       and options.img_name is None:

        clt = client.AcsClient(access_key_id, access_key_secret, 'cn-hangzhou')
        region_request = DescribeRegionsRequest.DescribeRegionsRequest()
        region_request.set_accept_format('json')
        region_result = clt.do_action(region_request)
        region_json = json.loads(region_result)
        for i in range(len(region_json['Regions']['Region'])):
            print 'RegionId: ' + region_json['Regions']['Region'][i]['RegionId'] + ' ==> LocalName: ' + region_json['Regions']['Region'][i]['LocalName']

    elif options.show_instance \
       and options.regionid is not None \
       and not options.show_region \
       and options.instanceid is None \
       and not options.create_image \
       and options.img_name is None:
        
        clt = client.AcsClient(access_key_id, access_key_secret, str(options.regionid))
        ins_request = DescribeInstancesRequest.DescribeInstancesRequest()
        ins_request.set_accept_format('json')
        ins_result = clt.do_action(ins_request)
        ins_json = json.loads(ins_result)
        if ins_json['TotalCount'] == 0:
            print 'There are no instances in this regionId'
        else:
            for j in range(ins_json['TotalCount']):
                print 'instance id: ' + ins_json['Instances']['Instance'][j]['InstanceId'] + ' ==> ' + 'instance name: ' + ins_json['Instances']['Instance'][j]['InstanceName']
    
    elif options.instanceid is not None\
       and options.create_image \
       and options.img_name is not None \
       and options.regionid is not None \
       and not options.show_region \
       and not options.show_instance:

        clt = client.AcsClient(access_key_id, access_key_secret, str(options.regionid))
        spins_request = DescribeInstancesRequest.DescribeInstancesRequest()
        spins_request.set_accept_format('json')
        spins_result = clt.do_action(spins_request)
        spins_json = json.loads(spins_result)
        for k in range(len(spins_json['Instances']['Instance'])):
            if str(options.instanceid) == spins_json['Instances']['Instance'][k]['InstanceId']:
                instance_name = spins_json['Instances']['Instance'][k]['InstanceName']
            else:
                pass
                
        
        img_request = CreateImageRequest.CreateImageRequest()
        img_request.set_accept_format('json')
        img_request.set_InstanceId(options.instanceid)
        img_request.set_ImageName(str(instance_name)+'_img')
        img_result = clt.do_action(img_request)
        img_json = json.loads(img_result)
        print img_json
    else:
        parser.print_help()

    print '\n'
    print 'options.show_region: ' + str(options.show_region)
    print 'options.regionid:' + str(options.regionid)
    print 'options.show_instance: ' + str(options.show_instance) 
    print 'options.instanceid: ' + str(options.instanceid)
    print 'options.create_image:' + str(options.create_image)
    print 'options.img_name: ' + str(options.img_name)

if __name__ == '__main__':
    main()