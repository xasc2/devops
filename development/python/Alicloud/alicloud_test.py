#!/usr/bin/python 
# -*- coding: utf-8 -*-

import json
from aliyunsdkcore import client

#ram account afk_mgmt
clt = client.AcsClient('xxxxxxxxxxxxxxxx', 'xxxxxxxxxxxxxxxxxxxxxxxxxx', 'cn-shenzhen')

#main account worldgnscloud_afk
# clt = client.AcsClient('xxxxxxxxxxxxxx', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'cn-shenzhen')

# from aliyunsdkecs.request.v20140526 import DescribeVpcsRequest
# vpc_request = DescribeVpcsRequest.DescribeVpcsRequest()
# vpc_request.set_accept_format('json')
# vpc_result = clt.do_action(vpc_request)
# json_result = json.loads(vpc_result)
# print json_result
# for i in range(len(json_result['Vpcs']['Vpc'])):
#     print json_result['Vpcs']['Vpc'][i]['VpcId']

# from aliyunsdkecs.request.v20140526 import DescribeVRoutersRequest
# vr_request = DescribeVRoutersRequest.DescribeVRoutersRequest()
# vr_request.set_accept_format('json')
# vr_result = clt.do_action(vr_request)
# json_result = json.loads(vr_result)
# print json_result

# from aliyunsdkecs.request.v20140526 import DescribeRouteTablesRequest
# vrt_request = DescribeRouteTablesRequest.DescribeRouteTablesRequest()
# vrt_request.set_VRouterId('vrt-wz9k1cyx8r0wlk2enyrlf')
# vrt_request.set_accept_format('json')
# vrt_result = clt.do_action(vrt_request)
# json_result = json.loads(vrt_result)
# print json_result
# for i in range(len(json_result['RouteTables']['RouteTable'][0]['RouteEntrys']['RouteEntry'])):
#     print json_result['RouteTables']['RouteTable'][0]['RouteEntrys']['RouteEntry'][i]['DestinationCidrBlock']


# from aliyunsdkecs.request.v20140526 import DescribeInstanceMonitorDataRequest
# imr_request = DescribeInstanceMonitorDataRequest.DescribeInstanceMonitorDataRequest()
# imr_request.set_InstanceId('i-wz97yj3jp885iiryfyn1')
# imr_request.set_StartTime('2017-04-18T00:00:00Z')
# imr_request.set_EndTime('2017-04-18T01:00:00Z')
# # vrt_request.get_VRouterId()
# imr_request.set_accept_format('json')
# imr_result = clt.do_action(imr_request)
# json_result = json.loads(imr_result)
# print json_result
# for i in range(len(json_result['RouteTables']['RouteTable'][0]['RouteEntrys']['RouteEntry'])):
#     print json_result['RouteTables']['RouteTable'][0]['RouteEntrys']['RouteEntry'][i]['DestinationCidrBlock']

from aliyunsdkvpc.request.v20160428 import DescribeRouterInterfacesRequest
ri_request = DescribeRouterInterfacesRequest.DescribeRouterInterfacesRequest()
ri_request.set_accept_format('json')
ri_result = clt.do_action_with_exception(ri_request)
json_result = json.loads(ri_result)
print json_result
# print json_result['RouterInterfaceSet']['RouterInterfaceType'][0]['Status']
# print json_result['RouterInterfaceSet']['RouterInterfaceType'][0]['RouterInterfaceId']
# print json_result['RouterInterfaceSet']['RouterInterfaceType'][0]['OppositeInterfaceStatus']