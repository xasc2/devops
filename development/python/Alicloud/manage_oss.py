#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import oss2

ali_cfg_json = json.load(open('alicfg.json'))

access_key_id = ali_cfg_json['AccessKeyId']
access_key_secret  = ali_cfg_json['AccessKeySecret']
endpoint = ali_cfg_json['Endpoint']
bucket_names = ali_cfg_json['BucketName']

service = oss2.Service(oss2.Auth(access_key_id, access_key_secret), endpoint)
for bucket_name in bucket_names:
    bucket = oss2.Bucket(oss2.Auth(access_key_id, access_key_secret), endpoint, bucket_name)
    try:
        bucket.create_bucket()
    except oss2.exceptions.ServerError as e:
    #     print e
        error_details = e.details
        print 'Error: ' + error_details['Code'] + ' for Bucket ' + error_details['BucketName']
        print error_details['Message']
        if error_details['Code'] == 'InvalidBucketName':
            print 'please follow below name rules to create a valid bucket: '
            print '-- Bucket names can contain lowercase letters, numbers, and hyphens.'
            print '-- Each label must start and end with a lowercase letter or a number.'
            print '-- Bucket names must be at least 3 and no more than 63 characters long.\n'
        else:
            pass
     
        