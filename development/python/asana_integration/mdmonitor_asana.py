#!/usr/bin/env python

# this script needs requests module
# download requests module and run:
# #python setup.py install
# to install this module

from asana import AsanaAPI,AsanaException
from requests import *
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import socket
import sys
import time
import logging
import signal
import os
import socket
import commands
import smtplib

#### substitution following variables in product environment####
"""list of modifiable variables 
        
        :param api_key:                       asana account api key
        :param logging_file:                  error log file, script runner should have write permission on it
        :param workspace_id:                  asana workspace id, example one is "LUXOTTICA"(type:int, not a string)
        :param user_id:                       asana task assignee (user) id, example one is "Terry Xiao"
        :param robot_project:                 asana project id array like ['21502226934159', '21502226989543', '21502226968742'] if multi projects, example one is created for test purpose under above assignee (user) id
        :param story_text:                    to add task comment text if task is detected still not finished
        :param tmp_file:                      temporary file to record failed devices
        :param single_subtask_name_list:      array of single subtask list
        :param dual_subtask_name_list:        array of dual subtask list
        """
api_key = "xxxxxxxxxxxxxxxxxxxxxxxxxxx"
logging_file = "/var/log/mdadm_asana.log"
workspace_id = 123456789
#user_id = "15162961607825"
robot_project = ['123456789']
tmp_file = "/var/log/mdmonitor_alert_info"
single_subtask_name_list = ['Run fixboot after OS Sync completed', 'OS Sync completed', 'Hard Drive replaced', 'Scheduled with Dell', 'Informed store Dell would attend', 'Failed drive if there are performance issues', 'Run fixboot prior HDD replacement']
dual_subtask_name_list = ['Setup calendar entries', 'Advise IMI of requirements', 'Make Appointment with store for DB migration', 'Notify Store of Issue']


####  detect connection to asana API  ####
def asana_connected():
    try:
        status_code_tuple = commands.getstatusoutput("curl -x https://89.0.0.247:3128 -sL -w \"%{http_code}\\n\" --connect-timeout 5 \"https://app.asana.com\" -o /dev/null")
        if status_code_tuple[1] == '000':
            return False
        else:
            return True
    except:
        pass

#########stage 1: collecting failure events and devices ####
processname = "/usr/local/share/lrapsoe/bin/asana_integration/mdmonitor_asana.py"

#disk_array = {
#              '/dev/md0':'/dev/sda3',
#              '/dev/md1':'/dev/sda1',
#              '/dev/md2':'/dev/sda4'
#              }

disk_array = {
              '/dev/md0':'md0',
              '/dev/md1':'md1',
              '/dev/md2':'md2'
              }

event_arg = sys.argv[1]
device_arg = sys.argv[2]

mdstat_tuple = commands.getstatusoutput("grep "+disk_array[str(sys.argv[2])]+"/proc/mdstat")

try:
    with open(tmp_file, 'a') as the_file:
#        the_file.write("event:"+str(sys.argv[1])+","+"device:"+disk_array[str(sys.argv[2])]+"\n")
        the_file.write("event:"+str(sys.argv[1])+","+"mdstat:"+mdstat_tuple[1]+"\n")
        the_file.close
except:
    logger.error(': Can not open %s' % tmp_file)
    raise sys.exit(1)

# detected how many processes are being created and running for this script, kill all the others,just leave the last running one
tmp_pid = 0
for line in os.popen("pgrep -fl "+processname):

    if line.strip():
        fields = line.split()
        pid = fields[0]
        process = fields[2]
        if process == processname:
            if tmp_pid == 0:
                tmp_pid = pid
            elif pid is not 0 and pid > tmp_pid:
                os.kill(int(tmp_pid), signal.SIGKILL)
                tmp_pid = pid
            else:
                pass
        else:
            pass
    else:
        pass

# you must leave this script sleep for 20 seconds, otherwise you can't catch the last running one
time.sleep(20)

#### stage 2: push to asana ####
####logging function#####
logger = logging.getLogger('mdadm_asana')
hdlr = logging.FileHandler(logging_file)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.WARNING)

####  logic on using asana API  ####
aapi = AsanaAPI(api_key, debug=True)
devices_string = ""
error_disk_count = 0
reverse_disk_name = {'/dev/sda1':'/dev/sdb1',
                     '/dev/sda3':'/dev/sdb3',
                     '/dev/sda4':'/dev/sdb4',
                     '/dev/sdb1':'/dev/sda1',
                     '/dev/sdb3':'/dev/sda3',
                     '/dev/sdb4':'/dev/sda4'}
dev = ""
# get the faulty disks
for i in range(3):
    (status, result) = commands.getstatusoutput("mdadm -D /dev/md"+str(i)+" | grep removed")
    if status == 0:       
        active_disk_tuple = commands.getstatusoutput("mdadm -D /dev/md"+str(i)+" | grep sync")
        faulty_disk = reverse_disk_name[active_disk_tuple[1].split()[6]]
        
        if faulty_disk[0:8] != dev:
            error_disk_count = error_disk_count + 1
        else:
            pass
        dev = faulty_disk[0:8]
        devices_string = devices_string + faulty_disk + " "
    else:
        pass

try:
    with open(tmp_file) as fp:
        for line in fp:
            line_dict = dict(e.rstrip().split(':') for e in line.split(','))
#            mdstat_string = mdstat_string + line_dict['mdstat'] + " "
        fp.close   
except:
    logger.error(': Can not read from %s' % tmp_file)
    raise sys.exit(2)


###  keep next line for reference ###
#task_name = hostname + " - store name - SDA " + line_dict['event'] + " happened on " + devices_string
hostname = socket.gethostname()[3:7]
disk = {'/dev/sda':'SDA', '/dev/sdb':'SDB'}
storeName_tuple = commands.getstatusoutput("ssh -o StrictHostKeyChecking=no focuslst@spacewalk 'grep " + str(hostname) + " /usr/local/share/cmdb/focuslst |cut -d \" \" -f 3'")
space = " "
store_name = space.join(storeName_tuple[1].split('.'))

single_task_name = hostname + " - " + store_name + " - " + disk[devices_string.split()[0][0:8]]
dual_task_name = hostname + " - " + store_name
st_tuple = commands.getstatusoutput("dmidecode |grep -A10 \"System Information\" |grep \"Serial Number\" |awk '{print $3}'")
notes_string = "Dell Case No: \nLogged with Dell on: \nService Tag: " + st_tuple[1] + "\nDrive failed: " + devices_string + "\nHDD condition:  MDMONITOR - " + line_dict['event'] + " RAID error"  
story_text = "Problem still persists, latest error - MDMONITOR - " + line_dict['event'] + " - " + devices_string

####  ignore RebuildXX event  ####
if line_dict['event'][0:7] == 'Rebuild':
    if os.path.exists(tmp_file):
        try:
            os.remove(tmp_file)
        except OSError, e:
            logger.error('--> Error: %s - %s.' % (e.tmp_file,e.strerror))
            raise sys.exit(8)
    else:
        logger.error(': Sorry, I can not find %s file.' % tmp_file)
        raise sys.exit(8)
    sys.exit()

# create and push task to asana  
if asana_connected():         
    try:
        my_tasks = aapi.list_project_tasks(robot_project[0], workspace=workspace_id, completed_since='now')
        count = 0
        item = 0
        while (count < len(my_tasks)):
            task_details = aapi.get_task(my_tasks[count]['id'])
            remote_spliter = task_details['name'].split(' ')
            if remote_spliter[0] == hostname:
                aapi.add_story(my_tasks[count]['id'], story_text)
                item = item + 1
            else:
                pass
            count =  count + 1
          
        if item == 0:
            try:
                if error_disk_count == 1:
                    new_task = aapi.create_task(name=single_task_name, workspace=workspace_id, due_on=time.strftime("%Y-%m-%d"), notes=notes_string, projects=robot_project)
                    for subtask_name in single_subtask_name_list:
                        aapi.create_subtask(parent_id=new_task['id'], name=subtask_name, due_on=time.strftime("%Y-%m-%d"))
                else:
                    new_task = aapi.create_task(name=dual_task_name, workspace=workspace_id, due_on=time.strftime("%Y-%m-%d"), notes=notes_string, projects=robot_project)
                    for subtask_name in dual_subtask_name_list:
                        aapi.create_subtask(parent_id=new_task['id'], name=subtask_name, due_on=time.strftime("%Y-%m-%d"))
            except (AsanaException,ConnectionError) as e:
                logger.error(e)
                raise sys.exit(3)
        else:
            pass
                
    except (AsanaException,ConnectionError) as e:
        logger.error(e)
        raise sys.exit(4)
else:
# if asana connection is faild, send information email
    sender = 'root'
    receivers = ['linux@luxottica.com.au']
    
    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = ', '.join(receivers)
    msg['Subject'] = "Asana connection issue @" + hostname 
    body = "There is no asana connection, there might be network issue."
    msg.attach(MIMEText(body, 'plain'))
    text = msg.as_string()
      
    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, text)
        #print "Successfully sent email"
    except SMTPException:
        #print "Error: unable to send email"
        logger.error(': unable to send email')
        raise sys.exit(5)

if os.path.exists(tmp_file):
    try:
        os.remove(tmp_file)
    except OSError, e:
        logger.error('--> Error: %s - %s.' % (e.tmp_file,e.strerror))
        raise sys.exit(6)
else:
    logger.error(': Sorry, I can not find %s file.' % tmp_file)
    raise sys.exit(7)

sys.exit()