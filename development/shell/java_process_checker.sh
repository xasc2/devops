#!/bin/bash

APP_CONF=$1
APP_PATH=$(dirname $(dirname $APP_CONF))
#echo $APP_PATH
#PARENT_DIR=$(dirname $APP_PATH)
#echo $PARENT_DIR
APP_EXEC=$2
#echo .$APP_EXEC
PID_FILE=${APP_PATH}/RUNNING_PID
JAVA_PID=$(cat $PID_FILE)
echo $JAVA_PID

LIVING_PID=$(ps -ef | grep java | grep -v "$0" | grep -v grep | sed '1q;d' | tr -s ' ' | cut -d ' ' -f 2)
#echo $LIVING_PID
if [ -z "$LIVING_PID" ]; then
        echo "dead"
        /bin/rm -f $PID_FILE
        cd $APP_PATH && /bin/sh .$APP_EXEC -Dhttp.port=80 -Dconfig.file=$APP_CONF &
fi
exit 0





#num=`/usr/bin/pgrep -x java | wc -l`
#if [[ $num -eq 2 ]]; then
#	echo "88"
#	exit 0
#elif [[ $num -eq 1 ]]; then
#	service=$(ps -ef | grep java | grep -v "$0" | grep -v grep | sed '1q;d' | tr -s ' ' | cut -d ' ' -f 12)
#	if [[ "$service" -eq "-Duser.dir=/mnt/ums/umsbackend-0.2" ]]; then
#		/bin/rm -f /mnt/fpmiddleware/stage/RUNNING_PID
#		/mnt/fpmiddleware/start.sh
#	else
#		/bin/rm -f /mnt/ums/umsbackend-0.2/RUNNING_PID
#		/mnt/ums/umsbackend-0.2/bin/umsbackend -Dhttp.port=8080 -Dconfig.file=/mnt/ums/umsbackend-0.2/conf/alpha.conf &
#	fi
#else
#	/bin/rm -f /mnt/ums/umsbackend-0.2/RUNNING_PID
#	/mnt/ums/umsbackend-0.2/bin/umsbackend -Dhttp.port=8080 -Dconfig.file=/mnt/ums/umsbackend-0.2/conf/alpha.conf &
#	/bin/rm -f /mnt/fpmiddleware/stage/RUNNING_PID
#	/mnt/fpmiddleware/start.sh
#fi
#exit 0