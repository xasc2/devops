terraform {
  backend "azurerm" {
    storage_account_name = "tfstatesta"
    container_name       = "tfstatestacontainer"
    key                  = "prod.terraform.tfstate"
    access_key           = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    environment          = "public"
  }
}

data "terraform_remote_state" "tfstate" {
  backend = "azurerm"
  config {
    storage_account_name = "tfstatesta"
    container_name       = "tfstatestacontainer"
    key                  = "prod.terraform.tfstate"
    access_key           = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    environment          = "public"
  }
}

provider "azurerm" {
  version     = "=1.21.0"
  environment = "public"
}

module "VNET_NC1PRDINFVN01" {
  source = "modules/vnet"
  vnet_name = "NC1PRDINFVN01"
  resource_group_name = "NC1PRDINFRG01"
  location = "chinanorth"
  address_space = "10.1.0.0/16"
  subnet_prefixes     = ["10.1.1.0/24", "10.1.2.0/24"]
  subnet_names        = ["GatewaySubnet", "NC1PRDINFSN01"]
  tags                = {
                            environment = "INF"
                        }
}

module "VNET_GATEWAY" {
  source = "modules/vnet_gateway"
  public_ip_name = "VNET-NC1PRDINFVN01-publice-ip"
  location = "${module.VNET_NC1PRDINFVN01.vnet_location}"
  resource_group_name = "${module.VNET_NC1PRDINFVN01.vnet_resource_group}"
  gateway_name = "NC1PRDINFVN01-gateway"
  subnet_id = "${module.VNET_NC1PRDINFVN01.vnet_subnets[0]}"
  vpn_address_space = ["192.168.0.0/24"]
}

module "VNET_NC1TSTDYNVN01" {
  source = "modules/vnet"
  vnet_name = "NC1TSTDYNVN01"
  resource_group_name = "NC1TSTDYNRG01"
  location = "East Asia"
  address_space = "10.2.0.0/16"
  subnet_prefixes     = ["10.2.1.0/24", "10.2.2.0/24"]
  subnet_names        = ["NCTSTDYNFRNSN01", "NCTSTDYNBNDSN01"]
  tags                = {
                            environment = "TST"
                        }
  # associated_subnet_id = 
  # associated_nsg_id = 
}

resource "azurerm_subnet_network_security_group_association" "associated-nsg" {
  count = 2
  subnet_id = "${module.VNET_NC1TSTDYNVN01.vnet_subnets[count.index]}"
  network_security_group_id = "${substr(element(split("/", module.VNET_NC1TSTDYNVN01.vnet_subnets[count.index]), length(split("/", module.VNET_NC1TSTDYNVN01.vnet_subnets[count.index]))-1),8,3)=="FRN"? module.SUBNET_FRONTEND_NSG.network_security_group_id:module.SUBNET_BACKEND_NSG.network_security_group_id}"
}

module "SUBNET_FRONTEND_NSG" {
  source = "modules/nsg"
  resource_group_name        = "NCNGRG"
  location                   = "East Asia"
  security_group_name        = "NCFRNNG"
  predefined_rules           = [
    {
      name                   = "HTTPS"
      priority               = "1000"
    },
    {
      name                   = "RDP"
      priority               = "2000"
      source_address_prefix  = "10.0.10.0/24"
    }
  ]
  custom_rules               = [
    {
      name                   = "AllowInfAnyAll"
      priority               = "2010"
      direction              = "Inbound"
      access                 = "Allow"
      description            = "AllowInfAnyAll"
    },
    {
      name                   = "AllowAnyTCCAll"
      priority               = "2010"
      direction              = "Outbound"
      access                 = "Allow"
      destination_address_prefix = "10.0.10.0/24"
      description            = "AllowAnyTCCAll"
    }
  ]
}

module "SUBNET_BACKEND_NSG" {
  source = "modules/nsg"
  resource_group_name        = "NCNGRG"
  location                   = "East Asia"
  security_group_name        = "NCBNDNG"
  predefined_rules           = [
    {
      name                   = "HTTPS"
      priority               = "1000"
    },
    {
      name                   = "RDP"
      priority               = "2000"
      source_address_prefix  = "10.0.10.0/24"
    }
  ]
  custom_rules               = [
    {
      name                   = "AllowInfAnyAll"
      priority               = "2010"
      direction              = "Inbound"
      access                 = "Allow"
      description            = "AllowInfAnyAll"
    },
    {
      name                   = "AllowAnyTCCAll"
      priority               = "2010"
      direction              = "Outbound"
      access                 = "Allow"
      destination_address_prefix = "10.0.10.0/24"
      description            = "AllowAnyTCCAll"
    }
  ]
}  

module "windowsservers" {
    source              = "modules/computer"
    resource_group_name = "${module.VNET_NC1TSTDYNVN01.vnet_resource_group}"
    location            = "${module.VNET_NC1TSTDYNVN01.vnet_location}"
    vm_hostname         = "NC1TSTDYNVM01" // line can be removed if only one VM module per resource group
    vm_size             = "Standard_A1_v2"
    storage_account_type= "Standard_LRS"
    admin_password      = "ComplxP@ssw0rd!"
    vm_os_simple        = "WindowsServer"
    public_ip_dns       = ["NC1TSTDYNVM01"] // change to a unique name per datacenter region
    vnet_subnet_id      = "${module.VNET_NC1TSTDYNVN01.vnet_subnets[0]}"
  }

module "VNET_NC1STGDYNVN01" {
  source = "modules/vnet"
  vnet_name = "NC1STGDYNVN01"
  resource_group_name = "NC1STGDYNRG01"
  location = "chinanorth"
  address_space = "10.3.0.0/16"
  subnet_prefixes     = ["10.3.1.0/24", "10.3.2.0/24"]
  subnet_names        = ["NCSTGDYNFRNSN01", "NCSTGDYNBNDSN01"]
  tags                = {
                            environment = "STG"
                        }
}

module "VNET_NC1PRDDYNVN01" {
  source = "modules/vnet"
  vnet_name = "NC1PRDDYNVN01"
  resource_group_name = "NC1PRDDYNRG01"
  location = "chinanorth"
  address_space = "10.4.0.0/16"
  subnet_prefixes     = ["10.4.1.0/24", "10.4.2.0/24"]
  subnet_names        = ["NCPRDDYNFRNSN01", "NCPRDDYNBNDSN01"]
  tags                = {
                            environment = "PRD"
                        }
}

resource "azurerm_virtual_network_peering" "NC1PRDINFVN01-to-NC1TSTDYNVN01" {
  name                         = "NCINFTSTPR01"
  resource_group_name          = "${module.VNET_NC1PRDINFVN01.vnet_resource_group}"
  virtual_network_name         = "${module.VNET_NC1PRDINFVN01.vnet_name}"
  remote_virtual_network_id    = "${module.VNET_NC1TSTDYNVN01.vnet_id}"
  allow_virtual_network_access = true
  allow_gateway_transit        = true
}

resource "azurerm_virtual_network_peering" "NC1PRDINFVN01-to-NC1STGDYNVN01" {
  name                         = "NCINFSTGPR01"
  resource_group_name          = "${module.VNET_NC1PRDINFVN01.vnet_resource_group}"
  virtual_network_name         = "${module.VNET_NC1PRDINFVN01.vnet_name}"
  remote_virtual_network_id    = "${module.VNET_NC1STGDYNVN01.vnet_id}"
  allow_virtual_network_access = true
  allow_gateway_transit        = true
}

resource "azurerm_virtual_network_peering" "NC1PRDINFVN01-to-NC1PRDDYNVN01" {
  name                         = "NCINFPRDPR01"
  resource_group_name          = "${module.VNET_NC1PRDINFVN01.vnet_resource_group}"
  virtual_network_name         = "${module.VNET_NC1PRDINFVN01.vnet_name}"
  remote_virtual_network_id    = "${module.VNET_NC1PRDDYNVN01.vnet_id}"
  allow_virtual_network_access = true
  allow_gateway_transit        = true
}

resource "azurerm_virtual_network_peering" "NC1TSTDYNVN01-to-NC1PRDINFVN01" {
  name                         = "NCTSTINFPR01"
  resource_group_name          = "${module.VNET_NC1TSTDYNVN01.vnet_resource_group}"
  virtual_network_name         = "${module.VNET_NC1TSTDYNVN01.vnet_name}"
  remote_virtual_network_id    = "${module.VNET_NC1PRDINFVN01.vnet_id}"
  allow_virtual_network_access = true
  allow_gateway_transit        = true
  use_remote_gateways          = true
}

resource "azurerm_virtual_network_peering" "NC1STGDYNVN01-to-NC1PRDINFVN01" {
  name                         = "NCSTGINFPR01"
  resource_group_name          = "${module.VNET_NC1STGDYNVN01.vnet_resource_group}"
  virtual_network_name         = "${module.VNET_NC1STGDYNVN01.vnet_name}"
  remote_virtual_network_id    = "${module.VNET_NC1PRDINFVN01.vnet_id}"
  allow_virtual_network_access = true
  allow_gateway_transit        = true
  use_remote_gateways          = true
}

resource "azurerm_virtual_network_peering" "NC1PRDDYNVN01-to-NC1PRDINFVN01" {
  name                         = "NCPRDINFPR01"
  resource_group_name          = "${module.VNET_NC1PRDDYNVN01.vnet_resource_group}"
  virtual_network_name         = "${module.VNET_NC1PRDDYNVN01.vnet_name}"
  remote_virtual_network_id    = "${module.VNET_NC1PRDINFVN01.vnet_id}"
  allow_virtual_network_access = true
  allow_gateway_transit        = true
  use_remote_gateways          = true
}