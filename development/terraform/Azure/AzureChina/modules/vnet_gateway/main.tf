provider "azurerm" {
  version     = "=1.21.0"
  environment = "${var.env}"
}

resource "azurerm_public_ip" "ip" {
  name                = "${var.public_ip_name}"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  allocation_method = "Dynamic"
}

resource "azurerm_virtual_network_gateway" "gateway" {
  name                = "${var.gateway_name}"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  type     = "Vpn"
  vpn_type = "RouteBased"

  active_active = false
  enable_bgp    = false
  sku           = "Basic"

  ip_configuration {
    name                          = "VNETGatewayConfig"
    public_ip_address_id          = "${azurerm_public_ip.ip.id}"
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = "${var.subnet_id}"
  }

  vpn_client_configuration {
    address_space = "${var.vpn_address_space}"
  }
}