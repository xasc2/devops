variable "env" {
  description = "provider's environment"
  default = "public"
}

variable "public_ip_name" {
  description = "public ip"
}

variable "location" {
  description = "same as resource group location"
}

variable "resource_group_name" {
  description = "resource group name"
}

variable "gateway_name" {
  description = "gateway name"
}

variable "subnet_id" {
  description = "subnet id"
}

variable "vpn_address_space" {
  type = "list"
  description = "vpn address spaces"
}
