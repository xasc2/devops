resource "aws_iam_role" "s3fullaccess_role" {
    name = "${var.project}-s3fullaccess"

    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "s3fullaccess_role_policy" {
    name = "s3fullaccess-role-policy"
    role = "${aws_iam_role.s3fullaccess_role.id}"

    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role" "s3_yum_repo" {
    name = "${var.project}-s3-yum-repo"

    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "s3_yum_repo_policy" {
  name = "s3-yum-repo-policy"
  role = "${aws_iam_role.s3_yum_repo.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:List*",
                "s3:Get*"
            ],
            "Resource": [
                "arn:aws:s3:::${var.s3_yum_repo_bucket_arn}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:List*",
                "s3:Get*"
            ],
            "Resource": [
                "arn:aws:s3:::ubi-adm-yum-s3/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role" "mgmt" {
    name = "${var.project}-mgmt"

    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "mgmt_policy" {
  name = "mgmt-policy"
  role = "${aws_iam_role.mgmt.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
        "Effect": "Allow",
        "Action": "*",
        "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_group" "admin" {
    name = "${var.project}-admin"
}

resource "aws_iam_group_membership" "admin_group_membership" {
    name = "admin-group-membership"

    users = [
        "${aws_iam_user.ops.name}",
        "${aws_iam_user.sd.name}"
    ]

    group = "${aws_iam_group.admin.name}"
}

resource "aws_iam_group_policy" "admin_group_policy" {
    name  = "admin_group_policy"
    group = "${aws_iam_group.admin.id}"

    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
        "Effect": "Allow",
        "Action": "*",
        "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_group" "readonly" {
  name = "${var.project}-readonly"
}

resource "aws_iam_group_membership" "readonly_group_membership" {
  name = "readonly-group-membership"

  users = [
    "${aws_iam_user.sa.name}"
  ]

  group = "${aws_iam_group.readonly.name}"
}

resource "aws_iam_group_policy" "readonly_group_policy" {
    name  = "readonly_group_policy"
    group = "${aws_iam_group.readonly.id}"

    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "acm:Describe*",
                "acm:Get*",
                "acm:List*",
                "apigateway:GET",
                "application-autoscaling:Describe*",
                "appstream:Describe*",
                "appstream:Get*",
                "appstream:List*",
                "athena:List*",
                "athena:Batch*",
                "athena:Get*",
                "autoscaling:Describe*",
                "batch:List*",
                "batch:Describe*",
                "clouddirectory:List*",
                "clouddirectory:BatchRead",
                "clouddirectory:Get*",
                "clouddirectory:LookupPolicy",
                "cloudformation:Describe*",
                "cloudformation:Get*",
                "cloudformation:List*",
                "cloudformation:Estimate*",
                "cloudformation:Preview*",
                "cloudfront:Get*",
                "cloudfront:List*",
                "cloudhsm:List*",
                "cloudhsm:Describe*",
                "cloudhsm:Get*",
                "cloudsearch:Describe*",
                "cloudsearch:List*",
                "cloudtrail:Describe*",
                "cloudtrail:Get*",
                "cloudtrail:List*",
                "cloudtrail:LookupEvents",
                "cloudwatch:Describe*",
                "cloudwatch:Get*",
                "cloudwatch:List*",
                "codebuild:BatchGet*",
                "codebuild:List*",
                "codecommit:BatchGet*",
                "codecommit:Get*",
                "codecommit:GitPull",
                "codecommit:List*",
                "codedeploy:BatchGet*",
                "codedeploy:Get*",
                "codedeploy:List*",
                "codepipeline:List*",
                "codepipeline:Get*",
                "codestar:List*",
                "codestar:Describe*",
                "codestar:Get*",
                "codestar:Verify*",
                "cognito-identity:List*",
                "cognito-identity:Describe*",
                "cognito-identity:Lookup*",
                "cognito-sync:List*",
                "cognito-sync:Describe*",
                "cognito-sync:Get*",
                "cognito-sync:QueryRecords",
                "cognito-idp:AdminList*",
                "cognito-idp:List*",
                "cognito-idp:Describe*",
                "cognito-idp:Get*",
                "config:Deliver*",
                "config:Describe*",
                "config:Get*",
                "config:List*",
                "connect:List*",
                "connect:Describe*",
                "connect:Get*",
                "datapipeline:Describe*",
                "datapipeline:EvaluateExpression",
                "datapipeline:Get*",
                "datapipeline:List*",
                "datapipeline:QueryObjects",
                "datapipeline:Validate*",
                "directconnect:Describe*",
                "devicefarm:List*",
                "devicefarm:Get*",
                "discovery:Describe*",
                "discovery:List*",
                "discovery:Get*",
                "dms:Describe*",
                "dms:List*",
                "dms:Test*",
                "ds:Check*",
                "ds:Describe*",
                "ds:Get*",
                "ds:List*",
                "ds:Verify*",
                "dynamodb:BatchGet*",
                "dynamodb:Describe*",
                "dynamodb:Get*",
                "dynamodb:List*",
                "dynamodb:Query",
                "dynamodb:Scan",
                "ec2:Describe*",
                "ec2:Get*",
                "ec2messages:Get*",
                "ecr:BatchCheck*",
                "ecr:BatchGet*",
                "ecr:Describe*",
                "ecr:Get*",
                "ecr:List*",
                "ecs:Describe*",
                "ecs:List*",
                "elasticache:Describe*",
                "elasticache:List*",
                "elasticbeanstalk:Check*",
                "elasticbeanstalk:Describe*",
                "elasticbeanstalk:List*",
                "elasticbeanstalk:Request*",
                "elasticbeanstalk:Retrieve*",
                "elasticbeanstalk:Validate*",
                "elasticfilesystem:Describe*",
                "elasticloadbalancing:Describe*",
                "elasticmapreduce:Describe*",
                "elasticmapreduce:List*",
                "elasticmapreduce:View*",
                "elastictranscoder:List*",
                "elastictranscoder:Read*",
                "es:Describe*",
                "es:List*",
                "es:ESHttpGet",
                "es:ESHttpHead",
                "events:Describe*",
                "events:List*",
                "events:Test*",
                "firehose:Describe*",
                "firehose:List*",
                "gamelift:List*",
                "gamelift:Get*",
                "gamelift:Describe*",
                "gamelift:RequestUploadCredentials",
                "gamelift:ResolveAlias",
                "gamelift:Search*",
                "glacier:List*",
                "glacier:Describe*",
                "glacier:Get*",
                "health:Describe*",
                "health:Get*",
                "health:List*",
                "iam:Generate*",
                "iam:Get*",
                "iam:List*",
                "iam:Simulate*",
                "importexport:Get*",
                "importexport:List*",
                "inspector:Describe*",
                "inspector:Get*",
                "inspector:List*",
                "inspector:Preview*",
                "inspector:LocalizeText",
                "iot:Describe*",
                "iot:Get*",
                "iot:List*",
                "kinesisanalytics:Describe*",
                "kinesisanalytics:Discover*",
                "kinesisanalytics:Get*",
                "kinesisanalytics:List*",
                "kinesis:Describe*",
                "kinesis:Get*",
                "kinesis:List*",
                "kms:Describe*",
                "kms:Get*",
                "kms:List*",
                "lambda:List*",
                "lambda:Get*",
                "lex:Get*",
                "lightsail:Get*",
                "lightsail:Is*",
                "lightsail:Download*",
                "logs:Describe*",
                "logs:Get*",
                "logs:FilterLogEvents",
                "logs:ListTagsLogGroup",
                "logs:TestMetricFilter",
                "machinelearning:Describe*",
                "machinelearning:Get*",
                "mobileanalytics:Get*",
                "mobilehub:Get*",
                "mobilehub:List*",
                "mobilehub:Validate*",
                "mobilehub:Verify*",
                "mobiletargeting:Get*",
                "opsworks:Describe*",
                "opsworks:Get*",
                "opsworks-cm:Describe*",
                "organizations:Describe*",
                "organizations:List*",
                "polly:Describe*",
                "polly:Get*",
                "polly:List*",
                "polly:SynthesizeSpeech",
                "rekognition:CompareFaces",
                "rekognition:Detect*",
                "rekognition:List*",
                "rekognition:Search*",
                "rds:Describe*",
                "rds:List*",
                "rds:Download*",
                "redshift:Describe*",
                "redshift:View*",
                "redshift:Get*",
                "route53:Get*",
                "route53:List*",
                "route53:Test*",
                "route53domains:Check*",
                "route53domains:Get*",
                "route53domains:List*",
                "route53domains:View*",
                "s3:Get*",
                "s3:List*",
                "s3:Head*",
                "sdb:Get*",
                "sdb:List*",
                "sdb:Select*",
                "servicecatalog:List*",
                "servicecatalog:Scan*",
                "servicecatalog:Search*",
                "servicecatalog:Describe*",
                "ses:Get*",
                "ses:List*",
                "ses:Describe*",
                "ses:Verify*",
                "shield:Describe*",
                "shield:List*",
                "sns:Get*",
                "sns:List*",
                "sns:Check*",
                "sqs:Get*",
                "sqs:List*",
                "sqs:Receive*",
                "ssm:Describe*",
                "ssm:Get*",
                "ssm:List*",
                "states:List*",
                "states:Describe*",
                "states:GetExecutionHistory",
                "storagegateway:Describe*",
                "storagegateway:List*",
                "sts:Get*",
                "swf:Count*",
                "swf:Describe*",
                "swf:Get*",
                "swf:List*",
                "tag:Get*",
                "trustedadvisor:Describe*",
                "waf:Get*",
                "waf:List*",
                "waf-regional:List*",
                "waf-regional:Get*",
                "workdocs:Describe*",
                "workdocs:Get*",
                "workdocs:CheckAlias",
                "workmail:Describe*",
                "workmail:Get*",
                "workmail:List*",
                "workmail:Search*",
                "workspaces:Describe*",
                "xray:BatchGet*",
                "xray:Get*"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
    ]
}EOF
}

resource "aws_iam_user" "ops" {
  name = "${var.project}-ops"
}

resource "aws_iam_user" "sa" {
  name = "${var.project}-sa"
}

resource "aws_iam_user" "sd" {
  name = "${var.project}-sd"
}