output "s3fullaccess_role_arn" {
    value = "${aws_iam_role.s3fullaccess_role.arn}"
}