variable project {
    description = "project prefix"
}

variable s3_yum_repo_bucket_arn {
    description = "s3 yum repo bucket arn from bucket module"
}