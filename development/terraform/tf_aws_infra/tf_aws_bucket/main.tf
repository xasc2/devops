resource "aws_s3_bucket" "backup" {
  bucket = "${var.project}-backup"
  acl    = "private"
}

# resource "aws_s3_bucket_policy" "backup_policy" {
#   bucket = "${aws_s3_bucket.backup.id}"

#   policy = <<POLICY
# {
# "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:List*",
#                 "s3:Get*",
#                 "s3:Put*"
#             ],
#             "Resource": [
#                 "arn:aws:s3:::${var.project}-backup",
#                 "arn:aws:s3:::${var.project}-backup/*"
#             ],
#             "Principal" : {"AWS": "${var.principal_arn}"}
#         }
#     ]
# }
# POLICY
# }

resource "aws_s3_bucket" "log" {
  bucket = "${var.project}-log"
  acl    = "private"
}

# resource "aws_s3_bucket_policy" "log_policy" {
#   bucket = "${aws_s3_bucket.log.id}"

#   policy = <<POLICY
# {
# "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:List*",
#                 "s3:Get*",
#                 "s3:Put*"
#             ],
#             "Resource": [
#                 "arn:aws:s3:::${var.project}-log",
#                 "arn:aws:s3:::${var.project}-log/*"
#             ],
#             "Principal" : {"AWS": "${var.principal_arn}"}
#         }
#     ]
# }
# POLICY
# }

resource "aws_s3_bucket" "s3_yum_repo" {
    bucket = "${var.project}-s3-yum-repo"
    acl    = "private"
}

# resource "aws_s3_bucket_policy" "s3_yum_repo_policy" {
#     bucket = "${aws_s3_bucket.s3_yum_repo.id}"

#     policy = <<POLICY
# {
# "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:List*",
#                 "s3:Get*"
#             ],
#             "Resource": [
#                 "arn:aws:s3:::${var.project}-s3-yum-repo/*"
#             ],
#             "Principal" : {"AWS": "${var.principal_arn}"}
#         }
#     ]
# }
# POLICY
# }