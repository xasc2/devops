variable "global_project" {
    description = "global project name"
    default = "mytestproject02"
}

variable "global_environment" {
    description = "global environment"
    default = "dev"
}