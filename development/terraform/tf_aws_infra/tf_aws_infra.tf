provider "aws" {
    access_key = ""
    secret_key = ""
    region     = "ap-southeast-1"
}

module "tf_aws_vpc" {
  source  = "./tf_aws_vpc"
  name = "${var.global_project}-${var.global_environment}"
  cidr = "10.0.0.0/16"
  public_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets = ["10.0.3.0/24", "10.0.4.0/24"]
  azs = ["ap-southeast-1a", "ap-southeast-1b"]
  enable_s3_endpoint = true
  enable_dns_hostnames = true
  enable_dns_support = true
}

module "tf_aws_bucket" {
    source = "./tf_aws_bucket"
    project = "${var.global_project}"
    principal_arn = "${module.tf_aws_iam.s3fullaccess_role_arn}"
}

module "tf_aws_iam" {
    source = "./tf_aws_iam"
    project = "${var.global_project}"
    s3_yum_repo_bucket_arn = "${module.tf_aws_bucket.s3_yum_repo_arn}"
}

module "tf_aws_sg" {
    source = "./tf_aws_sg"
    project = "${var.global_project}"
    environment = "${var.global_environment}"
    vpc_id = "${module.tf_aws_vpc.vpc_id}"
}

module "tf_aws_image" {
    source = "./tf_aws_image"
    private_key_path = "~/.ssh/terraform.pem"
    security_group_ids = [
        "${module.tf_aws_sg.base_sg_id}",
        "${module.tf_aws_sg.zabbix_server_sg_id}",
        "${module.tf_aws_sg.ssh_sg_id}",
        "${module.tf_aws_sg.mgmt_sg_id}"
    ]
    subnet_id = "${element(module.tf_aws_vpc.public_subnets, 1)}"
}
