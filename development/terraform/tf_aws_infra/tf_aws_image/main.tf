data "aws_ami" "centos7" {
    most_recent = true

    filter {
        name   = "product-code"
        values = ["aw0evgkw8e5c1q413zgy5pjce"]
    }

    owners     = ["aws-marketplace"]
}

resource "aws_key_pair" "terraform_key" {
  key_name   = "terraform"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCExflA+kFJMfNWlRYNEDC3pISWsTfhCncXa2jJGIZKBSHkCCsftLcBKoHQDD8Ur1bopMo6TJXixCIFMX+/U6XIpm1nPW8LZDmJ3T97EnV7fZ/7F+1lGyuITGeFnX3Z287QIhuhEQVxCq5VWKbqnAwyM4OsT/0PI+iqLQXKmjIhxaZUUXKFfy37eJKK3jjLpAhbjN3ir4yINRaS4eQmPcQHozjunViHgvPRXFyZ5Y8qFyUZM0bsvSqy03H37aBIuRvKtLg4PGEcftS2PUroO9TufRFlG9ruFrRwnryRMHfHJ3hunPqEgUMJWPsGA4S6kdGZGN8lp8RUKxYNIUb5Ciqr"
}

resource "aws_instance" "prototype_instance" {
    ami           = "${data.aws_ami.centos7.id}"
    instance_type = "t2.micro"

    connection {
        user = "centos"
        private_key = "${file(var.private_key_path)}"
    }

    key_name = "${aws_key_pair.terraform_key.key_name}"
    vpc_security_group_ids = ["${var.security_group_ids}"]
    subnet_id = "${var.subnet_id}"

    # provisioner "remote-exec" {
    #     inline = [
    #     "sudo yum -y update",
    #     "sudo yum -y install nginx",
    #     "sudo systemctl start nginx"
    #     ]
    # }
    provisioner "file" {
        source      = "${"${path.module}/image.sh"}"
        destination = "/tmp/image.sh"
    }

    provisioner "remote-exec" {
        inline = [
        "sudo chmod +x /tmp/image.sh",
        "sudo /tmp/image.sh"
        ]
    }
}

resource "aws_ami_from_instance" "gns_china_image" {
    name               = "gns-china-image"
    source_instance_id = "${aws_instance.prototype_instance.id}"
    # lifecycle {
    #     prevent_destroy = true
    # }
}