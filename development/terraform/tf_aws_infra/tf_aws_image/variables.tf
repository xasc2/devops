# variable "project" {
#     description = "project name"
# }

# variable "environment" {
#     description = "environment"
# }

variable "private_key_path" {
  description = <<DESCRIPTION
Path to the SSH private key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.

Example: ~/.ssh/terraform.pem
DESCRIPTION
}

variable security_group_ids {
    type = "list"
    description = "provisioned security groupd ids"
}

variable subnet_id {
    description = "provisioned subnet id"
}