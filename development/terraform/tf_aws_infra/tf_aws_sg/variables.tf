variable "project" {
    description = "project name"
}

variable "environment" {
    description = "environment"
}

variable "vpc_id" {
    description = "provisioned vpc id"
}