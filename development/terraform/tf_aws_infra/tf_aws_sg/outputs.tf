output base_sg_id {
    value = "${aws_security_group.base.id}"
}

output zabbix_server_sg_id {
    value = "${aws_security_group.zabbix_server.id}"
}

output ssh_sg_id {
    value = "${aws_security_group.ssh.id}"
}

output mgmt_sg_id {
    value = "${aws_security_group.mgmt.id}"
}