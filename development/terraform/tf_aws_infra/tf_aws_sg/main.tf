resource "aws_security_group" "base" {
    name        = "${var.project}-${var.environment}-base"
    vpc_id      = "${var.vpc_id}"
}

resource "aws_security_group" "zabbix_server" {
    name        = "${var.project}-${var.environment}-zabbix-server"
    vpc_id      = "${var.vpc_id}"
}

resource "aws_security_group" "ssh" {
    name        = "${var.project}-${var.environment}-ssh"
    vpc_id      = "${var.vpc_id}"
}

resource "aws_security_group" "mgmt" {
    name        = "${var.project}-${var.environment}-mgmt"
    vpc_id      = "${var.vpc_id}"
}

resource "aws_security_group_rule" "zabbix_client_tcp" {
    type            = "ingress"
    from_port       = 10050
    to_port         = 10050
    protocol        = "tcp"
    source_security_group_id = "${aws_security_group.zabbix_server.id}"

    security_group_id = "${aws_security_group.base.id}"
}

resource "aws_security_group_rule" "zabbix_client_udp" {
    type            = "ingress"
    from_port       = 10050
    to_port         = 10050
    protocol        = "udp"
    source_security_group_id = "${aws_security_group.zabbix_server.id}"

    security_group_id = "${aws_security_group.base.id}"
}

resource "aws_security_group_rule" "base_outbound_all" {
    type            = "egress"
    from_port       = 0
    to_port         = 65535
    protocol        = "all"
    cidr_blocks     = ["0.0.0.0/0"]

    security_group_id = "${aws_security_group.base.id}"
}

resource "aws_security_group_rule" "management_8080" {
    type            = "ingress"
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    cidr_blocks     = ["183.193.161.209/32"]
    security_group_id = "${aws_security_group.mgmt.id}"
}

resource "aws_security_group_rule" "management_22" {
    type            = "ingress"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["183.193.161.209/32"]
    security_group_id = "${aws_security_group.mgmt.id}"
}

resource "aws_security_group_rule" "mgmt_outbound_all" {
    type            = "egress"
    from_port       = 0
    to_port         = 65535
    protocol        = "all"
    cidr_blocks     = ["0.0.0.0/0"]

    security_group_id = "${aws_security_group.mgmt.id}"
}

resource "aws_security_group_rule" "zabbix_server_tcp" {
    type            = "ingress"
    from_port       = 10051
    to_port         = 10051
    protocol        = "tcp"
    source_security_group_id = "${aws_security_group.base.id}"

    security_group_id = "${aws_security_group.zabbix_server.id}"
}

resource "aws_security_group_rule" "zabbix_server_udp" {
    type            = "ingress"
    from_port       = 10051
    to_port         = 10051
    protocol        = "udp"
    source_security_group_id = "${aws_security_group.base.id}"

    security_group_id = "${aws_security_group.zabbix_server.id}"
}

resource "aws_security_group_rule" "http_tcp" {
    type            = "ingress"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    source_security_group_id = "${aws_security_group.base.id}"

    security_group_id = "${aws_security_group.zabbix_server.id}"
}

resource "aws_security_group_rule" "https_tcp" {
    type            = "ingress"
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    source_security_group_id = "${aws_security_group.base.id}"

    security_group_id = "${aws_security_group.zabbix_server.id}"
}

resource "aws_security_group_rule" "zabbix_server_outbound_all" {
    type            = "egress"
    from_port       = 0
    to_port         = 65535
    protocol        = "all"
    cidr_blocks     = ["0.0.0.0/0"]

    security_group_id = "${aws_security_group.zabbix_server.id}"
}

resource "aws_security_group_rule" "ssh_tcp" {
    type            = "ingress"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    source_security_group_id = "${aws_security_group.mgmt.id}"

    security_group_id = "${aws_security_group.ssh.id}"
}

resource "aws_security_group_rule" "ssh_outbound_all" {
    type            = "egress"
    from_port       = 0
    to_port         = 65535
    protocol        = "all"
    cidr_blocks     = ["0.0.0.0/0"]

    security_group_id = "${aws_security_group.ssh.id}"
}