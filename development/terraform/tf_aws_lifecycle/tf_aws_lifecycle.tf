provider "aws" {
  access_key = ""
  secret_key = ""
  region     = "ap-southeast-1"
}

module "test" {
  source  = "./tf_aws_lifecycle"
  lifecycle_hook_name = "test_lifecycle_hook"
  asnar_name = "test_asnar"
  asnar_policy_name = "test_asnar_policy_name"
  asg_lifecycly_hook_msg_topic_name = "test_asg_lifecycly_hook_msg_topic"
  lifecyclehook_sns_lambda_role_name = "test_lifecyclehook_sns_lambda_role"
  lifecyclehook_sns_lambda_role_policy_name = "test_lifecyclehook_sns_lambda_role_policy_name"
  terminated_ec2_ip_q_name = "test_terminated_ec2_ip_q"
}