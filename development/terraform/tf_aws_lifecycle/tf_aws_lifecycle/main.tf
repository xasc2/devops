# provider "aws" {
#     access_key = "${var.aws_access_key}"
#     secret_key = "${var.aws_secret_key}"
#     region = "${var.aws_region}"
# }
data "aws_region" "current" {
  current = true
}

resource "aws_autoscaling_lifecycle_hook" "lifecycle_hook" {
    name                   = "${var.lifecycle_hook_name}"
    # autoscaling_group_name = "${aws_autoscaling_group.main_asg.name}"
    autoscaling_group_name = "test_asg_group"
    default_result         = "ABANDON"
    heartbeat_timeout      = 120
    lifecycle_transition   = "autoscaling:EC2_INSTANCE_TERMINATING"

    notification_target_arn = "${aws_sns_topic.asg_lifecycly_hook_msg.arn}"
    role_arn                = "${aws_iam_role.AutoScalingNotificationAccessRole.arn}"
}

resource "aws_iam_role" "AutoScalingNotificationAccessRole" {
    name = "${var.asnar_name}"

    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
                    {
                    "Action": "sts:AssumeRole",
                    "Principal": {"Service": "autoscaling.amazonaws.com"},
                    "Effect": "Allow",
                    "Sid": ""
                    }
    ]
}
EOF
}

resource "aws_iam_role_policy" "AutoScalingNotificationAccessRole_policy" {
    name = "${var.asnar_policy_name}"
    role = "${aws_iam_role.AutoScalingNotificationAccessRole.id}"

    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
                    {
                        "Effect": "Allow",
                        "Resource": "*",
                        "Action": [
                            "sqs:SendMessage",
                            "sqs:GetQueueUrl",
                            "sns:Publish"
                        ]
                    }
    ]
}
EOF
}

resource "aws_sns_topic" "asg_lifecycly_hook_msg" {
    name = "${var.asg_lifecycly_hook_msg_topic_name}"
}

resource "aws_sns_topic_subscription" "lambda_subscription" {
    topic_arn = "${aws_sns_topic.asg_lifecycly_hook_msg.arn}"
    protocol  = "lambda"
    endpoint  = "${aws_lambda_function.get_ec2_ip.arn}"
}

resource "aws_iam_role" "lifecyclehook_sns_lambda_role" {
    name = "${var.lifecyclehook_sns_lambda_role_name}"

    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
                    {
                        "Action": "sts:AssumeRole",
                        "Principal": {"Service": "lambda.amazonaws.com"},
                        "Effect": "Allow",
                        "Sid": ""
                    }
    ]
}
EOF
}

resource "aws_iam_role_policy" "lifecyclehook_sns_lambda_role_policy" {
    name = "${var.lifecyclehook_sns_lambda_role_policy_name}"
    role = "${aws_iam_role.lifecyclehook_sns_lambda_role.id}"

    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
                    {
                        "Action": [
                                    "sqs:*",
                                    "sns:*",
                                    "ec2:Describe*",
                                    "elasticloadbalancing:Describe*",
                                    "cloudwatch:ListMetrics",
                                    "cloudwatch:GetMetricStatistics",
                                    "cloudwatch:Describe*",
                                    "autoscaling:Describe*",
                                    "logs:CreateLogGroup",
                                    "logs:CreateLogStream",
                                    "logs:PutLogEvents",
                                    "ec2:CreateNetworkInterface",
                                    "ec2:DescribeNetworkInterfaces",
                                    "ec2:DeleteNetworkInterface"
                                ],
                        "Effect": "Allow",
                        "Resource": "*"
                    }
    ]
}
EOF
}

resource "aws_lambda_function" "get_ec2_ip" {
    filename         = "getec2ip.zip"
    function_name    = "getEc2IP"
    role             = "${aws_iam_role.lifecyclehook_sns_lambda_role.arn}"
    handler          = "index.handler"
    source_code_hash = "${base64sha256(file("${path.module}/getec2ip.zip"))}"
    runtime          = "nodejs6.10"

    environment {
        variables = {
            Q_URL = "${aws_sqs_queue.terminated_ec2_ip_q.id}",
            Q_REGION = "${data.aws_region.current.name}"
        }
    }
}

resource "aws_lambda_permission" "allow_SNS" {
  statement_id   = "AllowExecutionFromSNS"
  action         = "lambda:InvokeFunction"
  function_name  = "${aws_lambda_function.get_ec2_ip.arn}"
  principal      = "sns.amazonaws.com"
  source_arn     = "${aws_sns_topic.asg_lifecycly_hook_msg.arn}"
}

resource "aws_sqs_queue" "terminated_ec2_ip_q" {
    name                      = "${var.terminated_ec2_ip_q_name}"
    delay_seconds             = 90
    max_message_size          = 2048
    message_retention_seconds = 86400
    receive_wait_time_seconds = 10
}