tf_aws_lifecycle
==========
A Terraform module for a chain procedure when ec2 is being terminated during autoscaling group scales in, autoscaling
group lifecycle hook function will send internal ip address of terminated ec2 to a sqs service via sns and aws lambda
function. a lifecyclehook, sns, sqs, lambda and their related iam role will be created in the chain
This module makes the following assumption:
* You have an autoscaling group

Input Variables
---------------

- 'lifecycle_hook_name' - The autoscaling group lifecycle hook name
- 'asnar_name' - The autoscaling notification role name
- 'asg_lifecycle_hook_msg_topic_name' - The topic to store ec2 details pushed from lifecycle hook during ec2 ternimation
- 'lifecyclehook_sns_lambda_role_name' - The necessary role permission for lambda function to fetch ec2 detail via vpc and permission to send information to sqs
- 'terminated_ec2_ip_q_name' - The queue to store terminated ec2 internal ip sent from lambda function
  

Outputs
-------

- 'sqs_url'


Usage
-----

You can use these in your terraform template with the following steps.

1.) Adding a module resource to your template, e.g. `main.tf`

```
module "my_autoscaling_lifecycle_hook_function" {
  source                                = "gitlab-ncsa.ubisoft.org/terraform_online_modules/tf_aws_lifecycle"
  lifecycle_hook_name                   = "${var.lch_name}"
  asnar_name                            = "${var.asnar_name}"
  asg_lifecycle_hook_msg_topic_name     = "${var.asg_lifecycle_hook_msg_topic_name}"
  lifecyclehook_sns_lambda_role_name    = "${var.lifecyclehook_sns_lambda_role_name}"
  terminated_ec2_ip_q_name              = "${var.terminated_ec2_ip_q_name}"


2.) Setting values for the following variables, either through `terraform.tfvars` or `-var` arguments on the CLI

- aws_access_key
- aws_secret_key
- aws_region
- lch_name
- asnar_name
- asg_lifecycle_hook_msg_topic_name
- lifecyclehook_sns_lambda_role_name
- terminated_ec2_ip_q_name