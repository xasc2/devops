'use strict';

// var QUEUE_URL = 'https://sqs.us-east-2.amazonaws.com/857546223415/Ec2IPQ';
var SQS_REGION = process.env.Q_REGION
var QUEUE_URL = process.env.Q_URL;
var aws = require('aws-sdk');
// var sqs = new aws.SQS({region : 'us-east-2'});
var sqs = new aws.SQS({region : SQS_REGION});
var privateIp;

exports.handler = (event, context, callback) => {
    
	var message = JSON.parse(event.Records[0].Sns.Message);
	var ec2_instanceid = message.EC2InstanceId;
	console.log("instance id is "+ec2_instanceid);
	
    aws.config.region = SQS_REGION;

    var ec2 = new aws.EC2();
    var ec2_params = {InstanceIds:[ec2_instanceid]};

    ec2.describeInstances(ec2_params, function(e, data) {
        if (e)
            console.log(e, e.stack);
        else
            var privateIp = data.Reservations[0].Instances[0].PrivateIpAddress;
            console.log("internal IP is "+privateIp);
	        var sqs_params = {
                MessageBody : privateIp,
                QueueUrl : QUEUE_URL
            };
	        console.log(sqs_params);
	        
	        sqs.sendMessage(sqs_params, function(err,data){
	            if(err) {
	              console.log('error:',"Fail Send Message " + err);
	              context.done('error', "ERROR Put SQS");  // ERROR with message
	            }else{
	              console.log('data:',data.MessageId);
	              context.done(null,'');  // SUCCESS 
	            }
	         });
    });
};