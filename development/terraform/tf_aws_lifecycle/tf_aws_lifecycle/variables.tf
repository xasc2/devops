# Module: tf_aws_lifecycle
# Module specific variables

variable "lifecycle_hook_name" {
    description = "The autoscaling life cycle hook name"
}

variable "asnar_name" {
    description = "The autoscaling notification role name"
}

variable "asnar_policy_name" {
    description = "The autoscaling notification role policy name"
}

variable "asg_lifecycly_hook_msg_topic_name" {
    description = "The topic to store ec2 details pushed from lifecycle hook during ec2 ternimation"
}

variable "lifecyclehook_sns_lambda_role_name" {
    description = "The role for lambda function to fetch ec2 detail via vpc and permission to send information to sqs"
}

variable "lifecyclehook_sns_lambda_role_policy_name" {
    description = "The role policy name for lifecyclehook_sns_lambda_role_name"
}

variable "terminated_ec2_ip_q_name" {
    description = "The queue to store terminated ec2 internal ip sent from lambda function"
}