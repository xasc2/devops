var credential = require('dotenv').config();
var Consumer = require('sqs-consumer');
var Zabbix = require('zabbix-node');
var AWS = require('aws-sdk');
var fs = require('fs');
var chef = require('chef');
var proxy = require('proxy-agent');
var key = fs.readFileSync(process.env.CHEF_USER_KEY);
var Log = require('log');

var log = new Log('debug', fs.createWriteStream('decom.log'));

AWS.config.update({
        region: process.env.AWS_REGION,
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY,
        httpOptions: { agent: proxy('http://proxy:3128')}
});

AWS.config.apiVersion = {
                  sqs: '2012-11-05'
};

var app = Consumer.create({
        queueUrl: process.env.SQS_URL,
        region: process.env.AWS_REGION,
        batchSize: 1,
        handleMessage: function (message, done) {
            var privateipaddress = message.Body;
            log.info('internal ip: %s', privateipaddress);
            var zab_client = new Zabbix(process.env.ZABBIX_HOST, process.env.ZABBIX_USER, process.env.ZABBIX_PASSWORD);

            zab_client.login(function(error, resp, body) {
                zab_client.call(
	                'host.get',
	                {'filter':{'ip':[privateipaddress]}},
                    function(error, resp, body) {
		    			try {
		    				var hostid = body[0].hostid;
//							console.log('zabbix host id: '+hostid);
							log.info('zabbix agent host id: %s', hostid);
							zab_client.call('host.delete',
											[hostid],
											function(error, resp, body) {
//												console.log('zabbix deletion body: ');
//												console.log(body);
//												console.log('zabbix deletion error: ');
//												console.log(error);
												log.error(error);
											}
							);
		    			}
		    			catch(err) {
		    				log.info('zabbix get request error');
		    				log.error(error);
		    			}
            		}
	            );
            });

            var chef_client = chef.createClient(process.env.CHEF_USER_NAME, key, process.env.CHEF_SERVER_URL);
//          var query_path = '/search/node?q=ipaddress:10.130.142.26';
            var query_path = '/search/node?q=ipaddress:' + privateipaddress;
            log.info('chef server query_path: %s', query_path);
            chef_client.get(query_path, function(err, res, body) {
                if (err) { return log.error(err); }
                try {
//                  console.log(body.rows[0].name);
                    var node_name = body.rows[0].name;
                    log.info('client host name: %s', node_name);

                    var delete_node_path = '/nodes/' + node_name;
                    log.info(delete_node_path);
                    chef_client.delete(delete_node_path, body, function(err, res, body) {
//                    	console.log(err ? err : body);
                    	log.debug(err);
                    });
                    
                	var delete_client_path = '/clients/' + node_name;
                	log.info(delete_client_path);
                    chef_client.delete(delete_client_path, body, function(err, res, body) {
//                    	console.log(err ? err : body);
                    	log.debug(err);
                    });
                }
    			catch(err) {
    				log.info('chef request error');
    				log.error(err);
    				log.info('chef request body');
    				log.info(body);
    			}
            });
            return done();
        },
        sqs: new AWS.SQS()
});

app.on('error', function (err) {
        console.log(err);
        log.debug(err);
});

app.start();