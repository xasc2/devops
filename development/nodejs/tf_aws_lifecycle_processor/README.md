This repo is a source code repo for building docker container image. Image includes a nodejs program to finish whole lifecycle process. Follow these steps to finish build and deployment. All steps passed sucessfully in production env with a centos7 VM sitting on onbe network, e.g. a centos7 VM on UCOV2 platform. There are 2 options to run this container application, one is single docker run mode, another is docker swarm mode with replication options for specification of running container amount. Whatever running mode you choose, all services base on either latest docker-ce version or yum package version along with centos, current yum package version is docker 1.12.6 

# Base on latest docker-ce version
## Before set running environment, disalbe selinux first

$ setenforce 0

## 1. install docker-ce package and start docker service

$ source proxy_env.sh

$ yum remove docker docker-common docker-selinux docker-engine

$ yum install -y yum-utils device-mapper-persistent-data lvm2

$ yum install docker-ce

$ systemctl start docker

$ systemctl enable docker

## 2. configure docker pull behind corporate proxy
copy http-proxy.conf to /etc/systemd/system/docker.service.d/ then restart docker service

$ cp -a http-proxy.conf /etc/systemd/system/docker.service.d/

$ systemctl restart docker


# Base on yum package version(docker 1.12.6)

## 1.install docker package and start docker service
$ source proxy_env.sh

$ yum install docker

## 2. configure docker pull behind corporate proxy

$ echo [Service] >> /usr/lib/systemd/system/docker.service

$ echo Environment=HTTP_PROXY=http://proxy:3128/ >> /usr/lib/systemd/system/docker.service

$ echo Environment=HTTPS_PROXY=http://proxy:3128/ >> /usr/lib/systemd/system/docker.service

$ systemctl daemon-reload

$ systemctl restart docker

check if proxy env variable is properly added

$ systemctl show --property Environment docker


## 3. build docker image with sqs-consumer nodejs program integrated

put files Dockerfile, lifecycle_process.js .env and package.json in same directory then run command:

$ docker build -t lifecycle-process .

check built image with：

$ docker images


## 4. preparation for nodejs program in container

create a directoy env_data under root

$ mkdir /root/env_data

copy chef client key to env_data

$ cp sgs-mgmt-api.pem /root/env_data/

note: you can copy chef user client key in mountable path


# option 1: run container in single run mode

## 5. run nodejs container built in step 3 and check if container outputs error

$ docker run --name lifecycle-process -v /root/env_data:/mnt -d --restart always lifecycle-process

note: if chef user client key is in your desired path, command looks like this:

$ docker run --name lifecycle-process -v /your/desired/path/to/env_data:/mnt -d --restart always lifecycle-process

check if container is up

$ docker ps -a

check if container has error output

$ docker logs lifecycle-process

# option 2: run container in docker swarm mode

## 5. build a docker swarm service and run container in service mode

$ docker swarm init --advertise-addr host-ip

$ docker service create --replicas 4 --name lifecycle-process --mount type=bind,source=/root/env_data,target=/mnt quay.ubisoft.com/feng_xiao/lifecycle-process

check if service is up
$ docker service ps lifecycle-process
output should look like following, the ERROR column appears nothing means all replicas are running well

ID                         NAME                 IMAGE                                         NODE           DESIRED STATE  CURRENT STATE              ERROR
0em52h9ywmrdu7lt8sqvfivfk  lifecycle-process.1  lifecycle-process  swarm-mgmt     Running        Running about an hour ago  
cpm4b5h7eynps9227afs8a0qo  lifecycle-process.2  lifecycle-process  swarm-node-01  Running        Running about an hour ago  
84ltkqvud8re14fcxenqdtnc5  lifecycle-process.3  lifecycle-process  swarm-node-01  Running        Running about an hour ago  
6zz3jyfnm1saxnfvsge5jeff7  lifecycle-process.4  lifecycle-process  swarm-mgmt     Running        Running about an hour ago